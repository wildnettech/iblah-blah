<?php

if (version_compare(phpversion(), '5.4', '<') === true) {
    exit('phpFox 4 requires PHP 5.4 or newer.');
}

ini_set('gd.jpeg_ignore_warning', 1);
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);

define('PHPFOX_PARENT_DIR', __DIR__ . DIRECTORY_SEPARATOR);

require('./PF.Base/start.php');
