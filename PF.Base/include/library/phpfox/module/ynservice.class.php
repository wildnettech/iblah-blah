<?php

defined('PHPFOX') or exit('NO DICE!');

class Phpfox_Ynservice extends Phpfox_Service
{	    
	/**
	 * Extends the database object.
	 *
	 * @see Phpfox_Database
	 * @return object
	 */
    protected function database()
    {
    	return Phpfox::getLib('database.yndatabase');
    }
}

?>