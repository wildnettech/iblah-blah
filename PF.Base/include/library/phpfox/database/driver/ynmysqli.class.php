<?php
defined('PHPFOX') or exit('NO DICE!');

Phpfox::getLibClass('phpfox.database.driver.mysqli');

class Phpfox_Database_Driver_Ynmysqli extends Phpfox_Database_Driver_Mysqli
{
	protected function ping(){
		if (!(@(mysqli_ping($this->_hMaster)))) {
			return false;
		}

		return true;
	}

	public function checkAlive(){
		return $this->ping();
	}

	public function reconnect(){
		$this->close();
		$this->connect(Phpfox::getParam(array('db', 'host')), Phpfox::getParam(array('db', 'user')), Phpfox::getParam(array('db', 'pass')), Phpfox::getParam(array('db', 'name')));
	}
}

?>