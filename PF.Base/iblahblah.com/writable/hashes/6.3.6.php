<?php $calhash = array (
  'directory' => 
  array (
    'admin' => '9029fdfee416657f92ae9c39eed374a6',
    'api' => 'd56664a157994193d02f01cba9663741',
    'colors' => '0b393fd6437e4a8b03c5cb263e60c5a9',
    'extensions/ads' => '610c2965e01d381e923576bb2187b933',
    'extensions/bots' => 'd6dea1a230515428d599e9c07f3d01a3',
    'extensions/desktop' => '491d99a0093cd2ed2e42e510435eb175',
    'extensions/mobileapp' => '056ddfa440bb7558d31852934ba1656d',
    'functions' => 'b51c5585a7a2e2eeac33cbde7eaaa05a',
    'images' => 'cdd19030690e75c7f220db603f11ec44',
    'js' => '0f7e62a3eaad0122748ef80bacacb7e4',
    'mobile' => 'd41d8cd98f00b204e9800998ecf8427e',
    'modules/announcements' => '72e4f39d46e597e24101b4a6bdd55278',
    'modules/broadcastmessage' => 'dcda064fc96d469ed33313202882c30f',
    'modules/chatrooms' => '325a5197b30a8490b77386e81b2eafc8',
    'modules/facebook' => 'ed2c64ffebce14ed7cac64c8a00f2153',
    'modules/games' => '670c78431acd58a6771e080b10227644',
    'modules/home' => '6bc2cd8e19b8b0806289b9ea87623028',
    'modules/realtimetranslate' => 'fb853e5bf5cd1c139fc19cc9a359cd8a',
    'modules/scrolltotop' => 'd72e98412e3e2c32c3f7aafe11130550',
    'modules/share' => '39b7a0344be1f3a94f23455a7990eee1',
    'modules/translate' => '82b8e4bad21e9c1d63c717b114b4391a',
    'modules/twitter' => 'd48c3b906852a9a8cb2cbd0b90c3cb25',
    'plugins/audiochat' => '97d7f1b7394ab8d935c5f92137f26f5a',
    'plugins/avchat' => 'f6f82b869104fcca5e7cf68781104f97',
    'plugins/block' => '3420fff11db34c43bad8b66ac21d2fb2',
    'plugins/broadcast' => '303109ce60850252077a9d8793a0409d',
    'plugins/chathistory' => '1b43a4892ea291289c232e9f4874f52f',
    'plugins/clearconversation' => '3a26c1c528e27e9c8038209fe8cb8e12',
    'plugins/filetransfer' => '62e3f4d92ff3115d8ea9b478a42a9959',
    'plugins/handwrite' => '473eda346c8c455b0a855506f6ae6016',
    'plugins/report' => '5db88c9a386e888482efc256fb2f6869',
    'plugins/save' => 'c117557912a3a6b085b0d2b205639765',
    'plugins/screenshare' => '86d2b668c8ef64fbbf7753c5b367086f',
    'plugins/smilies' => 'b399a17d227808f0428485d0223f640a',
    'plugins/stickers' => '7c282a823b0eb2cb80103565a6daae52',
    'plugins/style' => '80c3998ac0b3d9f7d83333791add0232',
    'plugins/transliterate' => 'a24546dc0b9a72f3d14465e8e50909d6',
    'plugins/voicenote' => '566b1a185e0b58bf95809439f2d0ed81',
    'plugins/whiteboard' => 'a2967d8b0532d497ae722c7e3638f1cb',
    'plugins/writeboard' => '082d25a24d5b31565a4c4bbf3b121d57',
    'sounds' => 'd41d8cd98f00b204e9800998ecf8427e',
    'temp' => 'd41d8cd98f00b204e9800998ecf8427e',
    'themes' => '4fe67459a6a02a6895bd33db8f758f12',
    'transports/cometservice' => 'b3bf6b7ed5c8a2ad1b11c99d78b820a6',
    'transports/cometservice-selfhosted' => '961899bc9fcb503cab6d489c4fcfe362',
    'updates' => '83527dfb63debd01a3becc1659e8fe18',
  ),
  'files' => 
  array (
    0 => '.htaccess',
    1 => 'CHANGELOG.txt',
    2 => 'EULA.txt',
    3 => 'VERSION.txt',
    4 => 'ccauth.php',
    5 => 'cometchat_cache.php',
    6 => 'cometchat_check.php',
    7 => 'cometchat_checkmobileapp.php',
    8 => 'cometchat_delete.php',
    9 => 'cometchat_embedded.php',
    10 => 'cometchat_getid.php',
    11 => 'cometchat_guests.php',
    12 => 'cometchat_init.php',
    13 => 'cometchat_isfriend.php',
    14 => 'cometchat_login.php',
    15 => 'cometchat_logout.php',
    16 => 'cometchat_popout.php',
    17 => 'cometchat_receive.php',
    18 => 'cometchat_send.php',
    19 => 'cometchat_session.php',
    20 => 'cometchat_shared.php',
    21 => 'cometchatcss.php',
    22 => 'cometchatjs.php',
    23 => 'config.php',
    24 => 'cron.php',
    25 => 'css.php',
    26 => 'index.html',
    27 => 'install.php',
    28 => 'js.php',
    29 => 'jsmin.php',
    30 => 'lang.php',
    31 => 'modules.php',
    32 => 'php4functions.php',
    33 => 'plugins.php',
  ),
  'fileshash' => '0d74e4411c5c9fcc59c4c84e33d5acd2',
); ?>
