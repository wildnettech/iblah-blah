<?php
/**
*
* For this app we return a callable function, which passes the current $App object
*/
new Core\Event([
    'lib_phpfox_template_getheader' => function(Phpfox_Template $Template) {
        if (setting('enable_cometchat')){
            if(file_exists(PHPFOX_DIR.'iblahblah.com/license.php') && strpos($_SERVER['REQUEST_URI'], '/admincp/') === false) {
                $Template->setHeader('<link type="text/css" href="//'.Phpfox::getParam('core.host').Phpfox::getParam('core.folder').'../PF.Base/iblahblah.com/cometchatcss.php" rel="stylesheet" charset="utf-8">');
                $Template->setHeader('head',array("\n\t\t\t<script type=\"text/javascript\" src=\"" . $Template->getStyle('jscript', '/../../../../../../../../PF.Base/iblahblah.com/cometchatjs.php') . "\"></script>\n\t\t"));
            }
        }
    }
    ]);
group('/cc_id', function() {
    route('/acp', function() {
        auth()->isAdmin(true);
        $checkdir = true;
        if(file_exists(dirname(dirname(dirname(dirname(__FILE__))))."/PF.Base/iblahblah.com/integration.php")){
            $checkdir = false;
        }
        $cometchat_admin = '//'.Phpfox::getParam('core.host').Phpfox::getParam('core.folder').'../PF.Base/iblahblah.com/admin';
        return view('admincp.html', [
            'checkdir' => $checkdir,
            'cometchat_admin' => $cometchat_admin
            ]);
    });
    route('/acp/upgrade', function() {
        auth()->isAdmin(true);
        title(_p("Upgrade CometChat"));
        return view('admincp_server.html', [
            ]);
    });
    route('/upgrade_controller', function() {
        auth()->isAdmin(true);
        if(isset($_FILES['cometchat_file'])){
            if(file_exists(dirname(dirname(dirname(dirname(__FILE__))))."/PF.Base/iblahblah.com/integration.php")){
                rename(dirname(dirname(dirname(dirname(__FILE__))))."/PF.Base/cometchat", dirname(dirname(dirname(dirname(__FILE__))))."/PF.Base/cometchat_".time());
            }
            $filename = $_FILES["cometchat_file"]["name"];
            $source = $_FILES["cometchat_file"]["tmp_name"];
            if($filename != 'cometchat.zip'){
                \Phpfox::addMessage(_p("<script type='text/javascript'> alert('Please upload valid CometChat zip file.'); </script>"));
                return url()->send('admincp.app', ['id' => 'cometchat']);
            }
            $target_path = dirname(dirname(dirname(dirname(__FILE__))))."/PF.Base/".$filename;
            if(move_uploaded_file($source, $target_path)) {
                $zip = new ZipArchive();
                $x = $zip->open($target_path);
                if ($x === true) {
                    $zip->extractTo(dirname(dirname(dirname(dirname(__FILE__))))."/PF.Base/");
                    $zip->close();
                    unlink($target_path);
                }
                $server_protocol = (strpos($_SERVER['SERVER_PROTOCOL'],'HTTPS')?'https://':'http://');
                $cometchat_install_path = $server_protocol   .Phpfox::getParam('core.host').Phpfox::getParam('core.folder').'../PF.Base/iblahblah.com/install.php';
                $install_status = @file_get_contents($cometchat_install_path);
                if ($install_status === FALSE) {
                     \Phpfox::addMessage(_p("CometChat upgraded successfully.<iframe src='".$cometchat_install_path."' style='width:1px;height:1px;border:0;'></iframe>"));
                }else{
                    \Phpfox::addMessage(_p("CometChat has been upgraded successfully."));
                }
            } else {
                \Phpfox::addMessage(_p("<script type='text/javascript'> alert('Oops! looks like we don\'t have necessary file permissions to rename \'cometchat\' folder. Please give required permissions and try again. Alternatively, please unzip the \'cometchat.zip\' that has been provided in the package. Now place the \'cometchat\' folder in your PhpFox root/PF.Base directory and execute \'cometchat/install.php\' from \'http://{PATH_TO_NEUTRON}/PF.Base/iblahblah.com/install.php\' file.'); </script>"));
            }
        }
        return url()->send('admincp.app', ['id' => 'cometchat']);
    });
    route('/upload', function() {
        auth()->isAdmin(true);
        $cometchat_install = false;
        if(isset($_FILES['cometchat_file'])){
            $filename = $_FILES["cometchat_file"]["name"];
            $source = $_FILES["cometchat_file"]["tmp_name"];
            if($filename != 'cometchat.zip'){
                \Phpfox::addMessage(_p("<script type='text/javascript'> alert('Please upload valid CometChat zip file.'); </script>"));
                return url()->send('admincp.app', ['id' => 'cometchat']);
            }
            $target_path = dirname(dirname(dirname(dirname(__FILE__))))."/PF.Base/".$filename;
            if(move_uploaded_file($source, $target_path)) {
                $zip = new ZipArchive();
                $x = $zip->open($target_path);
                if ($x === true) {
                    $zip->extractTo(dirname(dirname(dirname(dirname(__FILE__))))."/PF.Base/");
                    $zip->close();
                    $rootPath = '//'.Phpfox::getParam('core.host').Phpfox::getParam('core.folder').'../PF.Base/';                                
                    unlink($target_path);
                }
                $cometchat_install = true;
                $hash = md5(time().$_SERVER['SERVER_NAME']);
                $cc_check = Phpfox::getLib('database')->select('description')
                ->from(Phpfox::getT('apps'))
                ->where('apps_id = "cometchat"')
                ->execute('getRows');               

                if(empty($cc_check) || empty($cc_check[0]['description']) || $cc_check[0]['description'] == 'NULL' ){                    
                    Phpfox::getLib('database')->update(
                        Phpfox::getT('apps'),
                        array(                            
                            'description'=>$hash                            
                            ),'apps_id = "cometchat"'
                        );
                }else{}
                $server_type = (strpos($_SERVER['SERVER_PROTOCOL'],'HTTPS')?'https://':'http://');
                $cometchat_install_path = $server_type.Phpfox::getParam('core.host').Phpfox::getParam('core.folder').'../PF.Base/iblahblah.com/install.php';
                
                $install_status = @file_get_contents($cometchat_install_path);                
                if ($install_status === FALSE) {
                      \Phpfox::addMessage(_p("CometChat has been installed successfully.<iframe src='".$cometchat_install_path."' style='width:1px;height:1px;border:0;'></iframe>"));
                }else{
                      \Phpfox::addMessage(_p("CometChat has been installed successfully."));
                }
            } else {
                \Phpfox::addMessage(_p("<script type='text/javascript'> alert('Oops! looks like we don\'t have necessary file permissions to upload \'cometchat\' folder into your PhpFox directory. Please give required permissions and try again. Alternatively, please unzip the \'cometchat.zip\' that has been provided in the package. Now place the \'cometchat\' folder in your PhpFox root/PF.Base directory and execute \'cometchat/install.php\' from \'http://{PATH_TO_NEUTRON}/PF.Base/iblahblah.com/install.php\' file.'); </script>"));
            }
        }
        return url()->send('admincp.app', ['id' => 'cometchat']);
    });
});