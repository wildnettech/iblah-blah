<?php
            namespace Apps\cometchat;

            use Core\App;

            /**
             * Class Install
             * @author  CometChat
             * @version 4.5.0
             * @package Apps\cometchat
             */
            class Install extends App\App
            {
                private $_app_phrases = [
                
                ];
                protected function setId()
                {
                    $this->id = 'cometchat';
                }
                protected function setAlias() 
                {
                }
                protected function setName()
                {
                    $this->name = 'CometChat';
                } protected function setVersion() {
                    $this->version = '1.0';
                } protected function setSupportVersion() {
                    $this->start_support_version = '4.4.0';
                    $this->end_support_version = '4.5.0';
                } protected function setSettings() {
                    $this->settings = ['enable_cometchat' => ['type' => 'input:radio','info' => 'Enable CometChat','value' => '1',],
                                       'inbox_sync' => ['type' => 'input:radio','info' => 'Synchronize inbox with PhpFox','value' => '1',],                            
                                       ]; 

                } protected function setUserGroupSettings() {} protected function setComponent() {} protected function setComponentBlock() {} protected function setPhrase() {
                    $this->phrase = $this->_app_phrases;
                } protected function setOthers() {
                    $this->icon = 'https://www.cometchat.com/public/img/cometchat-logo-square.jpg';
                    $this->admincp_route = "/cc_id/acp";
                    $this->admincp_menu = [
                    "Administration" => "#"
                    ];
                    $this->admincp_action_menu = [
                    "/cc_id/acp/upgrade" => "Upgrade CometChat"
                    ];
                }                
                public $vendor = '<a href="http://www.cometchat.com" target="_blank" alt="">www.cometchat.com</a>';
            }