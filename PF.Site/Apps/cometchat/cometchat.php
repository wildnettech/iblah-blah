<?php
define('PHPFOX', true);
if(file_exists(dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR.'PF.Base'.DIRECTORY_SEPARATOR.'file'.DIRECTORY_SEPARATOR.'settings'.DIRECTORY_SEPARATOR.'server.sett.php')){
	include_once(dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR.'PF.Base'.DIRECTORY_SEPARATOR.'file'.DIRECTORY_SEPARATOR.'settings'.DIRECTORY_SEPARATOR.'server.sett.php');
}else{
	echo "Failed to include the database configuration file.";
 	exit;
}
$dbh = mysqli_connect($_CONF['db']['host'],$_CONF['db']['user'],$_CONF['db']['pass'],$_CONF['db']['name'],"3306");
if (mysqli_connect_errno($dbh)) {
	die("Connection failed: " . mysqli_connect_errno($dbh));
}
$sql = ("INSERT INTO ".$_CONF['db']['prefix']."apps (apps_id,apps_name,version,apps_alias,author,vendor,description,apps_icon,type,is_active) SELECT 'cometchat','CometChat','1.0','','CometChat','https://www.cometchat.com/','','http://apps.phpfoxer.net/module_icons_important/username_on_profile.jpg',2,1 from ".$_CONF['db']['prefix']."apps WHERE (SELECT COUNT(apps_id) FROM ".$_CONF['db']['prefix']."apps WHERE apps_id = 'cometchat') = 0 LIMIT 1");
$result = mysqli_query($GLOBALS['dbh'],$sql);
if($result){
	echo "Successfully created database entry for CometChat App!!!";
	$server_protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
	$phpfox_appspage = $server_protocol.$_SERVER['SERVER_NAME'].$_CONF['core.folder'].'index.php/admincp/apps/';	
	header("Location: ".$phpfox_appspage);
	die();
}else{
	echo "Failed to create database entry for CometChat App.";
}
?>