<?php

$oFilter = Phpfox::getLib('parse.input');
$message = $oFilter->prepare(utf8_decode($aVals['message']));
if(count($aUserInsert) == 2 ){
	$to = ($aUserInsert[1] != Phpfox::getUserId())?$aUserInsert[1]:$aUserInsert[0];
	$aCategories = $this->database()->select('menu')
	->from(Phpfox::getT('module'))
	->where('phrase_var_name = "cc_hash"')
	->execute('getSlaveRows');
	$cc_hash = $aCategories[0]["menu"];
	$from_id= Phpfox::getUserId();
	$hashval = md5($cc_hash.$from_id);
	$domain = Phpfox::getLib('url')->getDomain();
	$url = $domain.'../PF.Base/cometchat/cometchat_send.php';
	if(!isset($_SESSION['random'])){
		$rand1 = rand(10000000000000000000,99999999999999999999);
		$rand2 = rand(1000000000000,9999999999999);
		$callback = 'jqcc'.$rand1.'_'.$rand2;
		$_SESSION['random'] = $callback;
	}else{
		$callback = $_SESSION['random'];
	}
	if (in_array ('curl', get_loaded_extensions())){
		$fields_string = array('cc_social_userid'=>$from_id,'to'=>$to,'message'=>$message,'callback'=>$callback,'hash_val'=>$hashval,'deny_hooks_message'=>'true','deny_sanitize'=>'true','cc_direction'=>0);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPGET, 1);
		curl_setopt($ch, CURLOPT_URL, $url );
		curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
		curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		$result = curl_exec($ch);
		curl_close($ch);
	}else{
		$this->database()->query("INSERT INTO `cometchat`(`from`, `to`, `message`, `sent`, `read`, `direction`) VALUES($from_id,$to,'$message',".PHPFOX_TIME.",1,0)");
	}
}