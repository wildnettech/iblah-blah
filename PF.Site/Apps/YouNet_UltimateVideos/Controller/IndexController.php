<?php
/**
 * Created by PhpStorm.
 * User: davidnguyen
 * Date: 7/26/16
 * Time: 10:06 AM
 */

namespace Apps\YouNet_UltimateVideos\Controller;

use Phpfox;
use Phpfox_Component;
use Phpfox_Locale;
use Phpfox_Module;
use Phpfox_Pager;
use Phpfox_Plugin;
defined('PHPFOX') or exit('NO DICE!');
use Core\Route\Controller;

class IndexController extends \Phpfox_Component
{
    public function process()
    { 
        $bIsSearch = false;
        $aParentModule = $this->getParam('aParentModule');
        if ($aParentModule === null && $this->request()->getInt('req2') > 0)
        {

            if (($this->request()->get('req1') == 'pages' && Phpfox::isModule('pages') == false) ||
                ($aParentModule['module_id'] == 'pages' && Phpfox::getService('pages')->hasPerm($aParentModule['item_id'], 'ultimatevideo.view_browse_videos') == false) )
            {
                return \Phpfox_Error::display(_p('Cannot display due to privacy'));
            }
        }        
        if(defined('PHPFOX_IS_USER_PROFILE') || defined('PHPFOX_IS_PAGES_VIEW')){
            $bIsSearch = true;
            Controller::$name =  true;
        }else{
            
            Controller::$name =  '';
        }
        if(!defined('PHPFOX_IS_PAGES_VIEW')){
            sectionMenu(_p('Share A Video'), url('/ultimatevideo/add'));
        }
    	$bIsUserProfile = $this->getParam('bIsProfile');
        $aUser = [];
        $bShowModeration = (user('ynuv_can_approve_video') || user('ynuv_can_delete_video_of_other_user') || user('ynuv_can_feature_video'));

        if ($bIsUserProfile) {
            $aUser = $this->getParam('aUser');
        }
        $sView = $this->request()->get('view');

        if (defined('PHPFOX_IS_AJAX_CONTROLLER')) {
            $bIsProfile = true;
            $aUser = Phpfox::getService('user')->get($this->request()->get('profile_id'));
            $this->setParam('aUser', $aUser);
        } else {
            $bIsProfile = $this->getParam('bIsProfile');
            if ($bIsProfile === true) {
                $aUser = $this->getParam('aUser');
            }
        }

        Phpfox::getService('ultimatevideo.callback')->buildFilterMenu($this);

    	$this->search()->set([
                'type'        => 'ultimatevideo',
                'field'       => 'video.video_id',
                'search_tool' => [
                    'table_alias' => 'video',
                    'search'      => [
                        'action'        => (($aParentModule === null ? ($bIsUserProfile === true ? $this->url()->makeUrl($aUser['user_name'], array('ultimatevideo', 'view' => $this->request()->get('view'))) : $this->url()->makeUrl('ultimatevideo', array('view' => $this->request()->get('view')))) : $aParentModule['url'] . 'ultimatevideo/view_' . $this->request()->get('view') . '/')),
                        'default_value' => _p('Search videos'),
                        'name'          => 'search',
                        'field'         => 'video.title',
                    ],
                    'sort'        => [
                        'latest'     => ['video.time_stamp', _p('Latest')],
                        'most-viewed' => ['video.total_view',_p('Most Viewed')],
                        'most-liked' => ['video.total_like', _p('Most Liked')],
                        'most-commented' => ['video.total_comment',_p('Most Commented')],
                        'highest-rated' => ['video.rating',_p('Highest Rated')],
                        'featured'  => ['video.video_id',_p('Featured')]

                    ],
                    'show'        => [12, 24, 36],
                ],
            ]
        );

        $aBrowseParams = [
            'module_id' => 'ultimatevideo',
            'alias'     => 'video',
            'field'     => 'video_id',
            'table'     => Phpfox::getT('ynultimatevideo_videos'),
            'hide_view' => ['pending', 'my'],
        ];

    	if (user('ynuv_can_upload_video' , '0') == '1') {

        }

        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            foreach(['sort','show','when','view','s','user','tag'] as $temp){
                if(!empty($_GET[$temp])){
                    $bIsSearch = true;
                }
            }
        }



        $sCategory = null;
        if ($this->request()->get(($bIsProfile ? 'req3' : 'req2')) == 'category'){
            $bIsSearch = true;
            $sCategory =  $this->request()->getInt(($bIsProfile ? 'req4' : 'req3'));
            $sCategoryList =  Phpfox::getService('ultimatevideo.category')->getCategoryIdDescendants($sCategory);
            $this->search()->setCondition('AND video.category_id IN( ' . $sCategoryList. ')');
        }
        if($this->request()->get('sort') && $this->request()->get('sort') == 'featured')
        {
            $this->search()->setCondition(' AND video.is_featured=1 ');
        }
        $sView = $this->request()->get('view');
        switch($sView){
            case 'history':
                Phpfox::isUser(true);
                $bShowModeration = false;
                break;
            case 'later':
                Phpfox::isUser(true);
                $bShowModeration = false;
                break;
            case 'favorite':
                Phpfox::isUser(true);
                $bShowModeration = false;
                break;
            case 'my':
                Phpfox::isUser(true);
                $this->search()->setCondition(' AND video.module_id !="pages" AND video.module_id !="groups" AND video.user_id='.intval(Phpfox::getUserId()));
                break;
            case 'pending':
                Phpfox::isUser(true);
                $this->search()->setCondition(' AND video.is_approved=0');
                break;
            case 'featured':
                $this->search()->setCondition(' AND video.is_approved=0 AND video.status=1 AND video.is_featured=1');
                break;
            default:
                if (($this->request()->get('req1') == 'pages' && Phpfox::isModule('pages') && $this->request()->getInt('req2') > 0))
                {
                    $this->search()->setCondition(' AND video.module_id like "pages" AND video.item_id ='.$this->request()->getInt('req2'));
                    break;
                }
                if (($this->request()->get('req1') == 'groups' && Phpfox::isModule('groups') && $this->request()->getInt('req2') > 0))
                {
                    $this->search()->setCondition(' AND video.module_id ="groups" AND video.item_id ='.$this->request()->getInt('req2'));
                    break;
                }
                if(defined('PHPFOX_IS_USER_PROFILE'))
                {
                    $this->search()->setCondition(' AND video.user_id='.intval($aUser['user_id']));  
                    break;                  
                }
                $this->search()->setCondition(' AND video.is_approved=1 AND video.status=1 AND video.module_id !="pages" AND video.module_id !="groups"');
                break;
        }

        $iFromUser = $this->request()->get('user');
        if($iFromUser)
        {
            $this->search()->setCondition(' AND video.module_id !="pages" AND video.module_id !="groups" AND video.user_id='.intval($iFromUser));
        }
        $aItems = null;

        if($bIsSearch){
            $this->search()->browse()->params($aBrowseParams)->execute();
            $aItems = $this->search()->browse()->getRows();
            Phpfox_Pager::instance()->set(array('page' => $this->search()->getPage(), 'size' => $this->search()->getDisplay(), 'count' => $this->search()->browse()->getCount()));
        }
        $bIsNoItem = false;
        if(!$bIsSearch)
        {
            if(!Phpfox::getService('ultimatevideo.browse')->getMostRecommendedVideos(5) && !Phpfox::getService('ultimatevideo.browse')->getSlideshowVideos(5) && !Phpfox::getService('ultimatevideo.browse')->getWatchItAgainVideos(5) && !Phpfox::getService('ultimatevideo.playlist.browse')
            ->getMostRecentPlaylists(5)){
                $bIsNoItem = true;
            }
        }
        
        $corePath = Phpfox::getParam('core.path_actual').'PF.Site/Apps/YouNet_UltimateVideos';
        $this->setParam('sCategory', $sCategory);
        $this->template()->assign([
            'aItems'=>$aItems,
            'bVideoView'=>true,
            'bIsSearch'=> $bIsSearch,
            'sView'=>$sView,
            'bShowTotalView'=>true,
            'bShowTotalLike'=>true,
            'bShowTotalComment'=>false,
            'bShowModeration' => $bShowModeration,
            'corePath' => $corePath,
            'bIsPagesView' => defined('PHPFOX_IS_PAGES_VIEW') ? true : false,
            'bIsUserProfile' => $bIsUserProfile,
            'iPage' => $this->request()->get('page'),
            'bIsNoItem' => $bIsNoItem,
        ]);



        if($bShowModeration){
            $aModerationMenu =  [];
            if(user('ynuv_can_delete_video_of_other_user')){
                $aModerationMenu[] = [
                    'phrase' => Phpfox::getPhrase('core.delete'),
                    'action' => 'delete'
                ];
            }

            if(user('ynuv_can_approve_video') && $sView == 'pending'){
                $aModerationMenu[] = [
                    'phrase' => _p('Approve'),
                    'action' => 'approve'
                ];
            }
            if(user('ynuv_can_feature_video')){
                $aModerationMenu[] = [
                    'phrase' => _p('Feature'),
                    'action' => 'feature'
                ];
            }
            if($sView == 'favorite'){
                $aModerationMenu[] = [
                    'phrase' => _p('UnFavorite all selected'),
                    'action' => 'unfavorite'
                ];
            }
            if($sView == 'history'){
                $aModerationMenu[] = [
                    'phrase' => _p('Remove all from history'),
                    'action' => 'history'
                ];
            }
            if($sView == 'later'){
                $aModerationMenu[] = [
                    'phrase' => _p('UnWatched all selected'),
                    'action' => 'unwatched'
                ];
            }
            $this->setParam('global_moderation', [
                'name' => 'ultimatevideo',
                'ajax' => 'ultimatevideo.moderation',
                'menu' => $aModerationMenu
            ]);
        }

        if($this->request()->get('user')){
            $aUser =  Phpfox::getService('user')->getUser($this->request()->get('user'));
            $this->template()->setBreadCrumb(_p('{{ name }}\'s Videos',['name'=>$aUser['full_name']]),'');
        }else{
            $this->template()->setBreadCrumb(_p('Ultimate Videos'),Phpfox::permalink('ultimatevideo',null,false))
                ->setTitle(_p('Ultimate Videos'));
        }
        //Special breadcrumb for pages
        if (defined('PHPFOX_IS_PAGES_VIEW') && PHPFOX_IS_PAGES_VIEW && defined('PHPFOX_PAGES_ITEM_TYPE')){
            if (Phpfox::hasCallback(PHPFOX_PAGES_ITEM_TYPE, 'checkPermission') && !Phpfox::getService(PHPFOX_PAGES_ITEM_TYPE)->hasPerm($aParentModule['item_id'], 'ultimatevideo.view_browse_videos')) {
                $this->template()->assign(['aSearchTool' => []]);
                return \Phpfox_Error::display(_p('Cannot display this section due to privacy.'));
            }
            $this->template()
                ->clearBreadCrumb();
            $this->template()
                ->setBreadCrumb(Phpfox::getService(PHPFOX_PAGES_ITEM_TYPE)->getTitle($aParentModule['item_id']), $aParentModule['url'])
                ->setBreadCrumb(_p("Ultimate Videos"), $aParentModule['url'] . 'ultimatevideo/');
        }
    }
}