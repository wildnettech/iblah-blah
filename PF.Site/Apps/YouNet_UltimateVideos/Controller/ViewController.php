<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/12/16
 * Time: 11:09 AM
 */

namespace Apps\YouNet_UltimateVideos\Controller;
use Core\Route\Controller;
use Phpfox;
use Phpfox_Error;
use Privacy_Service_Privacy;

class ViewController extends \Phpfox_Component
{
    const MAX_CATEGORY_LEVEL =  3;

    public function process()
    {
        Controller::$name =  '';

        $id = $this->request()->get('req2');

        $aItem =  Phpfox::getService('ultimatevideo')
            ->getVideo($id);
        $sError =  null;
        if (Phpfox::isModule('privacy'))
        {
            Privacy_Service_Privacy::instance()->check('ultimatevideo', $aItem['video_id'], $aItem['user_id'], $aItem['privacy'], $aItem['is_friend']);
        }
        if(!user('ynuv_can_view_video'))
        {
            $sError = _p('You don\'t have permission to view this video.');
        }
        if(!$aItem)
        {
            $sError = _p('The video you are looking for does not exist or has been removed');
        }
        elseif ($aItem['status'] == 0){
            $sError = _p('The video you are looking for does not exist or has not been processed yet.');
        }
        elseif($aItem['status'] != 1 && $aItem['status'] != 0 && $aItem['status'] != 2)
        {
            $sError = _p('The video you are looking for was failed to upload.');
        }

        $this->template()->setTitle($aItem['title']);
        if (!user('ynuv_can_approve_video',0) && !$sError)
        {
            if ($aItem['is_approved'] != '1' && $aItem['user_id'] != Phpfox::getUserId())
            {
                $sError = _p('Video not found.');
            }
        }
        if($sError)
        {
            $this->template()->setBreadCrumb(_p('Ultimate Videos'),Phpfox::permalink('ultimatevideo',null,false));
            $this->template()->assign([
                'sError'=>$sError
            ]);
            return;
        }
        define('ULTIMATE_VIDEO_ID', $id);
        define('ULTIMATE_VIDEO_OWNER_ID', $aItem['user_id']);
        define('ULTIMATE_VIDEO_USER_NAME', $aItem['full_name'] );
        define('ULTIMATE_VIDEO_CATEGORY_ID', $aItem['category_id'] );

        // add video to history list
        Phpfox::getService('ultimatevideo.history')
            ->addVideo(Phpfox::getUserId(), $id);

        // update view later status if the video is in watch later list
        Phpfox::getService('ultimatevideo.watchlater')
            ->updateViewStatus(Phpfox::getUserId(), $id);

        Phpfox::getService('ultimatevideo.process')
            ->updateViewCount($id);

        $corePath = Phpfox::getParam('core.path_actual').'PF.Site/Apps/YouNet_UltimateVideos';
        $isWatchLater = Phpfox::getService('ultimatevideo.watchlater')->findId(Phpfox::getUserId(),$aItem['video_id']);
        if($isWatchLater)
            $aItem['watchlater'] = 1;
        else
            $aItem['watchlater'] = 0;

        $bShowEditMenu =  false;
        if(
            user('ynuv_can_approve_video') ||
            user('ynuv_can_delete_video_of_other_user') ||
            user('ynuv_can_edit_video_of_other_user') ||
            user('ynuv_can_feature_video')){
            $bShowEditMenu = true;
        }

        if((Phpfox::getUserId() == $aItem['user_id']) && (
            user('ynuv_can_delete_own_video') ||
            user('ynuv_can_edit_own_video'))){
            $bShowEditMenu = true;
        }

        $this->template()->assign([
            'bShowModeration' => false,
            'corePath' => $corePath,
            'aItem'=>$aItem,
            'bShowEditMenu' => $bShowEditMenu,
            'sUrl'=> Phpfox::permalink('ultimatevideo.embed', $id, ''),
            'bIsPagesView' => false,
            'bIsDetailView' => true,
        ]);

        if (!user('ynuv_can_approve_video',0))
        {
            if ($aItem['is_approved'] != '1' && $aItem['user_id'] != Phpfox::getUserId())
            {
                return \Phpfox_Error::display('Video not found.', 404);
            }
        }

        $this->setParam('aFeed', array(
                'comment_type_id' => 'ultimatevideo_video',
                'privacy' => $aItem['privacy'],
                'comment_privacy' => $aItem['privacy'],
                'like_type_id' => 'ultimatevideo_video',
                'feed_is_liked' => isset($aItem['is_liked']) ? $aItem['is_liked'] : false,
                'feed_is_friend' => $aItem['is_friend'],
                'item_id' => $aItem['video_id'],
                'user_id' => $aItem['user_id'],
                'total_comment' => $aItem['total_comment'],
                'feed_type'=>'ultimatevideo_video',
                'total_like' => $aItem['total_like'],
                'feed_link' => $aItem['bookmark_url'],
                'feed_title' => $aItem['title'],
                'feed_display' => 'view',
                'feed_total_like' => $aItem['total_like'],
                'report_module' => 'ultimatevideo_video',
                'report_phrase' => _p('Report this video'),
                'time_stamp' => $aItem['time_stamp'],
            )
        );
        $aCallback = false;
        if (!empty($aItem['module_id']) && $aItem['module_id'] != 'ynultimatevideo')
        {
            if (Phpfox::hasCallback($aItem['module_id'], 'getVideoDetails'))
            {
                $aCallback = Phpfox::callback($aItem['module_id'] . '.getVideoDetails', $aItem);
            }
            else
            {   
                $aCallback = $this->getVideoDetails($aItem);

            }
            $this->template()
                    ->setBreadcrumb($aCallback['breadcrumb_title'], $aCallback['breadcrumb_home'])
                    ->setBreadcrumb($aCallback['title'], $aCallback['url_home']);
            if (($aItem['module_id'] == 'pages' && !Phpfox::getService('pages')->hasPerm($aCallback['item_id'], '')) || $aItem['module_id'] == 'groups' && !Phpfox::getService('groups')->hasPerm($aCallback['item_id'], ''))
            {
                return \Phpfox_Error::display(_p('Unable to view this item due to privacy settings'));
            }
        }
        else{
            $this->template()->setBreadCrumb(_p('Ultimate Videos'),Phpfox::permalink('ultimatevideo',null,false));
        }

        $aCategories =  Phpfox::getService('ultimatevideo.category')->getCategoryAncestors($aItem['category_id'], self::MAX_CATEGORY_LEVEL);

        foreach($aCategories as $aItem){
            $this->template()->setBreadCrumb(\Core\Lib::phrase()->isPhrase($aItem['title']) ? _p($aItem['title']) : $aItem['title'], Phpfox::permalink('ultimatevideo.category', $aItem['category_id'], $aItem['title']));
        }
    }
    public function getVideoDetails($aItem)
    {
        Phpfox::getService('pages')->setIsInPage();

        $aRow = Phpfox::getService('groups')->getPage($aItem['item_id']);

        if (!isset($aRow['page_id']))
        {
            return false;
        }

        Phpfox::getService('groups')->setMode();

        $sLink = Phpfox::getService('groups')->getUrl($aRow['page_id'], $aRow['title'], $aRow['vanity_url']);

        return array(
            'breadcrumb_title' => _p('Groups'),
            'breadcrumb_home' => \Phpfox_Url::instance()->makeUrl('groups'),
            'module_id' => 'groups',
            'item_id' => $aRow['page_id'],
            'title' => $aRow['title'],
            'url_home' => $sLink,
            'url_home_photo' => $sLink . 'video/',
            'theater_mode' => Phpfox::getPhrase('pages.in_the_page_link_title', array('link' => $sLink, 'title' => $aRow['title']))
        );
    }    
}