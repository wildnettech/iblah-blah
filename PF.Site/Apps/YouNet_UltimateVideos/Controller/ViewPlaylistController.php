<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/11/16
 * Time: 5:47 PM
 */

namespace Apps\YouNet_UltimateVideos\Controller;

use Core\Route\Controller;
use Phpfox_Component;
use Phpfox;
use Phpfox_Error;
use Privacy_Service_Privacy;

class ViewPlaylistController extends Phpfox_Component
{
    const MAX_CATEGORY_LEVEL = 3;

    public function process()
    {
        Controller::$name  = '';

        $id = $this->request()->get('req3');
        $sError =  null;

        $this->template()->assign([
            'bShowModeration' => false,
        ]);

        $aItem = Phpfox::getService('ultimatevideo')->getPlaylist($id);
        $this->template()->setBreadCrumb(_p('Ultimate Videos'),Phpfox::permalink('ultimatevideo.playlist',null,false))->setTitle($aItem['title']);   
        if(!user('ynuv_can_view_playlist'))
        {
            $sError = _p('You don\'t have permission to view this playlist.');
        }     
        if(!$aItem)
        {
            $sError = _p('The playlist you are looking for does not exist or has been removed');
        }
        if (!user('ynuv_can_approve_playlist',0) && !$sError)
        {
            if ($aItem['is_approved'] != '1' && $aItem['user_id'] != Phpfox::getUserId())
            {
                $sError = _p('Playlist not found.');
            }
        }
        if($sError)
        {
            $this->template()->assign([
                'sError'=>$sError
            ]);
            return;
        }
        define('ULTIMATE_PLAYLIST_ID', $id);
        define('ULTIMATE_PLAYLIST_OWNER_ID', $aItem['user_id']);
        define('ULTIMATE_PLAYLIST_USER_NAME', $aItem['full_name'] );
        define('ULTIMATE_PLAYLIST_CATEGORY_ID', $aItem['category_id'] );
        
        $this->setParam('playlist_id',$id);
        if (Phpfox::isModule('privacy'))
        {
            Privacy_Service_Privacy::instance()->check('ultimatevideo_playlist', $aItem['playlist_id'], $aItem['user_id'], $aItem['privacy'], $aItem['is_friend']);
        }

        // add video to history list
        Phpfox::getService('ultimatevideo.history')
            ->addPlaylist(Phpfox::getUserId(), $id);

        Phpfox::getService('ultimatevideo.playlist.process')
            ->updateViewCount($id);
        $bShowEditMenu =  false;
        if(
            user('ynuv_can_approve_playlist') ||
            user('ynuv_can_delete_playlist_of_other_user') ||
            user('ynuv_can_edit_playlist_of_other_user') ||
            user('ynuv_can_feature_playlist')){
            $bShowEditMenu = true;
        }

        if((Phpfox::getUserId() == $aItem['user_id']) && (
            user('ynuv_can_delete_own_playlists') ||
            user('ynuv_can_edit_own_playlists'))){
            $bShowEditMenu = true;
        }

        $this->template()->assign([
            'aPitem'=>$aItem,
            'bShowEditMenu' => $bShowEditMenu,
            'bIsPagesView' => false,
            'sUrl'=> Phpfox::permalink('ultimatevideo.embed', $id, ''),
            'bIsDetailViewPlaylist' => true,
            ]);

        
        $this->setParam('aFeed', array(
                'comment_type_id' => 'ultimatevideo_playlist',
                'privacy' => $aItem['privacy'],
                'comment_privacy' => $aItem['privacy'],
                'like_type_id' => 'ultimatevideo_playlist',
                'feed_is_liked' => isset($aItem['is_liked']) ? $aItem['is_liked'] : false,
                'feed_is_friend' => $aItem['is_friend'],
                'item_id' => $aItem['playlist_id'],
                'user_id' => $aItem['user_id'],
                'total_comment' => $aItem['total_comment'],
                'feed_type'=>'ultimatevideo_playlist',
                'total_like' => $aItem['total_like'],
                'feed_link' => $aItem['bookmark_url'],
                'feed_title' => $aItem['title'],
                'feed_display' => 'view',
                'feed_total_like' => $aItem['total_like'],
                'report_module' => 'ultimatevideo_playlist',
                'report_phrase' => _p('Report this playlist'),
                'time_stamp' => $aItem['time_stamp'],
            )
        );

        $aCategories =  Phpfox::getService('ultimatevideo.category')->getCategoryAncestors($aItem['category_id'], self::MAX_CATEGORY_LEVEL);

        foreach($aCategories as $aItem){
            $this->template()->setBreadCrumb(\Core\Lib::phrase()->isPhrase($aItem['title']) ? _p($aItem['title']) : $aItem['title'], Phpfox::permalink('ultimatevideo.playlist.category', $aItem['category_id'], $aItem['title']));
        }
    }

}