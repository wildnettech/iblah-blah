<?php


namespace Apps\YouNet_UltimateVideos\Controller;

use Phpfox;
use Phpfox_Component;
use Phpfox_Locale;
use Phpfox_Module;
use Phpfox_Pager;
use Phpfox_Plugin;
use Phpfox_Error;
defined('PHPFOX') or exit('NO DICE!');


class AddController extends \Phpfox_Component
{
    public function process()
    {

        Phpfox::isUser(true);;
        \Core\Route\Controller::$name  = '';
        $bIsEdit = false;
        $sModule = $this->request()->get('module', false);
        $iItem = $this->request()->getInt('item', false);
        $sError =  null;
        
        if (!empty($sModule) && !empty($iItem))
        {
            $this->template()->assign(array(
                'sModule' => $sModule,
                'iItem' => $iItem
            ));
        }
        if (!empty($sModule) && ($sModule == "pages" || $sModule == "groups") && Phpfox::hasCallback($sModule, 'getItem'))
        {
            $aCallback = Phpfox::callback($sModule . '.getItem' , $iItem);
            $bCheckParentPrivacy = true;
            if (Phpfox::hasCallback($sModule, 'checkPermission')) {
                $bCheckParentPrivacy = Phpfox::callback($sModule . '.checkPermission' , $iItem, 'ultimatevideo.share_videos');
            }

            if (!$bCheckParentPrivacy)
            {
                $sError = _p('Unable to view this item due to privacy settings');
            }
        }
        if(!$sError && $iEditId = $this->_checkIsInEditVideo())
        {
            $bIsEdit = true;
            $aVideo = Phpfox::getService('ultimatevideo')->getVideoForEdit($iEditId);
            
            if(!$aVideo)
            {
                $sError = _p('Unable to find the video you are looking for.');
            }
            if(!user('ynuv_can_edit_own_video') && Phpfox::getUserId() == $aVideo['user_id'])
            {   
                $sError = _p('You do not have permission to edit your video.');
            }
            if(!user('ynuv_can_edit_video_of_other_user') && Phpfox::getUserId() != $aVideo['user_id']){
                $sError = _p('You do not have permission to edit video add by other user.');
            }
            if (Phpfox::isModule('tag'))
            {

                $aVideo['tag_list'] = '';                    

                $aTags = Phpfox::getService('tag')->getTagsById('ynultimatevideo', $aVideo['video_id']);

                if (isset($aTags[$iEditId]))
                {
                    foreach ($aTags[$iEditId] as $aTag)
                    {
                        $aVideo['tag_list'] .= ' ' . $aTag['tag_text'] . ',';    
                    }
                    $aVideo['tag_list'] = trim(trim($aVideo['tag_list'], ','));
                }
            }
            $this->template()->assign(array(
                        'aForms'=>$aVideo,
                        'iMaxFileSize'=> (user('ynuv_max_file_size_photos_upload') == 0) ? null : Phpfox::getLib('phpfox.file')->filesize((user('ynuv_max_file_size_photos_upload') / 1024) * 1048576),
                        ));
        }
        if(!$sError && Phpfox::getService('ultimatevideo')->countVideoOfUserId(Phpfox::getUserId()) >= user('ynuv_how_many_video_user_can_add') && !$bIsEdit){
            $sError = _p('You have reached your creating video limit. Please contact administrator.');
        }
        if (!$sError && !user('ynuv_can_upload_video') && !$bIsEdit) {
            $sError = _p('You do not have permission to upload a video. Please contact administrator.');
        }
        $bIsSpam = false;
        if(!$sError && user('ynuv_time_before_share_other_video',0) != 0)
        { 
            $iFlood = user('ynuv_time_before_share_other_video',0);
            $aFlood = array(
                'action' => 'last_post', // The SPAM action
                'params' => array(
                    'field' => 'time_stamp', // The time stamp field
                    'table' => Phpfox::getT('ynultimatevideo_videos'), // Database table we plan to check
                    'condition' => 'user_id = ' . Phpfox::getUserId(), // Database WHERE query
                    'time_stamp' => $iFlood * 60 // Seconds);   
                )
            );
                            
                // actually check if flooding
            if (Phpfox::getLib('spam')->check($aFlood))
            {   
                $bIsSpam = true;
                \Phpfox_Error::set(_p('Uploading video a little too soon.') . ' ' . Phpfox::getLib('spam')->getWaitTime()); 
            }
        }        
        $aValidationParam = $this->_getValidationParams();
        $corePath = Phpfox::getParam('core.path_actual').'PF.Site/Apps/YouNet_UltimateVideos';
        $isPass = true;
        $oValid = Phpfox::getLib('validator')->set(array(
                'sFormName' => 'ynuv_add_video_form',
                'aParams' => $aValidationParam
            )
        );

        if(!$sError && $this->_checkIfSubmittingAForm())
        {

            $aVals = $this->request()->getArray('val');

            $aValidationParam = $this->_getValidationParams();
            
            $oValid = Phpfox::getLib('validator')->set(array(
                    'sFormName' => 'ynuv_add_video_form',
                    'aParams' => $aValidationParam
                )
            );

            if(isset($aVals['upload_photo']) && $iEditedId = $this->_checkIsInEditVideo())
            {
                if(Phpfox::getService('ultimatevideo.process')->uploadVideoImage($iEditedId)){
                    $this->url()->send('ultimatevideo.add',array('id'=>$iEditedId,'photo' => '1'), _p('Photo Successfully Updated.'));
                }
            }
            else
            {

                if(user('ynuv_time_before_share_other_video',0) != 0)
                { 
                    $iFlood = user('ynuv_time_before_share_other_video',0);
                    $aFlood = array(
                        'action' => 'last_post', // The SPAM action
                        'params' => array(
                            'field' => 'time_stamp', // The time stamp field
                            'table' => Phpfox::getT('ynultimatevideo_videos'), // Database table we plan to check
                            'condition' => 'user_id = ' . Phpfox::getUserId(), // Database WHERE query
                            'time_stamp' => $iFlood * 60 // Seconds);   
                        )
                    );
                                    
                        // actually check if flooding
                    if (Phpfox::getLib('spam')->check($aFlood))
                    {
                        \Phpfox_Error::set(_p('Uploading video a little too soon.') . ' ' . Phpfox::getLib('spam')->getWaitTime()); 
                    }
                }
                if($this->_verifyCustomForm($aVals) && $oValid->isValid($aVals))
                {
                    
                    if (\Phpfox_Error::isPassed())
                    {
                         $aVals['video_path'] = "";
                        \Phpfox_Error::reset();
                        if($iEditedId = $this->_checkIsInEditVideo())
                        {
                            if($iVideoId = Phpfox::getService('ultimatevideo.process')->update($aVals,$iEditedId))
                            {
                                $this->url()->send('ultimatevideo.add',array('id'=>$iVideoId), _p('Your Video Successfully Updated.'));
                            }
                        }
                        else
                        {
                            //integrate with business and contest
                            $aVals['callback_module'] = $this->request()->get('module',false);
                            $aVals['callback_item_id'] = $this->request()->getInt('item',false);

                            if($aVals['video_source'] == "Uploaded"){

                                $maxFileSize = user('ynuv_file_size_limit_in_megabytes',0);
                                if(isset($_FILES['videoUpload']['name']) && empty($_FILES['videoUpload']['name']) || (isset($_FILES['videoUpload']['size']) && (int)$_FILES['videoUpload']['size'] <= 0)){
                                    $isPass = false;
                                    \Phpfox_Error::set(_p('No files found or file is not valid. Please try again.'));
                                }
                                else{
                                    $aVideo = Phpfox::getLib('file')->load('videoUpload',[], $maxFileSize);

                                    if($aVideo){
                                        //upload video file 
                                        $aVals['video_code'] = substr($_FILES['videoUpload']['type'],strpos($_FILES['videoUpload']['type'],'/') + 1);
                                        $filePath = PHPFOX_DIR_FILE . 'ynultimatevideo' . PHPFOX_DS;
                                        if (!is_dir($filePath)) {
                                            if (!@mkdir($filePath, 0777, 1)) {

                                            }
                                        }
                                        $videoFilePath = PhpFox::getLib('file')->upload('videoUpload', $filePath, $_FILES['videoUpload']['name'],true,0644,true,false);
                                        $aVals['video_path'] = ($videoFilePath) ? $videoFilePath : "";
                                    }
                                    else{
                                        $isPass = false;
                                    } 
                                }    
                            }
                            if($isPass)
                            {   
                                if($iVideoId = Phpfox::getService('ultimatevideo.process')->add($aVals))
                                {
                                    if($aVals['video_source'] == "Uploaded")
                                    {
                                        if(isset($aVals['allow_upload_channel']))
                                        {
                                            $this->url()->send('ultimatevideo.oauth2',$iVideoId,null);
                                        }
                                        else{
                                            $this->url()->send('ultimatevideo',['view'=>'my'], _p('Your Video Successfully Added.'));
                                        }
                                    }
                                    else
                                    {
                                        $this->url()->send('ultimatevideo',$iVideoId, _p('Your Video Successfully Added.'));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $aCallback = false;
        if (!$sError && $sModule !== false && $iItem !== false && Phpfox::hasCallback($sModule, 'getVideoDetails'))
        {
            if ($aCallback = Phpfox::callback($sModule . '.getVideoDetails', array('item_id' => $iItem)))
            {
                $this->template()
                        ->setBreadcrumb($aCallback['breadcrumb_title'], $aCallback['breadcrumb_home'])
                        ->setBreadcrumb($aCallback['title'], $aCallback['url_home']);
                if ($sModule == 'pages' && !Phpfox::getService('pages')->hasPerm($iItem, ''))
                {
                    $sError = _p('Unable to view this item due to privacy setting');
                }
            }
        }

        if($sError){
            $this->template()
                ->setBreadCrumb(_p('Ultimate Videos'),Phpfox::permalink('ultimatevideo',null,false));
        }else{
            $this->template()->setBreadCrumb(_p('Ultimate Videos'),Phpfox::permalink('ultimatevideo',null,false))
                ->setBreadCrumb((!$bIsEdit) ? _p('Share A Video'): _p('Edit Video'),Phpfox::permalink('ultimatevideo.add',($bIsEdit) ? 'id_'.$iEditId : null,null,false))
                ->setTitle((!$bIsEdit) ? _p('Share A Video') : _p('Edit Video'))
                ->setBreadcrumb(($bIsEdit ? _p('Manage Video') . ': ' . $aVideo['title'] : !$bIsSpam ? _p('Create A New Video') : ""), ($bIsEdit ? $this->url()->makeUrl('ultimatevideo.add', array('id' => $aVideo['video_id'])) : $this->url()->makeUrl('ultimatevideo.add')), true)
                ->setEditor(array('wysiwyg' => true))
                ->setHeader('cache', array(
                    'pager.css' => 'style_css',
                    'jquery/plugin/jquery.highlightFade.js' => 'static_script',
                    'switch_legend.js' => 'static_script',
                    'switch_menu.js' => 'static_script',
                    'quick_edit.js' => 'static_script',
                    'progress.js' => 'static_script',

                ));
            $this->template()->assign(array(
                'sCreateJs' => $oValid->createJS(),
                'sGetJsForm' => $oValid->getJsForm(),
                'sCategories' => Phpfox::getService('ultimatevideo.category')->get(),
                'corePath' => $corePath,
                'sModule' => '',
                'bIsEdit' => $bIsEdit
            ));
        }
        $this->template()->setHeader('cache', array(
                        '<script type="text/javascript">
                            var isInitAddVideo = false;
                            $Behavior.ultimatevideoAddVideoOnLoad = function() {
                                if(isInitAddVideo == false){
                                    ultimatevideoAddVideoInit = window.setInterval(function(){
                                        if(typeof ultimatevideo == \'undefined\'){
                                        }else{
                                            if(isInitAddVideo == false){
                                                isInitAddVideo = true;
                                                ultimatevideo.ultimatevideoAddVideo();
                                                window.clearInterval(ultimatevideoAddVideoInit);
                                            }
                                            else
                                            {
                                                window.clearInterval(ultimatevideoAddVideoInit);
                                            }
                                        }
                                    },200);
                                }
                            }
                        </script>'
                    ));
        if(!$sError && $bIsEdit)
        {
            $this->template()->setHeader('cache', array(
                '<script type="text/javascript">
                    var isInitAddVideoCategory = false;
                    $Behavior.ultimatevideoEditCategory = function() {
                        var aCategories = JSON.parse(\'' . $aVideo['categories'] . '\');
                        var categorySection;

                        for (var i = 0; i < aCategories.length; i++) {
                            
                            categorySection = $(\'#ynuv_section_category\');
                            $(categorySection).find(\'#js_mp_category_item_\' + aCategories[i]).attr(\'selected\', true);
                            $(categorySection).find(\'#js_mp_holder_\' + aCategories[i]).show();
                        }
                        if(isInitAddVideoCategory == false){
                            ultimatevideoAddVideoCategoryInit = window.setInterval(function(){
                                            if(typeof ultimatevideo == \'undefined\'){
                                            }else{
                                                if(isInitAddVideoCategory == false){
                                                    isInitAddVideoCategory = true;
                                                    ultimatevideo.changeCustomFieldByCategory(aCategories[aCategories.length-1]);
                                                    window.clearInterval(ultimatevideoAddVideoCategoryInit);
                                                }
                                                else
                                                {
                                                    window.clearInterval(ultimatevideoAddVideoCategoryInit);
                                                }
                                            }
                            },200);
                        }
                    }
                </script>'
            ));
        }
        $this->template()->assign('bNoAttachaFile', true);
        if (!$sError && Phpfox::isModule('attachment')) {
            $this->setParam(array('attachment_share' => array(
                    'type' => 'ultimatevideo',
                    'id' => 'ynuv_add_video_form',
                    'edit_id' => ($bIsEdit ? $this->request()->getInt('id') : 0),
                    'inline' => false
                )
                )
            );
        }

        $this->template()->assign([
            'sError'=>$sError,
            'bIsSpam' => $bIsSpam,
        ]);
    }
    /**
     * check validator for form
     * @by : hainm
     * @param array $aVals
     * @return array
     */
    private function _getValidationParams($aVals = array()) {
        if(!$this->_checkIsInEditVideo()){
            $aParam = array(
                'title' => array(
                    'def' => 'required',
                    'title' => _p('Video title cannot be empty'),
                ),
                'video_link' => array(
                    'def' => 'required',
                    'title' => _p('Video url cannot be empty'),
                ),
                'video_code' => array(
                    'def' => 'required',
                    'title' => _p('Invalid Video URL. Please try again.'),
                ),
                'video_embed' => array(
                    'def' => 'required',
                    'title' => _p('Video embed code cannot be empty')
                )
            );
        }
        else{
            $aParam = array(
                'title' => array(
                    'def' => 'required',
                    'title' => _p('Video title cannot be empty'),
                ),
            );
        }

        return $aParam;
    }    
    private function _checkIfSubmittingAForm() {
        if ($this->request()->getArray('val')) {
            return true;
        } else {
            return false;
        }
    }
    private function _checkIsInEditVideo() {
        if ($this->request()->getInt('id')) {
            $iEditedVideoId = $this->request()->getInt('id');
            return $iEditedVideoId;
        } else {
            return false;
        }
    }
    private function _verifyCustomForm($aVals)
    {
        if(isset($aVals['custom'])) {
            $aFieldValues = $aVals['custom'];

            $aFields =Phpfox::getService('ultimatevideo.custom')->getCustomField();
            
            foreach($aFields as $k=>$aField)
            { 
                if( $aField['is_required'] && isset($aFieldValues[$aField['field_id']]) && empty($aFieldValues[$aField['field_id']]) ) 
                {
                    return \Phpfox_Error::set(_p('Custom field is required: ').Phpfox::getPhrase($aField['phrase_var_name']));
                }
            } 

        }
        return true;

    }    
}