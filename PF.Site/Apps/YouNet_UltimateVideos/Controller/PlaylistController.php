<?php
/**
 * Created by PhpStorm.
 * User: davidnguyen
 * Date: 7/26/16
 * Time: 10:06 AM
 */

namespace Apps\YouNet_UltimateVideos\Controller;

use Core\Route\Controller;
use Phpfox;
use Phpfox_Component;
use Phpfox_Locale;
use Phpfox_Module;
use Phpfox_Pager;
use Phpfox_Plugin;
defined('PHPFOX') or exit('NO DICE!');


class PlaylistController extends Phpfox_Component
{
    public function process()
    {
        $bIsSearch = false;
        if(defined('PHPFOX_IS_USER_PROFILE')){
            $bIsSearch = true;
            Controller::$name =  true;
        }else{
            
            Controller::$name =  '';
        }
        $aCheckView = ['my','favorite','later','history','friend'];
        if(in_array($this->request()->get('req3'), $aCheckView)){
            Phpfox::isUser(true);
        }
        if($this->request()->get('req3') == 'pending'){
            $this->url()->send('ultimatevideo.playlist',['view'=>'pending']);
            return;
        }

        if($this->request()->get('req3') == 'my'){
            Phpfox::isUser(true);
            $this->url()->send('ultimatevideo.playlist',['view'=>'my']);
            return;
        }
        if($this->request()->get('req3') == 'friend'){
            Phpfox::isUser(true);
            $this->url()->send('ultimatevideo.playlist',['view'=>'friend']);
            return;
        }

        $bIsUserProfile = $this->getParam('bIsProfile');
        $bShowModeration = (user('ynuv_can_delete_playlist_of_other_user') || user('ynuv_can_feature_playlist'));
        $aUser = [];
        if ($bIsUserProfile) {
            $aUser = $this->getParam('aUser');
        }
        $sView = $this->request()->get('view');
        if (defined('PHPFOX_IS_AJAX_CONTROLLER')) {
            $bIsProfile = true;
            $aUser = Phpfox::getService('user')->get($this->request()->get('profile_id'));
            $this->setParam('aUser', $aUser);
        } else {
            $bIsProfile = $this->getParam('bIsProfile');
            if ($bIsProfile === true) {
                $aUser = $this->getParam('aUser');
            }
        }

        Phpfox::getService('ultimatevideo.callback')->buildFilterMenu($this);

        if(defined('PHPFOX_IS_USER_PROFILE') || defined('PHPFOX_IS_PAGES_VIEW')){
            $bIsSearch = true;
            Controller::$name =  true;
        }else{
            Controller::$name =  '';
        }


        $this->search()->set([
                'type'        => 'ultimatevideo_playlists',
                'field'       => 'playlist.playlist_id',
                'search_tool' => [
                    'table_alias' => 'playlist',
                    'search'      => [
                        'action'        => ($bIsProfile === true ? $this->url()->makeUrl($aUser['user_name'], ['ultimatevideo', 'view' => $this->request()->get('view')]) : $this->url()->makeUrl('ultimatevideo.playlist', ['view' => $this->request()->get('view')])),
                        'default_value' => _p('Search playlists'),
                        'name'          => 'search',
                        'field'         => 'playlist.title',
                    ],
                    'sort'        => [
                        'latest'     => ['playlist.time_stamp', _p('Latest')],
                        'most-viewed' => ['playlist.total_view',_p('Most Viewed')],
                        'most-liked' => ['playlist.total_like', _p('Most Liked')],
                        'most-commented' => ['playlist.total_comment',_p('Most Commented')],
                        'featured'  => ['playlist.playlist_id',_p('Featured')]
                    ],
                    'show'        => [10, 15, 20],
                ],
            ]
        );

        $aBrowseParams = [
            'module_id' => 'ultimatevideo.playlist',
            'alias'     => 'playlist',
            'field'     => 'playlist_id',
            'table'     => Phpfox::getT('ynultimatevideo_playlists'),
            'hide_view' => ['pending', 'my'],
        ];

        sectionMenu(_p('Create A Playlist'), url('/ultimatevideo/addplaylist'));


        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            foreach(['sort','show','when','view','s','user','tag'] as $temp){
                if(!empty($_GET[$temp])){
                    $bIsSearch = true;
                }
            }
        }

        if ($this->request()->get('req3') == 'my_playlist'){
            $bIsSearch = true;
        }
        if($this->request()->get('sort') && $this->request()->get('sort') == 'featured')
        {
            $this->search()->setCondition(' AND playlist.is_featured=1 ');
        }
        $sView = $this->request()->get('view');
        if($sView) 
            $bIsSearch = true;

        switch($sView){
            case 'my':
            case 'myplaylist':
                Phpfox::isUser(true);
                $this->search()->setCondition(' AND playlist.user_id='.intval(Phpfox::getUserId()));
                break;
            case 'pendingplaylist':
                Phpfox::isUser(true);
                $this->search()->setCondition(' AND playlist.is_approved=0');
                break;
            case 'featured':
                Phpfox::isUser(true);
                $this->search()->setCondition(' AND playlist.is_featured=1');
                break;
            case 'historyplaylist':
                Phpfox::isUser(true);
                $bShowModeration = false;
                break; 
            case 'friendplaylist':
                Phpfox::isUser(true);   
                break;    
            default:
                if(defined('PHPFOX_IS_USER_PROFILE'))
                {
                    $this->search()->setCondition(' AND playlist.user_id='.intval($aUser['user_id']));  
                    break;                  
                }
                $this->search()->setCondition(' AND playlist.is_approved=1');
                break;
        }
        $iFromUser = $this->request()->get('user');
        if($iFromUser)
        {
            $this->search()->setCondition(' AND playlist.user_id='.intval($iFromUser));
        }        
        $sCategory = null;
        if ($this->request()->get(($bIsProfile ? 'req4' : 'req3')) == 'category'){
            $sCategory = $this->request()->getInt($bIsProfile ? 'req5' : 'req4');
            $bIsSearch = true;
            $sCategoryList =  Phpfox::getService('ultimatevideo.category')->getCategoryIdDescendants($sCategory);
            $this->search()->setCondition('AND playlist.category_id IN( ' . $sCategoryList. ')');
        }

        $aItems =  null;
        if($bIsSearch){

            $this->search()->browse()->params($aBrowseParams)->execute();

            $aItems = $this->search()->browse()->getRows();

            Phpfox_Pager::instance()->set(array('page' => $this->search()->getPage(), 'size' => $this->search()->getDisplay(), 'count' => $this->search()->browse()->getCount()));
        }

        if($bShowModeration){
            $aModerationMenu =  [];
            if(user('ynuv_can_approve_playlist') && $sView == 'pendingplaylist'){
                $aModerationMenu[] = [
                    'phrase' => _p('Approve'),
                    'action' => 'approve'
                ];
            }
            if(user('ynuv_can_delete_playlist_of_other_user')){
                $aModerationMenu[] = [
                    'phrase' => Phpfox::getPhrase('core.delete'),
                    'action' => 'delete'
                ];
            }

            if(user('ynuv_can_feature_playlist')){
                $aModerationMenu[] = [
                    'phrase' => _p('Feature'),
                    'action' => 'feature'
                ];
            }
            if($sView == 'historyplaylist'){
                $aModerationMenu[] = [
                    'phrase' => _p('Remove all from history'),
                    'action' => 'history'
                ];
            }
            $this->setParam('global_moderation', [
                'name' => 'ultimatevideo_playlist',
                'ajax' => 'ultimatevideo.playlist_moderation',
                'menu' => $aModerationMenu
            ]);
        }
        if(count($aItems))
        {
            foreach ($aItems as $key => $aPlaylist) {
                $aItems[$key]['video_list'] = Phpfox::getService('ultimatevideo.playlist.browse')->getSomeVideoOfPlaylist($aPlaylist['playlist_id']);
            }
        }
        $bIsNoItem = false;
        if(!$bIsSearch)
        {
            if(!Phpfox::getService('ultimatevideo.playlist.browse')->getSlideshowPlaylists(5) && !Phpfox::getService('ultimatevideo.playlist.browse')
            ->getMostRecentPlaylists(5)){
                $bIsNoItem = true;
            }
        }
        $this->setParam('sCategory', $sCategory);
        $this->template()->assign([
            'sView'=>$sView,
            'aItems'=>$aItems,
            'bVideoView'=>true,
            'bIsSearch'=> $bIsSearch,
            'bShowTotalView'=> true,
            'bShowTotalLike'=> true,
            'bShowModeration' => $bShowModeration,
            'bIsUserProfile' => $bIsUserProfile,
            'iPage' => $this->request()->get('page'),
            'bIsNoItem' => $bIsNoItem,
        ]);

        $this->template()->setBreadCrumb(_p('Ultimate Videos'),Phpfox::permalink('ultimatevideo',null,false));
    }
}