<?php


namespace Apps\YouNet_UltimateVideos\Controller;

use Phpfox;
use Phpfox_Component;
use Phpfox_Locale;
use Phpfox_Module;
use Phpfox_Pager;
use Phpfox_Plugin;
use Phpfox_Error;
defined('PHPFOX') or exit('NO DICE!');


class AddPlaylistController extends \Phpfox_Component
{
    public function process()
    { 
        Phpfox::isUser(true);
        \Core\Route\Controller::$name  = '';
        $bIsEdit = false;
        $sError =  null;
        if($iEditId = $this->_checkIsInEditPlaylist())
        {
        	$bIsEdit = true;
            $aPlaylist = Phpfox::getService('ultimatevideo.playlist')->getForEdit($iEditId);

            if(!$aPlaylist)
            {
                $sError = _p('Unable to find the playlist you are looking for.');
            }
            if(!user('ynuv_can_edit_own_playlists') && Phpfox::getUserId() == $aPlaylist['user_id'])
            {   
                $sError = _p('You do not have permission to edit your playlist.');
            }
            if(!user('ynuv_can_edit_playlist_of_other_user') && Phpfox::getUserId() != $aPlaylist['user_id']){
                $sError = _p('You do not have permission to edit playlist add by other user.');
            }
            $aVideos = Phpfox::getService('ultimatevideo.playlist')->getVideosManage($iEditId);
            $this->template()->assign(array(
                    'aVideos'=>$aVideos,
                    'aForms'=>$aPlaylist,
                    'iMaxFileSize'=> (user('ynuv_max_file_size_photos_upload') == 0) ? null : Phpfox::getLib('phpfox.file')->filesize((user('ynuv_max_file_size_photos_upload') / 1024) * 1048576),
                    ));
        }
        $aValidationParam = $this->_getValidationParams();
        $oValid = Phpfox::getLib('validator')->set(array(
                'sFormName' => 'ynuv_add_playlist_form',
                'aParams' => $aValidationParam
            )
        );
        if (!$sError && !user('ynuv_can_add_playlist') && !$bIsEdit) {
            $sError = _p('You do not have permission to add a playlist. Please contact administrator.');
        }
        if (!$sError && $this->request()->getArray('order') && $iEditedId = $this->_checkIsInEditPlaylist())
        {
            $aOrder = $this->request()->getArray('order');
            Phpfox::getService('ultimatevideo.playlist.process')->updateOrder($aOrder,$iEditId);
            $sRemovedVideo = $this->request()->get('removed');
            if(!empty($sRemovedVideo))
            {
                $aRemoved = explode(',',$sRemovedVideo);
                foreach ($aRemoved as $key => $iVideoId) {
                    Phpfox::getService('ultimatevideo.playlist.process')->removeVideo($iVideoId,$iEditedId);
                }
            }
            $this->url()->send('ultimatevideo.addplaylist',array('id'=>$iEditedId,'video' => '1'), _p('Videos List Successfully Updated.'));
        }        
        if(!$sError && $this->_checkIfSubmittingAForm())
        {
            $aVals = $this->request()->getArray('val');

            $aValidationParam = $this->_getValidationParams();
            
            $oValid = Phpfox::getLib('validator')->set(array(
                    'sFormName' => 'ynuv_add_playlist_form',
                    'aParams' => $aValidationParam
                )
            );
            if(isset($aVals['upload_photo']) && $iEditedId = $this->_checkIsInEditPlaylist())
            {
                if(Phpfox::getService('ultimatevideo.playlist.process')->uploadImage($iEditedId)){
                    $this->url()->send('ultimatevideo.addplaylist',array('id'=>$iEditedId,'photo' => '1'), _p('Photo Successfully Updated.'));
                }

            }
            else
            {
            	if($oValid->isValid($aVals))
                {
                    
                    if (\Phpfox_Error::isPassed())
                    {
                        if($iEditedId = $this->_checkIsInEditPlaylist())
                        {
                            if($iPlaylistId = Phpfox::getService('ultimatevideo.playlist.process')->update($aVals,$iEditedId))
                            {
                                $this->url()->send('ultimatevideo.addplaylist',array('id'=>$iPlaylistId), _p('Your Playlist Successfully Updated.'));
                            }
                        }
                        else
                        {

                            if($iPlaylistId = Phpfox::getService('ultimatevideo.playlist.process')->add($aVals))
                            {
                                $this->url()->send('ultimatevideo.playlist',$iPlaylistId, _p('Your Playlist Successfully Added.'));
                            }    
                        }
                    }
                }
            }
        }
    	$this->template()->setBreadCrumb(_p('Ultimate Videos'),Phpfox::permalink('ultimatevideo.playlist',null,false))->setBreadCrumb((!$bIsEdit) ? _p('Create New Playlist'): _p('Edit Playlist'),Phpfox::permalink('ultimatevideo.addplaylist',($bIsEdit) ? 'id_'.$iEditId : null,null,false))
            ->setBreadcrumb(($bIsEdit ? _p('Manage Playlist') . ': ' . $aPlaylist['title'] : _p('Create A New Playlist')), ($bIsEdit ? $this->url()->makeUrl('ultimatevideo.addplaylist', array('id' => $aPlaylist['playlist_id'])) : $this->url()->makeUrl('ultimatevideo.addplaylist')), true)
            ->setEditor(array('wysiwyg' => true))
            ->setTitle((!$bIsEdit) ? _p('Create New Playlist'): _p('Edit Playlist'))
    					->setEditor(array('wysiwyg' => true))
    	                ->setHeader('cache', array(
                    'pager.css' => 'style_css',
                    'jquery/plugin/jquery.highlightFade.js' => 'static_script',
                    'switch_legend.js' => 'static_script',
                    'switch_menu.js' => 'static_script',
                    'quick_edit.js' => 'static_script',
                    'progress.js' => 'static_script',
                    'share.js' => 'module_attachment',
                    
                )); 
        $corePath = Phpfox::getParam('core.path_actual').'PF.Site/Apps/YouNet_UltimateVideos';
    	$this->template()->assign(array(
            'sCreateJs' => $oValid->createJS(),
            'sGetJsForm' => $oValid->getJsForm(),
    		'sCategories' => Phpfox::getService('ultimatevideo.category')->get(),
    		'corePath' => $corePath,
    		'sModule' => '',
            'bIsEdit' => $bIsEdit
    		));
        $this->template()->setHeader('cache', array(
                        '<script type="text/javascript">
                            var isInitAddPlaylist = false;
                            $Behavior.ultimatevideoAddPlaylistOnLoad = function() {
                                if(isInitAddPlaylist == false){
                                    setTimeout(function(){
                                        if(typeof ultimatevideo_playlist == \'undefined\'){
                                        }else{
                                            isInitAddPlaylist = true;
                                            ultimatevideo_playlist.ultimatevideoAddPlaylist();

                                        }
                                    },250);
                                }
                            }
                        </script>'
                    ));        
        if(!$sError && $bIsEdit)
        {
            $this->template()->setHeader('cache', array(
                '<script type="text/javascript">
                    $Behavior.ultimatevideoEditCategory = function() {
                        var aCategories = JSON.parse(\'' . $aPlaylist['categories'] . '\');
                        var categorySection;

                        for (var i = 0; i < aCategories.length; i++) {
                            
                            categorySection = $(\'#ynuv_section_category\');
                            $(categorySection).find(\'#js_mp_category_item_\' + aCategories[i]).attr(\'selected\', true);
                            $(categorySection).find(\'#js_mp_holder_\' + aCategories[i]).show();
                        }

                    }
                </script>'
            ));
        }  
        if (Phpfox::isModule('attachment')) {
            $this->template()->assign(array('aAttachmentShare' => array(
                    'type' => 'ultimatevideo',
                    'id' => 'ynuv_add_video_form',
                    'edit_id' => 0,
                    'inline' => false
                )
                )
            );
        }
        $this->template()->assign([
            'sError'=>$sError
        ]);          		                           
    }
    private function _checkIfSubmittingAForm() {
        if ($this->request()->getArray('val')) {
            return true;
        } else {
            return false;
        }
    }
    private function _checkIsInEditPlaylist() {
        if ($this->request()->getInt('id')) {
            $iEditedPlaylistId = $this->request()->getInt('id');
            return $iEditedPlaylistId;
        } else {
            return false;
        }
    }
    private function _getValidationParams($aVals = array()) {
        $aParam = array(
            'title' => array(
                'def' => 'required',
                'title' => _p('Playlist name cannot be empty'),
            )
        );

        return $aParam;
    }       
}