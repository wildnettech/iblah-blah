<?php

$video_phrases = [
	'video' => _p('Videos'),
	'say' => _p('Say something about this video...'),
	'save' => _p('Save'),
	'no_friends_found' => _p('No friends found'),
	'share' => \_p('share'),
    'not_valid_video'=> _p('Please select an video or paste a valid video link')
];

$sData .= '<script>var uv_phrases = ' . json_encode($video_phrases) . ';</script>';