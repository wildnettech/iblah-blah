<?php

/**
 * @param bool $flag
 * @return string
 */
function ultimatevideo_video_view_mode($flag = false){
    if(! $flag && false == \Phpfox_Template::instance()->getVar('bMultiViewMode')){
        return '';
    }
    return '<div class="pull-right ultimatevideo-modeviews show_grid_view">
            <span title="Grid View" data-toggle="ultimatevideo" data-cmd="show_grid_view"><i class="ynicon yn-grid-view"></i></span>
            <span title="Casual View" data-toggle="ultimatevideo" data-cmd="show_casual_view"><i class="ynicon yn-casual-view"></i></span>
            </div>';
}

/**
 * @return string
 */
function ultimatevideo_playlist_view_mode(){
    if(false == \Phpfox_Template::instance()->getVar('bMultiViewMode')){
        return '';
    }
    return '<div class="pull-right ultimatevideo-modeviews show_grid_view">
            <span title="Grid View" data-toggle="ultimatevideo" data-cmd="show_grid_view"><i class="ynicon yn-grid-view"></i></span>
            <span title="List View" data-toggle="ultimatevideo" data-cmd="show_list_view"><i class="ynicon yn-list-view"></i></span>
            </div>';
}

/**
 * @param $offsetCount
 * @return string
 */
function ultimatevideo_mode_view_video_format($offsetCount){
    $totalCount =  count(Phpfox_Template::instance()->getVar('aItems'));
    $MAX = 3;
    $total_row =  ceil($totalCount/$MAX);
    $current_row = ceil($offsetCount/$MAX);
    $current_row_total =  $current_row< $total_row? $MAX: ($totalCount -  ($total_row-1)* $MAX);
    $current_offset =  ($offsetCount - 1)%$MAX;
    return 'row-number-'.($current_row%2).' row-total-'. $current_row_total .' row-offset-'. $current_offset;
}

/**
 * @param $value
 * @return string
 */
function ultimatevideo_duration($value){
    $value = intval($value);

    if($value <= 0){
        return '';
    }

    $hour =  floor($value/3600);
    $min =  floor(($value - $hour*3600)/60);
    $second =  $value - $hour * 3600 - $min * 60;
    $result = [];

    if($hour){
        $result[] = str_pad($hour,2,'0', STR_PAD_LEFT);
    }
    $result[] = str_pad($min,2, '0',STR_PAD_LEFT);
    $result[] = str_pad($second,2,'0', STR_PAD_LEFT);

    return implode(':', $result);
}

/**
 * @param $value
 * @param int $id
 * @return string
 */
function ultimatevideo_rating($value, $id = 0){
    $value =  floor($value+0.4999);
    $result = [];
    $bCanEdit = $id > 0 && Phpfox::getUserId() > 0;

    for($i =1; $i<=5; ++$i){
        $edit =  $bCanEdit?'data-toggle="ultimatevideo" data-cmd="rate_video" data-id="'.$id.'" data-value="'.$i.'"':'';
        $result[] =  $i <= $value?'<i class="rate_video fa fa-star" '.$edit.'></i>':'<i class="rate_video fa fa-star disable" '.$edit.'></i>';
    }

    return implode('', $result);
}

/**
 * @param $id
 * @return mixed
 */
function ultimatevideo_favourite($id){
    return Phpfox::getService('ultimatevideo.favorite')->isFavorite(Phpfox::getUserId(),$id);
}

/**
 * @param $id
 * @return mixed
 */
function ultimatevideo_watchlater($id){
    return Phpfox::getService('ultimatevideo.watchlater')->isWatchLater(Phpfox::getUserId(),$id);
}

/**
 *
 */
event('app_settings', function ($settings){
    if (isset($settings['ynuv_app_enabled'])) {
        Phpfox::getService('admincp.module.process')->updateActivity('ultimatevideo', $settings['ynuv_app_enabled']);
    }
});

\Phpfox_Module::instance()->addServiceNames([
	'ultimatevideo' => '\Apps\YouNet_UltimateVideos\Service\Ultimatevideo',
    'ultimatevideo.callback' => '\Apps\YouNet_UltimateVideos\Service\Callback',
	'ultimatevideo.process' => '\Apps\YouNet_UltimateVideos\Service\Process',
    'ultimatevideo.browse' => '\Apps\YouNet_UltimateVideos\Service\Browse',
    'ultimatevideo.history' => '\Apps\YouNet_UltimateVideos\Service\History',
    'ultimatevideo.favorite' => '\Apps\YouNet_UltimateVideos\Service\Favourite',
    'ultimatevideo.watchlater' => '\Apps\YouNet_UltimateVideos\Service\Watchlaters',
    'ultimatevideo.playlist' => '\Apps\YouNet_UltimateVideos\Service\Playlist\Playlist',
    'ultimatevideo.playlist.process' => '\Apps\YouNet_UltimateVideos\Service\Playlist\Process',
    'ultimatevideo.playlist.browse' => '\Apps\YouNet_UltimateVideos\Service\Playlist\Browse',
	'ultimatevideo.category' => '\Apps\YouNet_UltimateVideos\Service\Category\Category',
	'ultimatevideo.category.process' => '\Apps\YouNet_UltimateVideos\Service\Category\Process',
	'ultimatevideo.multicat' => '\Apps\YouNet_UltimateVideos\Service\Multicat',
	'ultimatevideo.custom' => '\Apps\YouNet_UltimateVideos\Service\Custom\Custom',
	'ultimatevideo.custom.group' => '\Apps\YouNet_UltimateVideos\Service\Custom\Group',
	'ultimatevideo.custom.process' => '\Apps\YouNet_UltimateVideos\Service\Custom\Process',
    'ultimatevideo.rating' => '\Apps\YouNet_UltimateVideos\Service\Rating',
])->addComponentNames('block',[
    'ultimatevideo.editcategory' => '\Apps\YouNet_UltimateVideos\Block\EditCategoryBlock',
    'ultimatevideo.editcustomfield' => '\Apps\YouNet_UltimateVideos\Block\EditCustomFieldBlock',
    'ultimatevideo.popup_customfield_category' => '\Apps\YouNet_UltimateVideos\Block\PopupCustomFieldCategoryBlock',
    'ultimatevideo.custom.form' => '\Apps\YouNet_UltimateVideos\Block\Custom\FormBlock',
    'ultimatevideo.custom.view' => '\Apps\YouNet_UltimateVideos\Block\Custom\ViewBlock',
    'ultimatevideo.tags'=>'\Apps\YouNet_UltimateVideos\Block\TagsBlock',
    'ultimatevideo.most_liked'=>'\Apps\YouNet_UltimateVideos\Block\MostLikedVideoBlock',
    'ultimatevideo.most_viewed'=>'\Apps\YouNet_UltimateVideos\Block\MostViewedVideoBlock',
    'ultimatevideo.most_commented'=>'\Apps\YouNet_UltimateVideos\Block\MostCommentedVideoBlock',
    'ultimatevideo.slideshow'=>'\Apps\YouNet_UltimateVideos\Block\SlideshowVideoBlock',
    'ultimatevideo.tags_video'=>'\Apps\YouNet_UltimateVideos\Block\TagsVideoBlock',
    'ultimatevideo.tags_playlist'=>'\Apps\YouNet_UltimateVideos\Block\TagsPlaylistBlock',
    'ultimatevideo.most_liked_video'=>'\Apps\YouNet_UltimateVideos\Block\MostLikedVideoBlock',
    'ultimatevideo.most_liked_playlist'=>'\Apps\YouNet_UltimateVideos\Block\MostLikedPlaylistBlock',
    'ultimatevideo.most_viewed_video'=>'\Apps\YouNet_UltimateVideos\Block\MostViewedVideoBlock',
    'ultimatevideo.most_viewed_playlist'=>'\Apps\YouNet_UltimateVideos\Block\MostViewedPlaylistBlock',
    'ultimatevideo.most_commented_video'=>'\Apps\YouNet_UltimateVideos\Block\MostCommentedVideoBlock',
    'ultimatevideo.most_commented_playlist'=>'\Apps\YouNet_UltimateVideos\Block\MostCommentedPlaylistBlock',
    'ultimatevideo.recommended_video'=>'\Apps\YouNet_UltimateVideos\Block\RecommendedVideoBlock',
    'ultimatevideo.recommended_playlist'=>'\Apps\YouNet_UltimateVideos\Block\RecommendedPlaylistBlock',
    'ultimatevideo.category'=>'\Apps\YouNet_UltimateVideos\Block\CategoryBlock',
    'ultimatevideo.featured_video'=>'\Apps\YouNet_UltimateVideos\Block\FeaturedVideoBlock',
    'ultimatevideo.featured_playlist'=>'\Apps\YouNet_UltimateVideos\Block\FeaturedPlaylistBlock',
    'ultimatevideo.watch_it_again'=>'\Apps\YouNet_UltimateVideos\Block\WatchItAgainBlock',
    'ultimatevideo.recent_video'=>'\Apps\YouNet_UltimateVideos\Block\RecentVideoBlock',
    'ultimatevideo.recent_playlist'=>'\Apps\YouNet_UltimateVideos\Block\RecentPlaylistBlock',
    'ultimatevideo.related_video'=>'\Apps\YouNet_UltimateVideos\Block\RelatedVideoBlock',
    'ultimatevideo.related_playlist'=>'\Apps\YouNet_UltimateVideos\Block\RelatedPlaylistBlock',
    'ultimatevideo.slideshow_video'=>'\Apps\YouNet_UltimateVideos\Block\SlideshowVideoBlock',
    'ultimatevideo.slideshow_playlist'=>'\Apps\YouNet_UltimateVideos\Block\SlideshowPlaylistBlock',
    'ultimatevideo.user_posted_video'=>'\Apps\YouNet_UltimateVideos\Block\UserPostedVideoBlock',
    'ultimatevideo.user_posted_playlist'=>'\Apps\YouNet_UltimateVideos\Block\UserPostedPlaylistBlock',
    'ultimatevideo.user_playlist_checklist'=>'\Apps\YouNet_UltimateVideos\Block\UserPlaylistChecklistBlock',
    'ultimatevideo.feed_video'=>'\Apps\YouNet_UltimateVideos\Block\FeedVideoBlock',
    'ultimatevideo.editvideomenu'   => '\Apps\YouNet_UltimateVideos\Block\EditVideoMenuBlock',
    'ultimatevideo.editplaylistmenu'   => '\Apps\YouNet_UltimateVideos\Block\EditPlaylistMenuBlock',
    'ultimatevideo.playlist_detail_mode_listing'   => '\Apps\YouNet_UltimateVideos\Block\PlaylistDetailModeListingBlock',
    'ultimatevideo.playlist_detail_mode_slide'   => '\Apps\YouNet_UltimateVideos\Block\PlaylistDetailModeSlideBlock',
    'ultimatevideo.feed_playlist'=>'\Apps\YouNet_UltimateVideos\Block\FeedPlaylistBlock',
])->addComponentNames('controller', [
	'ultimatevideo.admincp.category.index' => '\Apps\YouNet_UltimateVideos\Controller\Admin\Category\IndexController',
	'ultimatevideo.admincp.category.add' => '\Apps\YouNet_UltimateVideos\Controller\Admin\Category\AddController',
	'ultimatevideo.admincp.customfield.add' => '\Apps\YouNet_UltimateVideos\Controller\Admin\Customfield\AddController',
	'ultimatevideo.admincp.customfield.index' => '\Apps\YouNet_UltimateVideos\Controller\Admin\Customfield\IndexController',
	'ultimatevideo.admincp.customfield.addfield' => '\Apps\YouNet_UltimateVideos\Controller\Admin\Customfield\AddFieldController',
	'ultimatevideo.admincp.ultilities' => '\Apps\YouNet_UltimateVideos\Controller\Admin\UltilitiesController',
	'ultimatevideo.admincp.managevideos' => '\Apps\YouNet_UltimateVideos\Controller\Admin\ManageVideosController',
    'ultimatevideo.admincp.manageplaylists' => '\Apps\YouNet_UltimateVideos\Controller\Admin\ManagePlaylistsController',
    'ultimatevideo.index'   => '\Apps\YouNet_UltimateVideos\Controller\IndexController',
    'ultimatevideo.add'   => '\Apps\YouNet_UltimateVideos\Controller\AddController',
    'ultimatevideo.playlist'   => '\Apps\YouNet_UltimateVideos\Controller\PlaylistController',
    'ultimatevideo.profile'   => '\Apps\YouNet_UltimateVideos\Controller\ProfileController',
    'ultimatevideo.view'   => '\Apps\YouNet_UltimateVideos\Controller\ViewController',
    'ultimatevideo.view_playlist'   => '\Apps\YouNet_UltimateVideos\Controller\ViewPlaylistController',
    'ultimatevideo.addplaylist' => '\Apps\YouNet_UltimateVideos\Controller\AddPlaylistController',
    'ultimatevideo.code' => '\Apps\YouNet_UltimateVideos\Controller\CodeController',
    'ultimatevideo.embed' => '\Apps\YouNet_UltimateVideos\Controller\EmbedController',
    'ultimatevideo.invite'=> '\Apps\YouNet_UltimateVideos\Controller\InviteController',
    'ultimatevideo.oauth2'=> '\Apps\YouNet_UltimateVideos\Controller\Oauth2Controller',
    'ultimatevideo.uploadchannel'=> '\Apps\YouNet_UltimateVideos\Controller\UploadChannelController',
])->addComponentNames('ajax', [
    'YouNet_UltimateVideos.ajax' => '\Apps\YouNet_UltimateVideos\Ajax\Ajax',
    'ultimatevideo.ajax'        => '\Apps\YouNet_UltimateVideos\Ajax\Ajax',
])->addTemplateDirs([
    'ultimatevideo' => PHPFOX_DIR_SITE_APPS . 'YouNet_UltimateVideos' . PHPFOX_DS . 'views',
])->addAliasNames('ultimatevideo', 'YouNet_UltimateVideos');

if(setting('ynuv_app_enabled')){
    group('/admincp/ultimatevideo/category',function(){
        route('/add', function (){
            auth()->isAdmin(true);
            Phpfox_Module::instance()->dispatch('ultimatevideo.admincp.category.add');
            return 'controller';
        });
        route('/edit/:id', function (){
            auth()->isAdmin(true);
            Phpfox_Module::instance()->dispatch('ultimatevideo.admincp.category.add');
            return 'controller';
        });
        route('/index', function (){
            auth()->isAdmin(true);
            Phpfox_Module::instance()->dispatch('ultimatevideo.admincp.category.index');
            return 'controller';
        });
    });
    group('/admincp/ultimatevideo/customfield',function(){
        route('/add', function (){
            auth()->isAdmin(true);
            Phpfox_Module::instance()->dispatch('ultimatevideo.admincp.customfield.add');
            return 'controller';
        });
        route('/edit/:id', function (){
            auth()->isAdmin(true);
            Phpfox_Module::instance()->dispatch('ultimatevideo.admincp.customfield.add');
            return 'controller';
        });
        route('/index', function (){
            auth()->isAdmin(true);
            Phpfox_Module::instance()->dispatch('ultimatevideo.admincp.customfield.index');
            return 'controller';
        });
    });
    route('/admincp/ultimatevideo/ultilities',function(){
        auth()->isAdmin(true);
        Phpfox_Module::instance()->dispatch('ultimatevideo.admincp.ultilities');
        return 'controller';
    });
    route('/admincp/ultimatevideo/managevideos',function(){
        auth()->isAdmin(true);
        Phpfox_Module::instance()->dispatch('ultimatevideo.admincp.managevideos');
        return 'controller';
    });
    route('/admincp/ultimatevideo/manageplaylists',function(){
        auth()->isAdmin(true);
        Phpfox_Module::instance()->dispatch('ultimatevideo.admincp.manageplaylists');
        return 'controller';
    });


    group('/ultimatevideo',function(){
        route('/admincp/category/order', function (){
            auth()->isAdmin(true);
            $ids = request()->get('ids');
            $ids = trim($ids, ',');
            $ids = explode(',', $ids);
            $values = [];
            foreach ($ids as $key => $id) {
                $values[ $id ] = $key + 1;
            }
            Core_Service_Process::instance()->updateOrdering([
                    'table'  => 'ynultimatevideo_category',
                    'key'    => 'category_id',
                    'values' => $values,
                ]
            );

            \Phpfox::getLib('cache')->remove('ynultimatevideos', 'substr');

            return true;
        });
        route('/upload', function () {
            $isPass = true;
            if($_FILES['ultvideoUpload']['name'] != '') {
                $maxFileSize = user('ynuv_file_size_limit_in_megabytes', 0);
                if (isset($_FILES['ultvideoUpload']['name']) && empty($_FILES['ultvideoUpload']['name']) || (isset($_FILES['ultvideoUpload']['size']) && (int)$_FILES['ultvideoUpload']['size'] <= 0)) {
                    $isPass = false;
                    \Phpfox::addMessage(_p('No files found or file is not valid. Please try again.'));
                } else {
                    $aVideo = \Phpfox::getLib('file')->load('ultvideoUpload', [], $maxFileSize);

                    if ($aVideo) {
                        //upload video file
                        $aVals['video_code'] = substr($_FILES['ultvideoUpload']['type'], strpos($_FILES['ultvideoUpload']['type'], '/') + 1);
                        $filePath = PHPFOX_DIR_FILE . 'ynultimatevideo' . PHPFOX_DS;
                        if (!is_dir($filePath)) {
                            if (!@mkdir($filePath, 0777, 1)) {

                            }
                        }
                        $videoFilePath = \PhpFox::getLib('file')->upload('ultvideoUpload', $filePath, $_FILES['ultvideoUpload']['name']);
                        $aVals['video_path'] = ($videoFilePath) ? $videoFilePath : "";
                    } else {
                        $isPass = false;
                    }
                }
            }

            $aVal = request()->get('val');
            $aVal['category'][0] = \Phpfox::getService('ultimatevideo.category')->getRandomCategory();
            $aVal['video_embed'] = $aVal['video_link'];
            $aVal['description'] = $aVal['status_info'];

            if($isPass){
                $iId = \Phpfox::getService('ultimatevideo.process')->add($aVal,null,true);
            }
            if(!empty($aVal['callback_module'])){
                return url()->send($aVal['callback_module'].'.'.$aVal['callback_item_id'].'.ultimatevideo');
            }
            else{
                return url()->send('ultimatevideo',['view' => 'my']);
            }

        });
        route('/','ultimatevideo.index');

        route('/code/:id', 'ultimatevideo.code')
            ->where(['id'=>'\d+']);

        route('/invite/:id', 'ultimatevideo.invite')
            ->where(['id'=>'\d+']);

        route('/embed/:id', 'ultimatevideo.embed')
            ->where(['id'=>'\d+']);

        route('/:id/*','ultimatevideo.view')
            ->where(['id'=>'\d+']);

        route('/category/:id/*','ultimatevideo.index')
            ->where(['id'=>'\d+']);

        route('/addplaylist/*','ultimatevideo.addplaylist');

        route('/add/*','ultimatevideo.add');

        route('/playlist/:id/*', 'ultimatevideo.view_playlist')
            ->where(['id'=>'\d+']);

        route('/profile/*', 'ultimatevideo.view');

        route('/playlist/*', 'ultimatevideo.playlist');

        route('/playlist/category/:id/*', 'ultimatevideo.playlist');

        route('/oauth2/*','ultimatevideo.oauth2');

        route('/uploadchannel/:id','ultimatevideo.uploadchannel')
            ->where(['id'=>'\d+']);

    });
}
