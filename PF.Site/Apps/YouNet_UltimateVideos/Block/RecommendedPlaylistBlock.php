<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/10/16
 * Time: 5:28 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;

class RecommendedPlaylistBlock extends Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('iLimit', setting('ynuv_recommended_playlists',10));
        $this->clearParam('iLimit');

        $aItems =  Phpfox::getService('ultimatevideo.playlist.browse')
            ->getMostRecentPlaylists($iLimit);
        if(empty($aItems)){
            return false;
        }
        $this->template()->assign([
            'sHeader'=> _p('Recommended Playlist'). ultimatevideo_playlist_view_mode(),
            'bShowTotalView'=> true,
            'bShowTotalLike'=> true,
            'bShowTotalComment'=> false,
            'aItems'=>$aItems
        ]);

        return 'block';

    }
}