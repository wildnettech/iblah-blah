<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/10/16
 * Time: 5:27 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;

class RelatedVideoBlock extends \Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('iLimit',setting('ynuv_related_videos',3));
        $this->clearParam('iLimit');

        $aItems = Phpfox::getService('ultimatevideo.browse')->getMostRelatedVideos($iLimit);
        if(empty($aItems)){
            return false;
        }
        $this->template()
            ->assign([
                'sHeader'=> _p('Related Video').ultimatevideo_video_view_mode(),
                'bShowTotalView'=> true,
                'bShowTotalLike'=> true,
                'bShowTotalComment'=> false,
                'aItems'=>$aItems,
            ]);

        return 'block';
    }
}