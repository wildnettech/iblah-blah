<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/9/16
 * Time: 6:05 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;

class MostCommentedVideoBlock extends Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('iLimit',setting('ynuv_most_commented_videos',3));
        $this->clearParam('iLimit');

        $aItems = Phpfox::getService('ultimatevideo.browse')->getMostCommentedVideos($iLimit);
        if(empty($aItems)){
            return false;
        }
        $this->template()
            ->assign([
                'sHeader'=> _p('Most Commented').ultimatevideo_video_view_mode(),
                'bShowTotalView'=> true,
                'bShowTotalLike'=> false,
                'bShowTotalComment'=> true,
                'aItems'=>$aItems,
            ]);
        return 'block';
    }
}