<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/9/16
 * Time: 6:04 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;

class MostLikedVideoBlock extends Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('iLimit',setting('ynuv_most_liked_videos',3));
        $this->clearParam('iLimit');

        $aItems = Phpfox::getService('ultimatevideo.browse')->getMostLikedVideos($iLimit);
        if(empty($aItems)){
            return false;
        }
        $this->template()
            ->assign([
                'sHeader'=> _p('Most Liked').ultimatevideo_video_view_mode(),
                'bShowTotalView'=> true,
                'bShowTotalLike'=> true,
                'bShowTotalComment'=> false,
                'aItems'=>$aItems,
            ]);

        return 'block';
    }
}