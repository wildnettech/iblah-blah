<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/10/16
 * Time: 5:31 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;

class RelatedPlaylistBlock extends Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('iLimit', setting('ynuv_related_playlists',3));
        $this->clearParam('iLimit');

        $aItems =  Phpfox::getService('ultimatevideo.playlist.browse')
            ->getMostRelatedPlaylists($iLimit);
        if(empty($aItems)){
            return false;
        }
        $this->template()->assign([
            'sHeader'=> _p('Related Playlist'),
            'bShowTotalView'=> true,
            'bShowTotalLike'=> true,
            'bShowTotalComment'=> false,
            'aItems'=>$aItems
        ]);

        return 'block';
    }
}