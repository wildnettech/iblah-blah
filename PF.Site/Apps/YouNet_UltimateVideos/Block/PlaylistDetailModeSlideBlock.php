<?php

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;

class PlaylistDetailModeSlideBlock extends Phpfox_Component
{
	public function process()
    {

    	$iPlaylistId = $this->getParam('playlist_id');
    	$corePath = Phpfox::getParam('core.path_actual').'PF.Site/Apps/YouNet_UltimateVideos';
    	$aLists = Phpfox::getService('ultimatevideo.playlist')->getVideosSlideShow($iPlaylistId);
        
    	$this->template()->assign([
    			'aItems' => $aLists,
    			'corePath' => $corePath,
    			'iPlaylistId' => $iPlaylistId,
                'bShowCommand' => false,
                'bIsSearch' => false,
                'bIsPagesView' => false,
    		]);
    }
}