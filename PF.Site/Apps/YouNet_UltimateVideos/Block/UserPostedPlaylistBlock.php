<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/12/16
 * Time: 11:24 AM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;

class UserPostedPlaylistBlock extends Phpfox_Component
{
    public function process()
    {
        if(!defined('ULTIMATE_PLAYLIST_OWNER_ID') || !ULTIMATE_PLAYLIST_OWNER_ID){
            return false;
        }

        $iLimit = $this->getParam('iLimit', setting('ynuv_more_from_owner_playlist', 3));
        $this->clearParam('iLimit');

        $aItems = Phpfox::getService('ultimatevideo.playlist.browse')->getUserPostedPlaylists($iLimit,ULTIMATE_PLAYLIST_OWNER_ID);
        if(empty($aItems)){
            return false;
        }        
        $this->template()->assign([
            'sHeader'=> _p('More From {{ name }}',['name'=> strtr('<a style="display:inline-block" href=":href">:name</a>',[
                ':name'=> ULTIMATE_PLAYLIST_USER_NAME,
                ':href'=> url('ultimatevideo.playlist',['user'=> ULTIMATE_PLAYLIST_OWNER_ID]),
            ])]),
            'bShowTotalView'=> true,
            'bShowTotalLike'=> true,
            'bShowTotalComment'=> false,
            'aItems'=>$aItems,
            'bIsPlaylistDetail' => true,
        ]);

        return 'block';
    }
}