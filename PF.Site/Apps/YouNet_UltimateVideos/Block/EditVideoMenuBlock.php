<?php

/**
 * [PHPFOX_HEADER]
 */
namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;
use Phpfox_Plugin;
defined('PHPFOX') or exit('NO DICE!');

class EditVideoMenuBlock extends \Phpfox_Component
{
	public function process()
    {
        if(!$id = $this->request()->get('id'))
        {
            return false;   
        }
        $isEditPhoto = false;
        $iId = $this->request()->get("id");   
        if(!$aVideo = Phpfox::getService('ultimatevideo')->getVideoForEdit($iId))
        {
            return false;
        }
        if($isEditPhoto = $this->request()->get("photo")){
            $isEditPhoto = true;
        }
        $aMenus = array(
            'detail' => _p('Video Details'),
            'photo' => _p('Photo')
        );
        $sLink = $this->url()->permalink('ultimatevideo',$aVideo['video_id'],$aVideo['title']);
        $sView = _p('View This Video');
        
        $this->template()->assign(array(
                'sView' => $sView,
                'aMenus' => $aMenus,
                'sLink' => $sLink,
                'isEditPhoto' => $isEditPhoto
            )
        );
        return 'block';    	
    }
}