<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/10/16
 * Time: 5:33 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;

class WatchItAgainBlock extends Phpfox_Component
{

    public function process()
    {
        $iLimit = $this->getParam('iLimit',setting('ynuv_watch_it_again',6));
        $this->clearParam('iLimit');

        if(!Phpfox::getUserId()){
            return false;
        }

        $aItems = Phpfox::getService('ultimatevideo.browse')->getWatchItAgainVideos($iLimit);
        if(empty($aItems)){
            return false;
        }
        Phpfox::getService('ultimatevideo.browse')->processRows($aItems);
        $this->template()
            ->assign([
                'sHeader'=> ('Watch It Again') .ultimatevideo_video_view_mode(),
                'bShowTotalView'=> true,
                'bShowTotalLike'=> true,
                'bShowTotalComment'=> false,
                'aItems'=>$aItems,
            ]);
        return 'block';
    }
}