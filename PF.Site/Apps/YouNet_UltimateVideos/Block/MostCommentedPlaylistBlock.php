<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/10/16
 * Time: 5:30 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;
use Phpfox;


class MostCommentedPlaylistBlock extends \Phpfox_Component
{

    public function process()
    {
        $iLimit = $this->getParam('iLimit', setting('ynuv_most_commented_paylists',3));
        $this->clearParam('iLimit');

        $aItems =  Phpfox::getService('ultimatevideo.playlist.browse')
            ->getMostCommentedPlaylists($iLimit);
        if(empty($aItems)){
            return false;
        }
        $this->template()->assign([
            'sHeader'=> _p('Most Commented'). ultimatevideo_playlist_view_mode(),
            'bShowTotalView'=> true,
            'bShowTotalLike'=> false,
            'bShowTotalComment'=> true,
            'aItems'=>$aItems
        ]);

        return 'block';
    }
}