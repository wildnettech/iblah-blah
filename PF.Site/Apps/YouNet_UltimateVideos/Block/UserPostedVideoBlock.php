<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/12/16
 * Time: 11:23 AM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;

class UserPostedVideoBlock extends Phpfox_Component
{
    public function process()
    {

        if(!defined('ULTIMATE_VIDEO_OWNER_ID') || !ULTIMATE_VIDEO_OWNER_ID){
            return false;
        }

        $iLimit = $this->getParam('iLimit', setting('ynuv_more_from_owner_video',3));
        $this->clearParam('iLimit');

        $aItems = Phpfox::getService('ultimatevideo.browse')->getUserPostedVideos($iLimit, ULTIMATE_VIDEO_OWNER_ID);
        if(empty($aItems)){
            return false;
        }
        $this->template()
            ->assign([
                'sHeader'=> _p('More From {{ name }}',['name'=> strtr('<a style="display:inline-block" href=":href">:name</a>',[
                    ':name'=> ULTIMATE_VIDEO_USER_NAME,
                    ':href'=> url('ultimatevideo',['user'=> ULTIMATE_VIDEO_OWNER_ID]),
                ])]).ultimatevideo_video_view_mode(),
                'bShowTotalView'=> true,
                'bShowTotalLike'=> true,
                'bShowTotalComment'=> false,
                'aItems'=>$aItems,
            ]);
        return 'block';
    }
}