<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/10/16
 * Time: 5:28 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;

class FeaturedPlaylistBlock extends \Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('iLimit', setting('ynuv_featured_playlists',5));
        $this->clearParam('iLimit');

        $aItems =  Phpfox::getService('ultimatevideo.playlist.browse')
            ->getMostFeaturedPlaylist($iLimit);

        if(empty($aItems)){
            return false;
        }

        $this->template()->assign([
            'sHeader'=> _p('Featured'). ultimatevideo_playlist_view_mode(),
            'aItems'=>$aItems
        ]);

        return 'block';
    }
}