<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/11/16
 * Time: 9:22 AM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;


class SlideshowPlaylistBlock extends Phpfox_Component
{

    public function process()
    {
        $iLimit = $this->getParam('iLimit',setting('ynuv_featured_playlists',5));
        $this->clearParam('iLimit');

        $aItems = Phpfox::getService('ultimatevideo.playlist.browse')->getSlideshowPlaylists($iLimit);
        foreach ($aItems as $key => $aItem) {
            $aCategory = Phpfox::getService('ultimatevideo.category')->getCategoryById($aItem['category_id']);
            if($aCategory)
            {
                $aItems[$key]['category_name'] = $aCategory['title'];
            }
            else{
                $aItems[$key]['category_name'] = _p('No Category');
            }
        }
        $this->template()->assign([
            'bShowTotalView'=> true,
            'bShowTotalLike'=> true,
            'bShowTotalComment'=> true,
            'aItems'=>$aItems,
            'corePath' => Phpfox::getParam('core.path_actual').'PF.Site/Apps/YouNet_UltimateVideos',
        ]);

        return 'block';
    }
}