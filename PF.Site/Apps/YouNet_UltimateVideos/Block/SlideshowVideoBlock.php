<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/9/16
 * Time: 6:06 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;

class SlideshowVideoBlock extends Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('iLimit',setting('ynuv_featured_videos',5));

        $this->clearParam('iLimit');

        $aItems = Phpfox::getService('ultimatevideo.browse')->getSlideshowVideos($iLimit);

        $this->template()
            ->assign([
                'bShowTotalView'=> true,
                'bShowTotalLike'=> true,
                'bShowTotalComment'=> false,
                'aItems'=>$aItems,
                'corePath' => Phpfox::getParam('core.path_actual').'PF.Site/Apps/YouNet_UltimateVideos',
            ]);
        return 'block';
    }
}