<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/10/16
 * Time: 5:27 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;

class RecommendedVideoBlock extends Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('iLimit',setting('ynuv_recommended_videos',3));
        $this->clearParam('iLimit');

        $aItems = Phpfox::getService('ultimatevideo.browse')->getMostRecommendedVideos($iLimit);
        if(empty($aItems)){
            return false;
        }
        Phpfox::getService('ultimatevideo.browse')->processRows($aItems);

        $this->template()
            ->assign([
                'sHeader'=> _p('Recommended Videos') .ultimatevideo_video_view_mode(),
                'bShowTotalView'=> true,
                'bShowTotalLike'=> true,
                'bShowTotalComment'=> false,
                'aItems'=>$aItems,
            ]);

        return 'block';
    }
}