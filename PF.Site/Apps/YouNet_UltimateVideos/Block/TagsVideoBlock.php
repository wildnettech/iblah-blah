<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/9/16
 * Time: 6:03 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;


use Phpfox;

class TagsVideoBlock extends \Phpfox_Component
{

    public function process()
    {
        $iLimit = $this->getParam('iLimit',setting('ynuv_tag_videos',10));
        $this->clearParam('iLimit');

        $aRows = Phpfox::getService('ultimatevideo.browse')->getTagCloud($iLimit);
        if(empty($aRows)){
            return false;
        }
        $this->template()->assign([
            'sHeader'=> _p('Tags'),
            'aRows'=>$aRows,
        ]);

        return 'block';
    }

}