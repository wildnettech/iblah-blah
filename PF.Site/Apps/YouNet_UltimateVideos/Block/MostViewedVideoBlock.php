<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/9/16
 * Time: 6:04 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;

class MostViewedVideoBlock extends Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('iLimit',setting('ynuv_most_viewed_videos',10));
        $this->clearParam('iLimit');

        $aItems = Phpfox::getService('ultimatevideo.browse')->getMostViewedVideos($iLimit);
        if(!$aItems)
            return false;
        $this->template()
            ->assign([
                'sHeader'=> _p('Most Viewed').ultimatevideo_video_view_mode(),
                'bShowTotalView'=> true,
                'bShowTotalLike'=> true,
                'bShowTotalComment'=> false,
                'aItems'=>$aItems,
            ]);
        return 'block';
    }
}