<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/11/16
 * Time: 9:25 AM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;

class MostLikedPlaylistBlock extends \Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('iLimit', setting('ynuv_most_liked_playlists',3));
        $this->clearParam('iLimit');

        $aItems =  Phpfox::getService('ultimatevideo.playlist.browse')
            ->getMostLikedPlaylists($iLimit);
        if(empty($aItems)){
            return false;
        }
        $this->template()->assign([
            'sHeader'=> _p('Most Liked'). ultimatevideo_playlist_view_mode(),
            'bShowTotalView'=> true,
            'bShowTotalLike'=> true,
            'bShowTotalComment'=> false,
            'aItems'=>$aItems
        ]);

        return 'block';
    }
}