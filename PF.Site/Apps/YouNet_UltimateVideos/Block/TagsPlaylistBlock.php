<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/11/16
 * Time: 9:31 AM
 */

namespace Apps\YouNet_UltimateVideos\Block;


use Phpfox;

class TagsPlaylistBlock extends \Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('iLimit', setting('ynuv_tag_playlists',10));
        $this->clearParam('iLimit');

        $aRows = Phpfox::getService('ultimatevideo.playlist.browse')->getTagCloud($iLimit);

        $this->template()
            ->assign([
                'aRows'=>$aRows,
                'sHeader'=> _p("Tags"),
            ]);

        return 'block';
    }
}