<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/10/16
 * Time: 5:29 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;

class MostViewedPlaylistBlock extends \Phpfox_Component
{
    public function process()
    {
        $iLimit = $this->getParam('iLimit', setting('ynuv_most_viewed_playlists',3));
        $this->clearParam('iLimit');

        $aItems =  Phpfox::getService('ultimatevideo.playlist.browse')
            ->getMostViewedPlaylists($iLimit);
        if(!$aItems)
            return false;
        $this->template()->assign([
            'sHeader'=> _p('Most Viewed'). ultimatevideo_playlist_view_mode(),
            'bShowTotalView'=> true,
            'bShowTotalLike'=> true,
            'bShowTotalComment'=> false,
            'aItems'=>$aItems
        ]);

        return 'block';
    }
}