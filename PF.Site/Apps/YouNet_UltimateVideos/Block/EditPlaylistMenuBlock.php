<?php

/**
 * [PHPFOX_HEADER]
 */
namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox;
use Phpfox_Component;
use Phpfox_Plugin;
defined('PHPFOX') or exit('NO DICE!');

class EditPlaylistMenuBlock extends \Phpfox_Component
{
	public function process()
    {
        if(!$id = $this->request()->get('id'))
        {
            return false;   
        }
        $isEditPhoto = false;
        $isManageVideo = false;
        $iId = $this->request()->get("id");
        if(!$aPlaylist = Phpfox::getService('ultimatevideo.playlist')->getForEdit($iId))
        {
            return false;
        }
        if($this->request()->get("photo")){
            $isEditPhoto = true;
        }
        if($this->request()->get('video')){
            $isManageVideo = true;
        }
        $aMenus = array(
            'detail' => _p('Playlist Details'),
            'photo' => _p('Photo'),
            'video' => _p('Manage Videos'),
        );
        $sLink = $this->url()->permalink('ultimatevideo.playlist',$aPlaylist['playlist_id'],$aPlaylist['title']);
        $sView = _p('View This Playlist');
        
        $this->template()->assign(array(
                'sView' => $sView,
                'aMenus' => $aMenus,
                'sLink' => $sLink,
                'isEditPhoto' => $isEditPhoto,
                'isManageVideo' => $isManageVideo
            )
        );
        return 'block';    	
    }
}