<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/9/16
 * Time: 6:11 PM
 */

namespace Apps\YouNet_UltimateVideos\Block;


use Phpfox;

class FeaturedVideoBlock extends \Phpfox_Component
{

    public function process()
    {
        $iLimit = $this->getParam('iLimit', setting('ynuv_featured_videos',3));
        $this->clearParam('iLimit');

        $aItems =  Phpfox::getService('ultimatevideo.browse')
            ->getMostFeaturedVideos($iLimit);

        if(empty($aItems)){
            return false;
        }

        $this->template()->assign([
            'sHeader'=> _p('Featured').ultimatevideo_video_view_mode(),
            'aItems'=>$aItems
        ]);

        return 'block';
    }
}