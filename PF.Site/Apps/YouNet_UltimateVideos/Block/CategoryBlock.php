<?php
namespace Apps\YouNet_UltimateVideos\Block;

use Phpfox_Component;
use Phpfox;
use Phpfox_Request;

class CategoryBlock extends Phpfox_Component{

    public function process()
    {
        if (defined('PHPFOX_IS_USER_PROFILE')) {
            return false;
        }

        $iCategoryId = $this->getParam('sCategory', 0);

        switch (\Phpfox_Module::instance()->getFullControllerName()){
            case 'ultimatevideo.playlist':
                $aCategories = Phpfox::getService('ultimatevideo.category')->getForBrowsePlaylist($iCategoryId);
                break;
            default:
                $aCategories = Phpfox::getService('ultimatevideo.category')->getForBrowse($iCategoryId);
        }

        if (!is_array($aCategories)) {
            return false;
        }

        if (!count($aCategories)) {
            return false;
        }

        if ($sView = \Phpfox_Request::instance()->get('view'))
        {
            $sView = Phpfox::getLib('parse.input')->clean($sView);
            if (in_array($sView, ['my', 'pending', 'all','later','history','friend','myplaylist','historyplaylist','pendingplaylist']))
            {
                foreach ($aCategories as $iKey => $aCategory) {
                    $aCategories[ $iKey ]['url'] = $aCategory['url'] . '?view=' . $sView;
                    if(count($aCategories[ $iKey ]['sub']))
                    {
                        foreach ($aCategories[$iKey]['sub'] as $skey => $aSub) {
                            $aCategories[$iKey]['sub'][$skey]['url'] = $aCategories[$iKey]['sub'][$skey]['url']. '?view='.$sView;
                        }
                    }
                }
            }
        }

        $this->template()->assign([
                'sHeader'     => ($iCategoryId ?_p('Sub categories') : _p('Categories')),
                'aCategories' => $aCategories,
            ]
        );

        return 'block';
    }
}