<?php

/**
 * YouNet Company
 *
 * @category   Application_Extensions
 * @package    Ynultimatevideo
 * @author     YouNet Company
 */
namespace Apps\YouNet_UltimateVideos\Adapter;

use Phpfox;
use Phpfox_Url;
use Apps\YouNet_UltimateVideos\Adapter\Abstracts;
use Phpfox_Service;
class Uploaded extends Abstracts
{
	protected $_params;
	public function compileVideo($params)
	{
		$video_id = $params['video_id'];
		$location = $params['location'];
		$location1 = $params['location1'];
		$view = $params['view'];
		$duration = $params['duration'];
		$mobile = $params['mobile'];
		$video = Phpfox::getService('ultimatevideo')->getVideoForEdit($params['video_id']);
		$class = "";
		if($video['image_path']){
			$imagePath = Phpfox::getParam('core.path_actual').'PF.Base/file/pic/'.sprintf($video['image_path'],'_500');
		}
		else{
			$imagePath = Phpfox::getParam('core.path_actual').'PF.Site/Apps/YouNet_UltimateVideos/assets/image/noimg_video.jpg';
		}
		if($location1)
		{
			if($mobile)
			{
				$class="video-js vjs-default-skin";
			}
			$embedded = '
			 <video id="player_' . $video_id . '" class="ultimatevideo-player '.$class.'" controls
				 preload="auto" poster="' . $imagePath . '"
				 data-setup="{}">
				  <source src="' . $location1 . '" type="video/mp4">
				</video>';
		}
		else
		{
			 $embedded = "
		  <div id='videoFrame".$video_id."'></div>
		  <script type='text/javascript'>
		  $Behavior.onLoadUploadedVideo = function() {\$('video_thumb_".$video_id.$params['count_video']."').removeEvents('click').addEvent('click', function(){flashembed('videoFrame$video_id',{src: '".Zend_Registry::get('StaticBaseUrl')."externals/flowplayer/flowplayer-3.1.5.swf', width: ".($view?"480":"420").", height: ".($view?"386":"326").", wmode: 'opaque'},{config: {clip: {url: '$location',autoPlay: ".($view?"false":"true").", duration: '$duration', autoBuffering: true},plugins: {controls: {background: '#000000',bufferColor: '#333333',progressColor: '#444444',buttonColor: '#444444',buttonOverColor: '#666666'}},canvas: {backgroundColor:'#000000'}}});})});
		  </script>";
		}
		return $embedded;
	}

	public function extractVideo($params)
	{
		$video_id = $params['video_id'];
		$location = $params['location'];
		$location1 = $params['location1'];
		$view = $params['view'];
		$duration = $params['duration'];
		$mobile = $params['mobile'];
		$video = Phpfox::getService('ultimatevideo')->getVideoForEdit($params['video_id']);
		$class = "";
		if($video['image_path']){
			$imagePath = Phpfox::getParam('core.path_actual').'PF.Base/file/pic/'.sprintf($video['image_path'],'_500');
		}
		else{
			$imagePath = Phpfox::getParam('core.path_actual').'PF.Site/Apps/YouNet_UltimateVideos/assets/image/noimg_video.jpg';
		}
		if($location1)
		{
			if($mobile)
			{
				$class="video-js vjs-default-skin";
			}
			$embedded = '
			 <video id="player_' . $video_id . '" class="ynultimatevideo-player '.$class.'" controls
				 preload="auto" poster="' . $imagePath . '"
				 data-setup="{}" style="display:none;" data-type="3" width="766" height="426">
				 <source src="' . $location1 . '" type="video/mp4">
				</video>';
		}
		else
		{
			$embedded = '
			 <video id="player_' . $video_id . '" class="ynultimatevideo-player '.$class.'" controls
				 preload="auto" poster="' . $imagePath . '"
				 data-setup="{}" style="display:none;" data-type="3" width="766" height="426">
				 <source src="' . $location . '" type="video/mp4">
				</video>';
		}
		return $embedded;
	}

	public function setParams($options)
	{
		foreach ($options as $key => $value)
		{
			$this -> _params[$key] = $value;
		}
	}

	public function getVideoLargeImage()
	{
		if (isset($this -> _params) && array_key_exists('video_id', $this -> _params))
		{
			$video_id = $this -> _params['video_id'];
			$video = Engine_Api::_() -> getItem('ynultimatevideo_video', $video_id);

			if ($video instanceof Ynultimatevideo_Model_Video)
			{
				$storageObject = Engine_Api::_() -> getItem('storage_file', $video -> file_id);
				if (!$storageObject)
				{
					_p('Video storage file was missing');
				}

				$thumb_splice = $video -> duration / 2;
				$tmpDir = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary' . DIRECTORY_SEPARATOR . 'ynultimatevideo';
				$thumbPathLarge = $tmpDir . DIRECTORY_SEPARATOR . $video -> getIdentity() . '_vthumb_large.jpg';
				$ffmpeg_path = setting('ynuv_ffmpeg_path');

				if (!@file_exists($ffmpeg_path) || !@is_executable($ffmpeg_path))
				{
					$output = null;
					$return = null;
					exec($ffmpeg_path . ' -version', $output, $return);
					if ($return > 0)
					{
						return 0;
					}
				}

				// Prepare output header
				$output = PHP_EOL;
				$output .= $storageObject -> temporary() . PHP_EOL;
				$output .= $thumbPathLarge . PHP_EOL;

				$thumbCommand = $ffmpeg_path . ' ' . '-i ' . escapeshellarg($storageObject -> storage_path) . ' ' . '-f image2' . ' ' . '-ss ' . $thumb_splice . ' ' . '-vframes ' . '1' . ' ' . '-v 2' . ' ' . '-y ' . escapeshellarg($thumbPathLarge) . ' ' . '2>&1';
				// Process thumbnail
				$thumbOutput = $output . $thumbCommand . PHP_EOL . shell_exec($thumbCommand);
				// Check output message for success
				$thumbSuccess = true;
				if (preg_match('/video:0kB/i', $thumbOutput))
				{
					$thumbSuccess = false;
				}

				// Resize thumbnail
				if ($thumbSuccess && is_file($thumbPathLarge))
				{
					try
					{
				        $sNewsPicStorage = Phpfox::getParam('core.dir_pic') . 'ynultimatevideo';
				        if (!is_dir($sNewsPicStorage)) {
				            @mkdir($sNewsPicStorage, 0777, 1);
				            @chmod($sNewsPicStorage, 0777);
				        }
				        $ThumbNail = Phpfox::getLib('file')->getBuiltDir($sNewsPicStorage . PHPFOX_DS) . md5('image_' . $iToken . '_' . PHPFOX_TIME) . '%s.jpg';
		        		Phpfox::getLib('image')->createThumbnail($thumbPathLarge,sprintf($ThumbNail,''), 250, 250);
		        		Phpfox::getLib('image')->createThumbnail($thumbPathLarge,sprintf($ThumbNail, '_' . 500), 500, 500);
		        		$sFileName = str_replace(Phpfox::getParam('core.dir_pic'), "", $ThumbNail);
		       			$sFileName = str_replace("\\", "/", $sFileName);      
						unlink($thumbPathLarge);
						
						return $sFileName;
					}
					catch (Exception $e)
					{
						throw $e;
						unlink($thumbPathLarge);
						unlink($thumbPath);
						return "";
					}
				}
			}
		}
	}
	public function getVideoDescription(){}
	public function getVideoTitle(){}
	public function getVideoDuration(){}
	public function isValid(){}
	public function fetchLink(){}
}
