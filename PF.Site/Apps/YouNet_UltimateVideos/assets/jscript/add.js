var ultimatevideo = {
	changeCustomFieldByCategory: function(iCategoryId){
		iVideoId = $('#ynuv_videoid').val();

        $Core.ajax('ultimatevideo.changeCustomFieldByCategory',
        {
            type: 'POST',
            params:
            {
                action: 'changeCustomFieldByCategory'
                ,iCategoryId: iCategoryId
                ,iVideoId : iVideoId
            },
            success: function(sOutput)
            {
                var oOutput = $.parseJSON(sOutput);
                if(oOutput.status == 'SUCCESS')
                {
                	$('#ynuv_customfield_category').html(oOutput.content);
                	// add validate each custom field
					$('#ynuv_customfield_category').find('[data-isrequired="1"]').each(function(){
						var type = $(this).data('type');
						switch(type){
							case 'text':
								$(this).rules('add', {
									checkCustomFieldText: true
								});
								break;
							case 'textarea':
								$(this).rules('add', {
									checkCustomFieldTextarea: true
								});
								break;
							case 'select':
								$(this).rules('add', {
									checkCustomFieldSelect: true
								});
								break;
							case 'multiselect':
								$(this).rules('add', {
									checkCustomFieldMultiselect: true
								});
								break;
							case 'radio':
								$(this).rules('add', {
									checkCustomFieldRadio: true
								});
								break;
							case 'checkbox':
								$(this).rules('add', {
									checkCustomFieldCheckbox: true
								});
								break;
						}
					});

                } else
                {
                }
            }
        });
	},
	showValidProcess : function (type){
		if(type == "Embed")
			$("#ynuv_add_processing_embed").css('display','block');
		else
			$("#ynuv_add_processing").css('display','block');
		$("#ynuv_add_submit").attr('disabled','disabled');
		$('#ynuv_add_error_link').css('display','none');
		$('#ynuv_add_error_embed').css('display','none');
	},
	emptyOldValueWhenChangeSource : function (){
		$('#ynuv_add_video_input_mp4').val('');
		$('#ynuv_add_video_input_link').val('');
		$('#ynuv_add_video_input_embed').val('');
		$("#ynuv_add_submit").attr('disabled','disabled');
	},
	hideAllMessage : function(){
		$("#ynuv_add_processing_embed").css('display','none');
		$("#ynuv_add_processing").css('display','none');
		$("#ynuv_add_submit").attr('disabled','disabled');
		$('#ynuv_add_error_link').css('display','none');
		$('#ynuv_add_error_embed').css('display','none');
	},
	fileSelected: function()
    {
        var file = document.getElementById('videoUpload').files[0];
        if (file) {
            var fileSize = 0;
            if (file.size > 1024 * 1024)
                fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
            else
                fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';

            $('#ynuv_fileSize').html('Size: ' + fileSize);
            $('#ynuv_fileName').html('Name: ' + file.name);
            $("#ynuv_add_submit").removeAttr('disabled');
            //$('#ynuv_progress').css('display','inline-block');
            //$('#ynuv_progressNumber').css('display','inline-block');
        }
    },
    initValidator: function(element){
		jQuery.validator.messages.required  = "This field is required";
		$.data(element[0], 'validator', null);
		element.validate({
			errorPlacement: function (error, element) {
				if(element.is(":radio") || element.is(":checkbox")) {
					// error.appendTo(element.parent());
					error.appendTo($(element).closest('.table_right'));
				} else {
					error.appendTo(element.parent());
				}
			},
			errorClass: 'ultimatevideo-error',
			errorElement: 'span',
			debug: false
		});
	},
	init: function(){
		ultimatevideo.initValidator($('#ynuv_add_video_form'));
		jQuery.validator.addMethod('checkCategory', function() {
			var result = false;
			if($('#ynuv_add_video_form #ynuv_section_category.table_right:first #js_mp_id_0').val() != ''){
				result = true;
			}
			return result;
		}, 'This field is required');
		jQuery.validator.addMethod('checkCustomFieldText', function(value, element, params) {
			var result = false;
			if(element.value.length > 0){
				result = true;
			}
			return result;
		}, 'This field is required');
		jQuery.validator.addMethod('checkCustomFieldTextarea', function(value, element, params) {
			var result = false;
			if($(element).val().length > 0){
				result = true;
			}
			return result;
		}, 'This field is required');
		jQuery.validator.addMethod('checkCustomFieldSelect', function(value, element, params) {
			var result = false;
			if($(element).val().length > 0){
				result = true;
			}
			return result;
		}, 'This field is required');
		jQuery.validator.addMethod('checkCustomFieldMultiselect', function(value, element, params) {
			var result = false;
			var select = $(element).val();
			if(undefined != select && null != select && select.length > 0){
				result = true;
			}
			return result;
		}, 'This field is required');
		jQuery.validator.addMethod('checkCustomFieldCheckbox', function(value, element, params) {
			var result = false;
			var name = element.name;
			if($('input[name="' + name + '"]:checkbox').is(':checked')){
				result = true;
			}
			return result;
		}, 'This field is required');
		jQuery.validator.addMethod('checkCustomFieldRadio', function(value, element, params) {
			var result = false;
			var name = element.name;
			if($('input[name="' + name + '"]:radio').is(':checked')){
				result = true;
			}
			return result;
		}, 'This field is required');	
		$('#ynuv_add_video_form #ynuv_add_video_title').rules('add', {
			required: true
		});
		$('#ynuv_add_video_form #ynuv_section_category.table_right:first #js_mp_id_0').rules('add', {
			checkCategory: true
		});
	},
	ultimatevideoAddVideo : function()
	{
		if(!$("#js_ultimatevideo_block_detail").length) return;
		ultimatevideo.init();
		if($('#ynuv_add_video_input_embed').val() == "")
		{
			$('#ynuv_add_video_input_embed').val("1");
		}
		$('.js_mp_category_list').change(function()
		{
			var $this = $(this);
			var iParentId = parseInt(this.id.replace('js_mp_id_', ''));
			iCatId = $this.val();
			if(!iCatId) {
				iCatId = parseInt($this.parent().attr("id").replace('js_mp_holder_', ""));
			}


			$('.js_mp_category_list').each(function()
			{
				if (parseInt(this.id.replace('js_mp_id_', '')) > iParentId)
				{
					$('#js_mp_holder_' + this.id.replace('js_mp_id_', '')).hide();				
					
					this.value = '';
				}
			});
			var $parent = $(".table_right > .js_mp_parent_holder").find('.js_mp_category_list');
			$('#js_mp_holder_' + $(this).val()).show();
			ultimatevideo.changeCustomFieldByCategory($parent.val());
		});	
		$('#ynav_js_source_video').change(function(){
			console.log('ynav_js_source_video changed');
			ultimatevideo.emptyOldValueWhenChangeSource();
			ultimatevideo.hideAllMessage();
			iSoureVideo = $(this).val();
			console.log('iSoureVideo', iSoureVideo);
			switch(iSoureVideo){
				case 'Youtube':
				case 'Vimeo':
				case 'Dailymotion':
				case 'Facebook':
					$('#ynuv_add_video_input_link').val('');
					if($('#ynuv_add_video_input_embed').val() == "")
					{
						$('#ynuv_add_video_input_embed').val("1");
					}
					$('#ynav_add_video_link').css('display','block');
					$('#ynav_add_video_embed').css('display','none');
					$('#ynav_add_video_upload').css('display','none');
					$('#ynav_add_video_mp4_url').css('display','none');
					$('#ynuv_help_block_link').css('display','block');
					$('#ynuv_help_block_url').css('display','none');
					break;
				case 'VideoURL':
					$('#ynuv_add_video_input_link').val('');
					if($('#ynuv_add_video_input_embed').val() == "")
					{
						$('#ynuv_add_video_input_embed').val("1");
					}
					$('#ynav_add_video_link').css('display','block');
					$('#ynav_add_video_embed').css('display','none');
					$('#ynav_add_video_upload').css('display','none');
					$('#ynav_add_video_mp4_url').css('display','none');
					$('#ynuv_help_block_link').css('display','none');
					$('#ynuv_help_block_url').css('display','block');
					break;
					break;
				case 'Embed':
					if($('#ynuv_add_video_input_link').val() == "")
					{
						$('#ynuv_add_video_input_link').val("1");
					}			
					$('#ynuv_add_video_input_embed').val('');
					$('#ynav_add_video_embed').css('display','block');
					$('#ynav_add_video_upload').css('display','none');
					$('#ynav_add_video_link').css('display','none');
					$('#ynav_add_video_mp4_url').css('display','none');
					break;
				case 'Uploaded':
					console.log('switch uploaded');
					if($('#ynuv_add_video_input_link').val() == "")
					{
						$('#ynuv_add_video_input_link').val("1");
					}
					if($('#ynuv_add_video_input_embed').val() == "")
					{
						$('#ynuv_add_video_input_embed').val("1");
					}
					if($('#ynuv_add_video_code').val() == "")
					{
						$('#ynuv_add_video_code').val("1");
					}		
					$('#ynav_add_video_upload').css('display','block');
					$('#ynav_add_video_embed').css('display','none');
					$('#ynav_add_video_link').css('display','none');
					$('#ynav_add_video_mp4_url').css('display','none');
					break;
				default:
					$('#ynav_add_video_link').css('display','none');
					$('#ynav_add_video_embed').css('display','none');
					$('#ynav_add_video_upload').css('display','none');
					$('#ynav_add_video_mp4_url').css('display','none');
					break;
			}
		});
		//Get video data for Youtube, Vimeo, Dailymotion, Facebook
		$('#ynuv_add_video_input_link').change(function(){
			var type = $('#ynav_js_source_video').val();
			ultimatevideo.showValidProcess(type);
			var url = $(this).val();
			url = url.replace(/\\|\'|\(\)|\"|$|\#|%|<>/gi, "");
			var code = eval('ynultimatevideo_extract_code.' + type + "('" + url + "')");
			$("#ynuv_add_video_code").val(code);
			$Core.ajax('ultimatevideo.validationUrl',
			 {
			 	type:"POST",
			 	params:
			 	{
			 		url : code,
			 		type : type,
			 	},
			 	success: function(sOutput){
			 		$("#ynuv_add_processing").css('display','none');
			 		var oOutput = $.parseJSON(sOutput);
			 		if(oOutput.status == "SUCCESS")
			 		{
			 			$("#ynuv_add_submit").removeAttr('disabled');
			 			if(oOutput.title != "")
			 				$('#ynuv_add_video_title').val(oOutput.title);
			 			if(oOutput.description != "")
			 				$('#description').val(oOutput.description);
			 		}
			 		else{
			 			$('#ynuv_add_error_link').html(oOutput.error_message);
			 			$('#ynuv_add_error_link').css('display','block');
			 		}
			 	}
			 });
		});
		$("#ynuv_add_video_input_embed").change(function(){
			var type = $('#ynav_js_source_video').val();
			ultimatevideo.showValidProcess(type);
			var embed = $(this).val();
			var code = embed.match(/(<iframe.*? src=(\"|\'))(.*?)((\"|\').*)/);
			if(code && code.length > 2)
			{
				$("#ynuv_add_submit").removeAttr('disabled');
				$("#ynuv_add_processing_embed").css('display','none');
				$("#ynuv_add_video_code").val(code[3]);
			}
			else
			{
				$("#ynuv_add_processing_embed").css('display','none');
				$('#ynuv_add_error_embed').css('display','block');
				$("#ynuv_add_video_code").val("");
			}
		});

	}
}
var ynultimatevideo_extract_code = {
	Youtube : function (url) {
		var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
	    if(videoid){
	    	return videoid[1];
	    }
	    return "";
	},

	Vimeo : function (url) {
	    var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?vimeo.com\/(\d+)($|\/)/);
	    if (videoid){
    		return videoid[1];
		}
	    return "";
	},

	Dailymotion : function (url) {
	    var videoid = url.match(/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/);
	   	if (videoid){
    		return videoid[2];
		}
	    return "";
	},

	Facebook : function (url){
		var videoid = url.match(/http(?:s?):\/\/(?:www\.|web\.|m\.)?facebook\.com\/([A-z0-9\.]+)\/videos(?:\/[0-9A-z].+)?\/(\d+)(?:.+)?$/);
		if (videoid){
    		return videoid[2];
		}
	    return "";
	},

	VideoURL : function (url) {
		var ext = url.substr(url.lastIndexOf('.') + 1);
		if (ext.toUpperCase() == 'MP4'){
			return url;
		}
	    return "";
	}
}

