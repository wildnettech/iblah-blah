
var ultimatevideo_playlist = {
	initValidator: function(element){
		jQuery.validator.messages.required  = "This field is required";
		$.data(element[0], 'validator', null);
		element.validate({
			errorPlacement: function (error, element) {
				if(element.is(":radio") || element.is(":checkbox")) {
					error.appendTo($(element).closest('.table_right'));
				} else {
					error.appendTo(element.parent());
				}
			},
			errorClass: 'ultimatevideo-error',
			errorElement: 'span',
			debug: false
		});
	},
	init: function(){
		ultimatevideo_playlist.initValidator($('#ynuv_add_playlist_form'));
		jQuery.validator.addMethod('checkCategory', function() {
			var result = false;
			if($('#ynuv_add_playlist_form #ynuv_section_category.table_right:first #js_mp_id_0').val() != ''){
				result = true;
			}
			return result;
		}, 'This field is required');
		$('#ynuv_add_playlist_form #ynuv_add_playlist_title').rules('add', {
			required: true
		});
		$('#ynuv_add_playlist_form #ynuv_section_category.table_right:first #js_mp_id_0').rules('add', {
			checkCategory: true
		});
	},
	addSort: function()
	{
		$('.sortable ul').sortable({
				axis: 'y',
				update: function (element, ui) {
					var iCnt = 0;
					$('.js_mp_order').each(function () {
						iCnt++;
						this.value = iCnt;
					});
				},
				opacity: 0.4
			}
		);	
	
	},
	ultimatevideoAddPlaylist : function()
	{	
		if(!$("#js_ultimatevideo_playlist_block_detail").length) return;
		ultimatevideo_playlist.init();
		ultimatevideo_playlist.addSort();
		$('.js_mp_category_list').change(function()
		{
			var $this = $(this);
			var iParentId = parseInt(this.id.replace('js_mp_id_', ''));
			iCatId = $this.val();
			if(!iCatId) {
				iCatId = parseInt($this.parent().attr("id").replace('js_mp_holder_', ""));
			}


			$('.js_mp_category_list').each(function()
			{
				if (parseInt(this.id.replace('js_mp_id_', '')) > iParentId)
				{
					$('#js_mp_holder_' + this.id.replace('js_mp_id_', '')).hide();				
					
					this.value = '';
				}
			});
			var $parent = $(".table_right > .js_mp_parent_holder").find('.js_mp_category_list');
			$('#js_mp_holder_' + $(this).val()).show();
		});

	}

}
$Core.loadInit();