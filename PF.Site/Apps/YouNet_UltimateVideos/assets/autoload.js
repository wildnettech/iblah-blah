var UltimateVideo = {};

(function(videos, $){
    var supportCommands  = ['approve_video','featured_video','unfeatured_video',
        'favorite_video','unfavorite_video','watchlater_video',
        'unwatchlater_video','delete_video_history','rate_video',
        'featured_playlist','unfeatured_playlist','approve_playlist','delete_playlist_history'
    ];

    function makeVideoFunction(cmd){
        return function(ele){
            $.ajaxCall('ultimatevideo.'+ cmd, $.param({iVideoId: ele.data('id'), iValue: ele.data('value')||0}));
            return false;
        };
    }

    videos.makeId =  function() {
        var text = "",
            possible = "abcdefghijklmnopqrstuvwxyz01234567890",
            i = 0;

        for(; i < 10; i++ ) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return '_'+text;
    };

    for(var i in supportCommands){
        videos[supportCommands[i]] = makeVideoFunction(supportCommands[i]);
    }

    videos.delete_video = function (ele) {
        if(!confirm($(ele).data('confirm'))) return;
        $.ajaxCall('ultimatevideo.delete_video', $.param({iVideoId: ele.data('id'),isDetail: ele.data('detail')}));
    };

    videos.delete_playlist = function (ele) {
        if(!confirm($(ele).data('confirm'))) return;
        $.ajaxCall('ultimatevideo.delete_playlist', $.param({iPlaylistId: ele.data('id'),isDetail: ele.data('detail')}));
    };

    videos.show_embed_code =function(ele){
        var block  = $('.ultimate_video_html_code_block',ele.closest('.ultimatevideo_video_detail'));
        block.toggleClass('hide');
        if(!block.hasClass('hide')){
            $('textarea', block).get(0).select();
        }
    };

    videos.copy_embed_code =function(ele){
        var block  = $('.ultimate_video_html_code_block',ele.closest('.ultimatevideo_video_detail'));
        $('textarea', block).get(0).select();
    };

    videos.video_clear_all = function(ele){

        $.ajaxCall('ultimatevideo.video_clear_all',$.param({sView: $(ele).data('view')}));
    };

    videos.playlist_clear_all = function(ele){
        $.ajaxCall('ultimatevideo.playlist_clear_all');
    };

    $(document).on('click','[data-toggle="ultimatevideo"]',function(evt){
        var cmd =  $(this).data('cmd');
        if(!videos.hasOwnProperty(cmd)) return;
        evt.preventDefault();
        return videos[cmd]($(this), evt);
    });

    $(document).on('mouseenter','.rate_video', function(){
        var ele  = $(this);
        ele.prevAll().addClass('hover');
        ele.addClass('hover');

    }).on('mouseout', '.rate_video', function(){
        var ele  = $(this);
        ele.prevAll().removeClass('hover');
        ele.removeClass('hover');
    });

    // ultimatevideo-grid
    var modeViews = ['show_list_view','show_grid_view','show_casual_view'];
    function makeModeView(mode,list){
        return function(ele){

            var grid =$('.ultimatevideo-grid',$(ele).closest('.block'));
            var icon_modeview =$('.ultimatevideo-modeviews',$(ele).closest('.block'));
            grid.removeClass(list.join(' '));
            icon_modeview.removeClass(list.join(' '));
            grid.addClass(mode);
            icon_modeview.addClass(mode);
        }
    }
    for(var i in modeViews){
        videos[modeViews[i]] =  makeModeView(modeViews[i], modeViews);
    }
    videos.currentModeViewInPlaylistDetail = function(){
        var ele = $('#ultimatevideo-modeviews-playlist-detail'),
            grid_mode = ele.find('.ultimatevideo-grid.show_grid_view'),
            casual_mode = ele.find('.ultimatevideo-grid.show_casual_view');
        if(grid_mode.length)
            return 1;
        if(casual_mode.length)
            return 2;
    }
    videos.showFormAddPlaylist = function(ele){
        if(!ele.prop('id'))
            ele.prop('id', videos.makeId());
        $(ele).find('.fa-angle-down').toggle();
        $(ele).find('.fa-angle-up').toggle();
        $(".ultimatevideo-quick-add-form", $(ele).closest('ul')).toggle();
        return false;
    };

    videos.add_to_playlist = function(ele){
        var input =$('input',ele).get(0),
            checked  = input.checked,
            data ={
                isChecked: checked?0:1,
                iVideoId: ele.data('id'),
                iPlaylistId: ele.data('playlist'),
                iContainerId : ele.closest('ul').find('.ynuv_quick_list_playlist').prop('id'),
            };

        input.checked =  !checked;
        $.ajaxCall('ultimatevideo.updateQuickAddVideoToPlaylist',$.param(data));
        return false;
    };

    videos.slideshows =  function(ele){
        if (!ele.length) return;

        ele.owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 300,
            paginationSpeed: 400,
            autoPlay: true,
            singleItem: true,
            navigationText: ["<i class='ynicon yn-arr-left'></i>", "<i class='ynicon yn-arr-right'></i>"],
        });

        
    };

    videos.close_add_form = function(ele){
        $(ele).closest('.ultimatevideo-quick-add-form').css('display','none');
        $(ele).closest('ul').find('.fa-angle-down').toggle();
        $(ele).closest('ul').find('.fa-angle-up').toggle();
        return false;
    };
    videos.add_new_playlist = function(ele){
        var ele = $(ele),
            parent = ele.closest('.ultimatevideo-quick-add-form'),
            sInput = parent.find('input').val(),
            iContainerId = ele.closest('ul').find('.ynuv_quick_list_playlist').prop('id');
        if(sInput == ""){
            parent.find('.ultimatevideo-error').css('display','block');
            return false;
        }
        parent.find('input').val('');
        $(ele).closest('ul').find('.fa-angle-down').toggle();
        $(ele).closest('ul').find('.fa-angle-up').toggle();
        $(".ultimatevideo-quick-add-form", $(ele).closest('ul')).toggle();
        $.ajaxCall('ultimatevideo.addPlaylistOnAction',$.param({id: ele.data('id'), sTitle: sInput,iVideoId: ele.data('id'),iContainerId : iContainerId}));
        return false;
    };
    videos.get_embed_code = function(ele){
        $('.ultimatevideo_video_detail-embed_code').toggleClass('hide');
    };
    videos.invite_friend = function(ele){
        alert('this feature is coming soon!');
    };
    videos.remove_video_from_playlist = function(obj){
        var ele = $(obj),
            parent = ele.closest('li.ui-sortable-handle'),
            removed = $('#ynuv_removed_video'),
            removedArr = removed.val();
        if(removedArr == "")
            removed.val(ele.data('video'));
        else
            removed.val(removedArr+','+ele.data('video'));
        parent.fadeOut();

    };

    videos.playlist_slideshow =  function(ele){
        if(!ele.length) return;
        var slider = new MasterSlider(),
            previousIndex = [0];

        slider.setup('ultimatevideo_playlist_slideshow_masterslider', {
            width : 1110,
            height : 550,
            space : 0,
            loop : false,
            view : 'basic',
            swipe : false,
            mouse : false,
            speed : 100,
            autoplay : false
        });

        slider.control('arrows');
        slider.control('thumblist', {autohide : false,  dir : 'h',width:300,height:80,});


        slider.api.addEventListener(MSSliderEvent.INIT , function(){
            var ynultimatevideo_contain_btn = $('.ultimatevide_playlist_mode_slide .ms-container');
            var ynultimatevideo_btn_action = $('.ultimatevideo_playbutton-block');

            if(ynultimatevideo_btn_action.length){
                ynultimatevideo_btn_action.appendTo(ynultimatevideo_contain_btn);
            }
            // console.log(ynultimatevideo_contain_btn);
        });

        var player = new ynultimatevideoPlayer(slider);

        slider.api.addEventListener(MSSliderEvent.CHANGE_START , function(){
            var current = slider.api.index();
            previousIndex.push(current);
            player.removeAllPlayers();
        });

        slider.api.addEventListener(MSSliderEvent.CHANGE_END , function(){
            player.updateCurrentSlideInfo();
            player.init(false);
        });

        window.ynultimatevideoPlay = function() {
            player.init(true);
        };

        window.ynultimatevideoNext = function() {
            var min = 0;
            var total = slider.api.count();
            var current = slider.api.index();
            var status = getPlayingStatus();
            if (status.shuffle) {
                slider.api.gotoSlide(generateRandom(current, min, total));
            } else if (status.repeat && current == total - 1) {
                slider.api.gotoSlide(0);
            } else if (current < total - 1) {
                slider.api.next();
            }
        };

        window.ynultimatevideoAutoNext = function() {
            var status = getPlayingStatus();
            if (status.auto_play) {
                ynultimatevideoNext();
            }
        };

        window.ynultimatevideoPrev = function() {
            var pos = previousIndex.pop();
            pos = previousIndex.pop();
            if (pos != null){
                slider.api.gotoSlide(pos);
            } else {
                slider.api.gotoSlide(0);
            }
        };
    };
    videos.playlist_slideshow_landingpage = function(ele){
        var sync1 = $("#ultimatevideo_slider_featured-1");
        var sync2 = $("#ultimatevideo_slider_featured-2");

        sync1.owlCarousel({
            singleItem: true,
            slideSpeed: 1000,
            navigation: false,
            autoPlay: true,
            pagination: false,
            // navigationText: ["<i class='ynicon yn-arr-left'></i>", "<i class='ynicon yn-arr-right'></i>"],
            afterAction: syncPosition,
            responsiveRefreshRate: 200,
        });

        sync2.owlCarousel({
            items: 6,
            singleItem: false,
            pagination: false,
            navigation: true,
            navigationText: ["<i class='ynicon yn-arr-left'></i>", "<i class='ynicon yn-arr-right'></i>"],
            responsiveRefreshRate: 100,
            itemsCustom : [
                [0, 2],
                [450, 4],
                [600, 6],
                [700, 6],
                [1000, 6],
                [1200, 6],
            ],
            afterInit: function(el) {
                el.find(".owl-item").eq(0).addClass("synced");
            },
            beforeInit : beforeInit,

        });

        function beforeInit(){
           var item_length = $("#ultimatevideo_slider_featured-2 .ultimatevideo-video-entry").length;

           if(item_length <= 6){
                $('#ultimatevideo_slider_featured-2').addClass('uv-nopadding');
           }
        }

        function syncPosition(el) {
            var current = this.currentItem;
            $("#ultimatevideo_slider_featured-2")
                .find(".owl-item")
                .removeClass("synced")
                .eq(current)
                .addClass("synced")
            if ($("#ultimatevideo_slider_featured-2").data("owlCarousel") !== undefined) {
                center(current)
            }
        }

        $("#ultimatevideo_slider_featured-2").on("click", ".owl-item", function(e) {
            e.preventDefault();
            var number = $(this).data("owlItem");
            sync1.trigger("owl.goTo", number);
        });

        function center(number) {
            var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
            var num = number;
            var found = false;
            for (var i in sync2visible) {
                if (num === sync2visible[i]) {
                    var found = true;
                }
            }

            if (found === false) {
                if (num > sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                } else {
                    if (num - 1 === -1) {
                        num = 0;
                    }
                    sync2.trigger("owl.goTo", num);
                }
            } else if (num === sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", sync2visible[1])
            } else if (num === sync2visible[0]) {
                sync2.trigger("owl.goTo", num - 1)
            }
        }
    }
})(UltimateVideo, jQuery);

$Ready(function() {
    var videoFrameClass = [
        '.youtube_iframe_big',
        '.youtube_iframe_small',
        '.vimeo_iframe_small',
        '.vimeo_iframe_big',
        '.facebook_iframe',
        '.dailymotions_iframe_small',
        '.dailymotions_iframe_big'
    ], videoAspec =  16/9;

    (function(eles){
        eles.each(function(index, ele){
            var $ele =  $(ele),
                parent = $ele.parent();

            $ele.data('built', true);
            $ele.css("width", parent.width());
            $ele.css("height", parent.width()/videoAspec);
        });
    })($(videoFrameClass.join(', ')).not('.built'));

    (function(ele){
        if(!ele.length) return;
        if(ele.prop('built')) return;
        ele.prop('built',true);
        var ultimatevideoSlideshowIntervalId;

        if(typeof $.fn.owlCarousel == 'undefined'){
            $Core.loadStaticFiles(ele.data('js'));
            // check interval timeout
            ultimatevideoSlideshowIntervalId = window.setInterval(function(){
                if(typeof $.fn.owlCarousel == 'undefined'){
                }else{
                    UltimateVideo.slideshows(ele);
                    window.clearInterval(ultimatevideoSlideshowIntervalId);
                }
            },250);
        }else{
            UltimateVideo.slideshows(ele);
        }
    })($('#ultimatevideo_slider_featured'));

    (function(ele){
        console.log('js_ultimatevideo_block_detail', ele.data('addjs'));
        if(!ele.length) return;

        ultimatevideoLoadValidJsIntervalId = window.setInterval(function(){
            if(typeof jQuery.validator == 'undefined'){
                $Core.loadStaticFile(ele.data('validjs'));
            }else{
                $Core.loadStaticFile(ele.data('addjs'));
                window.clearInterval(ultimatevideoLoadValidJsIntervalId);
            }
        },250); 


        // $Core.loadStaticFile(ele.data('validjs'));
        // $Core.loadStaticFile(ele.data('addjs'));

    })($('#js_ultimatevideo_block_detail'));

    (function(ele){

        if(!ele.length) return;
        $Core.loadStaticFile(ele.data('validjs'));
        $Core.loadStaticFile(ele.data('addjs'));

    })($('#js_ultimatevideo_playlist_block_detail'));

    (function(ele){
        if(!ele.length) return;
        if(ele.prop('built')) return;
        ele.prop('built',true);
        var ultimatevideoPlaylistSlideshowIntervalId,
            jsFiles = ele.data('js').split(',')
            ;
        $Core.loadStaticFiles(jsFiles);

        if(typeof window.MSLayerEffects == 'undefined'){
            // check interval timeout
            ultimatevideoPlaylistSlideshowIntervalId = window.setInterval(function(){
                if(typeof window.MSLayerEffects == 'undefined'){
                }else{
                    UltimateVideo.playlist_slideshow(ele);
                    window.clearInterval(ultimatevideoPlaylistSlideshowIntervalId);
                }
            },250);
        }else{
            UltimateVideo.playlist_slideshow(ele);
        }
    })($('#ultimatevideo_playlist_slideshow_masterslider'));

    (function(ele){
        if(!ele.length) return;
        if(ele.prop('built')) return;
        ele.prop('built',true);
        var ultimatevideoSlideshowPlaylistLandingIntervalId;

        if(typeof $.fn.owlCarousel == 'undefined'){
            $Core.loadStaticFiles(ele.data('js'));
            // check interval timeout
            ultimatevideoSlideshowPlaylistLandingIntervalId = window.setInterval(function(){
                if(typeof $.fn.owlCarousel == 'undefined'){
                }else{
                    UltimateVideo.playlist_slideshow_landingpage(ele);
                    window.clearInterval(ultimatevideoSlideshowPlaylistLandingIntervalId);
                }
            },250);
        }else{
            UltimateVideo.playlist_slideshow_landingpage(ele);
        }
    })($('#ultimatevideo_slider_featured-1'));

    $('.ultimatevideo_playlist_detail_actions-toggle').click(function(){
        $('.ms-thumb-list').toggle();
        $(this).toggleClass('active');
        $('.ultimatevide_playlist_mode_slide').toggleClass('uv-close');
    });

    //Support add video from feed
    if (ynuv_app_enabled != '1') {
        return;
    }
    if( $('.select-video-upload').length){
        $('.select-video-upload').parent().remove();
    }

    // Upload routine for videos
    var m = $('#page_core_index-member .activity_feed_form_attach, #panel .activity_feed_form_attach'), p = $('#page_pages_view .activity_feed_form_attach'), g = $('#page_groups_view .activity_feed_form_attach'), v = $('.select-ult-video-upload'), b = $('#ynuv_upload_form_input');
    if (m.length && !v.length) {
        var html = '<li><a href="#" class="select-ult-video-upload" rel="custom">' + uv_phrases.video + '</a></li>';

        m.append(html);
    }
    if (p.length && !v.length && can_post_ult_video_on_page == 1) {
        var html = '<li><a href="#" class="select-ult-video-upload" rel="custom">' + uv_phrases.video + '</a></li>';
        p.append(html);
    }

    if (g.length && !v.length && can_post_ult_video_on_group == 1) {
        var html = '<li><a href="#" class="select-ult-video-upload" rel="custom">' + uv_phrases.video + '</a></li>';
        g.append(html);
    }

    $('.activity_feed_form_attach a:not(.select-ult-video-upload)').click(function() {
        $('.process-ult-video-upload').remove();
        $('.activity_feed_form .error_message').remove();
    });

    $('.select-ult-video-upload').click(function() {

        $('.activity_feed_form_attach a.active').removeClass('active');
        $(this).addClass('active');
        $('.global_attachment_holder_section').hide().removeClass('active');
        $('.activity_feed_form_button').show();
        $('#activity_feed_submit').hide();

        $('#activity_feed_textarea_status_info').attr('placeholder', $('<div />').html(uv_phrases.say).text()).show();

        add_uv_video_button();
        var l = $('#global_attachment_videos');
        if (l.length == 0) {
            var m = $('<div id="global_attachment_videos" class="global_attachment_holder_section" style="display:block;"><div style="text-align:center;"><i class="fa fa-spin fa-circle-o-notch"></i></div></div>');
            $('.activity_feed_form_holder').prepend(m);

            $Core.ajax('ultimatevideo.loadFormAddOnFeed',
                {
                    type: "POST",
                    success: function(e){
                        m.html(e);
                        $('.activity_feed_form_button_status_info').show();
                    }
                }
            );
        }
        else {
            $('#ultvideoUpload').val('');
            $('#ynuv_add_video_code').val('');
            $('#ynuv_add_video_source').val('');
            l.show();
            $('.activity_feed_form_button_status_info').show();
            $Core.loadInit();
        }

        return false;
    });

    $('.process-ult-video-upload').click(function() {
        var t = $(this);

        t.hide();
        t.before('<span class="form-spin-it"><i class="fa fa-spin fa-circle-o-notch"></i></span>');
        var f = $(this).parents('form:first');
        f.find('.error_message').remove();
        if(ynultimatevideo_extract_code_on_feed($('#ynuv_add_video_input_link').val()) == false){

            $('.ynuv_upload_form').prepend('<div class="error_message">'+ uv_phrases.not_valid_video + '</div>');
            t.show();
            t.parent().find('span').remove();
            return false;
        }
        t.addClass('in_process');


        var data = new FormData(f);
        console.log(data);
        f.attr('action',PF.url.make('/ultimatevideo/upload'));
        f.submit();
        //$.ajaxCall('ultimatevideo.uploadVideoOnFeed',f.serialize(),'POST');
        return false;
    });


    var ynuv_url_changed = function() {
        $('.yn_uv_video_info').show();
        $('.yn_uv_video_url .extra_info').removeClass('hide_it');
        $('.uv_select_video').slideUp();
        $('#activity_feed_submit').removeClass('button_not_active'); // .attr('disabled', false);
        $('#__form_caption').parent().parent().hide();
        $('.activity_feed_form_button_status_info').show();
    };

    $('#__form_url').focus(ynuv_url_changed);

    $('.yn_uv_url_cancel').click(function() {
        $(this).parent().addClass('hide_it');
        $('.uv_select_video').slideDown();
        $('.yn_uv_video_url #__form_url').val('');
        var f = $(this).parents('form:first');
        f.find('.error_message').remove();
        $('.process-ult-video-upload').hide();
        $('.activity_feed_form_button_status_info').hide();
        // $('.yn_uv_video_info').hide();

        return false;
    });

    $('.yn_uv_upload_cancel').click(function() {
        $(this).parent().addClass('hide_it');
        $('.yn_uv_video_url').slideDown();
        var f = $(this).parents('form:first');
        f.find('.error_message').remove();
        $('.activity_feed_form_button_status_info').hide();

        return false;
    });

    $('.yn_uv_message_cancel').click(function() {
        $(this).parent().addClass('hide_it');
        $('.yn_uv_video_url').show();
        $('.uv_select_video').show();
        $('.uv_upload_form').slideDown();
        $('.uv_video_message').hide();
        $('.uv_upload_form .message').remove();
        $('.process-ult-video-upload').remove();
        $('#uv_video_id_temp').remove();
        $('.uv_video_caption input').val('');

        return false;
    });

    $('#js_activity_feed_form, .uv_is_popup').submit(function() {
        // console.log('form submit...');
        if ($('.select-ult-video-upload').hasClass('active')) {
            console.log('form submit...');
            // $(this).find('.btn-primary').attr('disabled', true);
        }
    });
});
var ynuv_videoUpload = function(e) {
    var t = $('.process-ult-video-upload');
        t.hide();
        t.before('<span class="form-spin-it"><i class="fa fa-spin fa-circle-o-notch"></i></span>');

    var files = e.target.files || e.dataTransfer.files;
    for (var i = 0, f; f = files[i]; i++) {
        $.ajax({
            type: "POST",
            url: PF.url.make('/ultimatevideo/upload'),
            data: new FormData(),
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData: false,
            success: function (e) {
            }
        });
    }
};
var add_uv_video_button = function() {
    if($('.process-ult-video-upload').length) return;
    $('#activity_feed_submit').before('<a href="#" class="button btn-lg btn-primary process-ult-video-upload">'+uv_phrases.save+'</a>');
    $Core.loadInit();
};
var ynultimatevideo_extract_code_on_feed = function(url) {

    if($('#ultvideoUpload').val() != '')
    {
        $('#ynuv_add_video_source').val('Uploaded');
        return true;
    }

    var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
    if(videoid){
        $('#ynuv_add_video_source').val('Youtube');
        $('#ynuv_add_video_code').val(videoid[1]);
        return true;
    }

    var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?vimeo.com\/(\d+)($|\/)/);
    if (videoid){
        $('#ynuv_add_video_source').val('Vimeo');
        $('#ynuv_add_video_code').val(videoid[1]);
        return true;
    }

    var videoid = url.match(/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/);
    if (videoid){
        $('#ynuv_add_video_source').val('Dailymotion');
        $('#ynuv_add_video_code').val(videoid[2]);
        return true;
    }

    var videoid = url.match(/http(?:s?):\/\/(?:www\.|web\.|m\.)?facebook\.com\/([A-z0-9\.]+)\/videos(?:\/[0-9A-z].+)?\/(\d+)(?:.+)?$/);
    if (videoid){
        $('#ynuv_add_video_source').val('Facebook');
        $('#ynuv_add_video_code').val(videoid[2]);
        return true;
    }

    var ext = url.substr(url.lastIndexOf('.') + 1);
    if (ext.toUpperCase() == 'MP4'){
        $('#ynuv_add_video_source').val('VideoURL');
        return true;
    }
    var code = url.match(/(<iframe.*? src=(\"|\'))(.*?)((\"|\').*)/);
    if(code && code.length > 2)
    {
        $('#ynuv_add_video_source').val('Embed');
        $("#ynuv_add_video_code").val(code[3]);
        return true;
    }
    return false;
}

function getPlaylistToQuickAddVideo(obj)
{
    var ele  = $(obj),
        videoId = ele.data('id'),
        imgPath = ele.data('imgpath'),
        isExpand = ele.attr('aria-expanded'),
        container = $('.ynuv_quick_list_playlist', ele.closest('div'));

    if(!container.prop('id')){
        container.prop('id', UltimateVideo.makeId());
    }

    if(isExpand == 'false'){
        $(".ynuv_quick_list_playlist_" + videoId).html('<div class="text-center"><img src="'+imgPath+'"/></div>');
    }

    $.ajaxCall('ultimatevideo.getAllPlaylistOfUser',$.param({id: videoId, eleId: container.prop('id')}));
}

        