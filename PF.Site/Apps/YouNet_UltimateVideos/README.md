## Installation & Upgrade Guide for Ultimate Videos App
Please follow all steps in this installation guide in order to make this app work properly

If you need any assistance, please submit a new ticket at the client area (http://phpfox.younetco.com/client)

### Requirements
1. phpFox version: Please check ChangeLog.html for the best compatible version.

### Installation Steps
1. Install YouNet Ultimate Videos App on the phpFox Store
2. Rebuild Bootstrap Core (at AdminCP > Maintenance > Rebuild Bootstrap Core).
3. Clear your cache site (at AdminCP > Maintenance > Cache Manager > Clear Cache).
4. Set up cron tab:
	- Video Guide: http://knowledgebase.younetco.com/2016/09/06/v4-ultimate-videos-video-guide-for-setting-up-cronjob/
	- The url of cron: http://sitename.com/PF.Site/apps/YouNet_UltimateVideos/cron.php
### Upgrade Steps
1. Upgrade our latest version.
2. Rebuild Bootstrap Core (at AdminCP > Maintenance > Rebuild Bootstrap Core).
3. Clear your cache site (at AdminCP > Maintenance > Cache Manager > Clear Cache)

#### Congratulations! You finished the Installation/Upgrade process.