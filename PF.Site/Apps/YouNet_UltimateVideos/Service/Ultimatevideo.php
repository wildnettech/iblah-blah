<?php

/**
 * [PHPFOX_HEADER]
 */
namespace Apps\YouNet_UltimateVideos\Service;

use Apps\YouNet_UltimateVideos\Adapter;
use Phpfox;
use Phpfox_Service;
use Phpfox_Pages_Category;
use Phpfox_Plugin;
use Phpfox_Error;
defined('PHPFOX') or exit('NO DICE!');

/**
 *
 *
 * @copyright      YouNet Company
 * @author         HaiNM
 * @package        Module_UltimateVideo
 * @version        4.01
 */
class Ultimatevideo extends Phpfox_Service
{
	private $_aCallback = null;

    public function __construct()
    {
        $this->_sTable = Phpfox::getT('ynultimatevideo_videos');
    }



	public function getCustomFieldByCategoryId($iCategoryId)
    {
        $sWhere = '';
        $sWhere .= ' AND ccd.category_id = ' . (int)$iCategoryId;
        $aFields = $this->database()
            ->select('cfd.*')
            ->from(Phpfox::getT("ynultimatevideo_category_customgroup_data"), 'ccd')
            ->join(Phpfox::getT('ynultimatevideo_custom_group'), 'cgr', ' ( cgr.group_id = ccd.group_id AND cgr.is_active = 1 ) ')
            ->join(Phpfox::getT('ynultimatevideo_custom_field'), 'cfd', ' ( cfd.group_id = cgr.group_id ) ')
            ->where('1=1' . $sWhere)
            ->order('cgr.group_id ASC , cfd.ordering ASC, cfd.field_id ASC')
            ->execute("getSlaveRows");

        $aHasOption = Phpfox::getService('ultimatevideo.custom')->getHasOption();
        if (is_array($aFields) && count($aFields)) {
            foreach ($aFields as $k => $aField) {
                if (in_array($aField['var_type'], $aHasOption)) {
                    $aOptions = $this->database()->select('*')->from(Phpfox::getT('ynultimatevideo_custom_option'))->where('field_id = ' . $aField['field_id'])->order('option_id ASC')->execute('getSlaveRows');
                    if (is_array($aOptions) && count($aOptions)) {
                        foreach ($aOptions as $k2 => $aOption) {
                            $aFields[$k]['option'][$aOption['option_id']] = $aOption['phrase_var_name'];
                        }
                    }
                }
            }
        }

        return $aFields;
    }
    public function getClass($cName)
    {
        $name = 'Apps\\YouNet_UltimateVideos\\Adapter\\' . $cName;
        if (class_exists($name)) {
            return new $name;
        }

        return null;
    }
    public function getSourTypeIdFromName($sName)
    {
        $sSoureId = array(
            "Youtube" => "1",
            "Vimeo" => "2",
            "Uploaded" => "3",
            "Dailymotion" => "4",
            "VideoURL" => "5",
            "Embed" => "6",
            "Facebook" => "7",
        );
        $sId = $sSoureId[$sName];
        return $sId;
    }
    public function getSourTypeNameFromId($sId)
    {
        $sSoureId = array(
            "1" => "Youtube",
            "2" => "Vimeo" ,
            "3" => "Uploaded",
            "4" => "Dailymotion",
            "5" => "VideoURL",
            "6" => "Embed",
            "7" => "Facebook",
        );
        $sName = $sSoureId[$sId];
        return $sName;
    }

    public function getManageVideo($aConds = array(), $iPage = 0, $iLimit = NULL, $iCount = NULL)
    {
        $sWhere = '';
        $sWhere .= '1=1';
        if (count($aConds) > 0) {
            $sCond = implode('  ', $aConds);
            $sWhere .= ' ' . $sCond;
        }
        $iCount = $this->database()
            ->select("COUNT(uvid.video_id)")
            ->from($this->_sTable, 'uvid')
            ->join(Phpfox::getT("user"), 'u', 'uvid.user_id =  u.user_id')
            ->join(Phpfox::getT('ynultimatevideo_category'), 'uvc', 'uvc.category_id = uvid.category_id')
            ->where($sWhere)
            ->execute("getSlaveField");
        $aVideos= array();
        if($iCount){
            $aVideos = $this->database()
                    ->select("uvid.*,uvc.title as category_title,". Phpfox::getUserField())
                    ->from($this->_sTable,'uvid')
                    ->join(Phpfox::getT("user"), 'u', 'uvid.user_id =  u.user_id')
                    ->join(Phpfox::getT('ynultimatevideo_category'), 'uvc', 'uvc.category_id = uvid.category_id')
                    ->where($sWhere)
                    ->order('uvid.video_id DESC')
                    ->limit($iPage,$iLimit,$iCount)
                    ->execute('getSlaveRows');
            foreach ($aVideos as $key => $aVideo){
                $aVideos[$key]['source'] = $this->getSourTypeNameFromId($aVideo['type']);
            }
        }
        
        return array($iCount,$aVideos);                
    }
    public function getVideoOwnerId($iVideoId)
    {
        return $this->database()->select('user_id')->from($this->_sTable)->where("video_id = {$iVideoId}")->execute('getSlaveField');
    }
    public function getOwnerEmail($iUserId)
    {
        return $this->database()->select('email')->from(Phpfox::getT('user'))->where('user_id = ' . $iUserId)->execute('getField');
    }    
    public function countVideoOfUserId($iUserId = 0)
    {
        $sWhere = '';
        $sWhere .= ' AND uvid.user_id = ' . (int)$iUserId;
        $iCount = $this->database()
            ->select("COUNT(uvid.video_id)")
            ->from($this->_sTable, 'uvid')
            ->where('1=1' . $sWhere)
            ->innerJoin(Phpfox::getT('user'), 'u', 'u.user_id = uvid.user_id')
            ->innerJoin(Phpfox::getT('ynultimatevideo_category'), 'uvc', 'uvc.category_id = uvid.category_id')
            ->execute("getSlaveField");

        return $iCount;        
    }

    public function getVideoForEdit ($iVideoId)
    {
        $aVideo = $this->database()->select("v.*")
                        ->from($this->_sTable,'v')
                        ->join(Phpfox::getT('user'), 'u', 'u.user_id = v.user_id')
                        ->where('v.video_id ='.(int)$iVideoId)
                        ->execute('getSlaveRow');

        if($aVideo){
            $aCategories = array();
            $aCategories[] = $aVideo['category_id'];
            $iParent = Phpfox::getService('ultimatevideo.category')->getParentCategoryId($aVideo['category_id']);
            while($iParent != 0){
                $aCategories[] = $iParent;
                $iParent = Phpfox::getService('ultimatevideo.category')->getParentCategoryId($iParent);
            }
            $aVideo['categories'] = json_encode($aCategories);
            return $aVideo;                
        }
        else {
            return false;
        }
    }

    public function getPlaylist($iPlaylistId)
    {
        if (Phpfox::isModule('friend'))
        {
            $this->database()->select('f.friend_id AS is_friend, ')->leftJoin(Phpfox::getT('friend'), 'f', "f.user_id = playlist.user_id AND f.friend_user_id = " . Phpfox::getUserId());
        }

        if (Phpfox::isModule('like'))
        {
            $this->database()->select('l.like_id AS is_liked, ')
                ->leftJoin(Phpfox::getT('like'), 'l', 'l.type_id = \'ultimatevideo_playlist\' AND l.item_id = playlist.playlist_id AND l.user_id = ' . Phpfox::getUserId());
        }

        $aRow = $this->database()->select("playlist.*, " . Phpfox::getUserField())
            ->from(Phpfox::getT('ynultimatevideo_playlists'), 'playlist')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = playlist.user_id')
            ->where('playlist.playlist_id = ' . (int) $iPlaylistId)
            ->execute('getSlaveRow');
        if(!$aRow)
        {
            return false;
        }
                                    
        (($sPlugin = Phpfox_Plugin::get('ultimatevideo.component_service_video_getplaylist__end')) ? eval($sPlugin) : false);

        if (!isset($aRow['is_friend']))
        {
            $aRow['is_friend'] = 0;
        }
        $aRow['bookmark_url'] =  Phpfox::permalink('ultimatevideo.playlist', $aRow['playlist_id'], $aRow['title']);
        $aRow['description'] = strip_tags($aRow['description']);
        return $aRow;
    }

    public function getVideo($iVideoId,$bView = true)
    {
        if (Phpfox::isModule('friend'))
        {
            $this->database()->select('f.friend_id AS is_friend, ')->leftJoin(Phpfox::getT('friend'), 'f', "f.user_id = video.user_id AND f.friend_user_id = " . Phpfox::getUserId());
        }

        if (Phpfox::isModule('like'))
        {
            $this->database()->select('l.like_id AS is_liked, ')
                ->leftJoin(Phpfox::getT('like'), 'l', 'l.type_id = \'ultimatevideo_video\' AND l.item_id = video.video_id AND l.user_id = ' . Phpfox::getUserId());
        }

        $aRow = $this->database()->select("video.*, " . Phpfox::getUserField())
            ->from($this->_sTable, 'video')
            ->join(Phpfox::getT('user'), 'u', 'u.user_id = video.user_id')
            ->where('video.video_id = ' . (int) $iVideoId)
            ->execute('getSlaveRow');
        if(!$aRow)
        {
            return false;
        }
        (($sPlugin = Phpfox_Plugin::get('ultimatevideo.component_service_video_getvideo__end')) ? eval($sPlugin) : false);

        if (!isset($aRow['is_friend']))
        {
            $aRow['is_friend'] = 0;
        }

        if(isset($aRow['type']))
        {
            $sVideoPath = Phpfox::getParam('core.path_actual').'PF.Base/file/ynultimatevideo/'.sprintf($aRow['video_path'],'');
            if(Phpfox::getParam('core.allow_cdn') && $aRow['video_server_id'] > 0 && $aRow['type'] == 3){
                $sVideoPath = Phpfox::getLib('cdn')->getUrl(Phpfox::getParam('core.path_file').'file/ynultimatevideo/'.sprintf($aRow['video_path'],''),$aRow['video_server_id']);
            }
            $sSourceType = Phpfox::getService('ultimatevideo') -> getSourTypeNameFromId($aRow['type']);
            $adapter = Phpfox::getService('ultimatevideo') -> getClass($sSourceType);
            $aParams = array(
                'video_id' => $aRow['video_id'],
                'code'	=> $aRow['code'],
                'view'	=> $bView,
                'mobile' => Phpfox::getService('ultimatevideo')->isMobile(),
                'count_video'=> 0,
                'location' => $aRow['code'],
                'location1' => $sVideoPath,
                'duration' => $aRow['duration']
            );

            $embedCode = $adapter -> compileVideo($aParams);

        }
        $aRow['description'] = strip_tags($aRow['description']);
        $aRow['embed_code'] =  $embedCode;

        $aRow['sTags'] = $this->getHtmlTagString($aRow['video_id'],'ynultimatevideo');

        $aRow['bookmark_url'] =  Phpfox::permalink('ultimatevideo', $aRow['video_id'], $aRow['title']);
        return $aRow;
    }

    /**
     * @param $iVideoId
     * @return string
     */
    public function getHtmlTagString($iVideoId, $type)
    {

        return implode(', ', array_map(function($temp){
        return strtr('<a href=":link">:text</a>',[
            ':text'=>$temp['tag_text'],
            ':link'=> url('ultimatevideo',['tag'=> $temp['tag_text']])]);
    }, $this->database()
        ->select('tag_text')
        ->from(Phpfox::getT('tag'))
        ->where(strtr("category_id=':type' AND item_id=:id",[
            ':type'=>$type,
            ':id'=> intval($iVideoId)
        ]))
        ->execute('getSlaveRows')));

    }
    public function isMobile()
    {
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $user_agent = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('/(android|iphone|ipad|mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|wireless| mobi|ahong|lg380|lgku|lgu900|lg210|lg47|lg920|lg840|lg370|sam-r|mg50|s55|g83|t66|vx400|mk99|d615|d763|el370|sl900|mp500|samu3|samu4|vx10|xda_|samu5|samu6|samu7|samu9|a615|b832|m881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|p404i|s210|c5100|teleca|s940|c500|s590|foma|samsu|vx8|vx9|a1000|_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|s800|8325rc|ac831|mw200|brew |d88|htc\/|htc_touch|355x|m50|km100|d736|p-9521|telco|sl74|ktouch|m4u\/|me702|8325rc|kddi|phone|lg |sonyericsson|samsung|240x|x320vx10|nokia|sony cmd|motorola|up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|pocket|kindle|mobile|psp|treo)/i', $user_agent)) {
                return true;
            }

        } else {
            return false;
        }

        return false;
    }
    public function getParentCategoryOfVideo($iVideoId)
    {
        $aVideo = $this->database()->select('vi.category_id as category_id,ct.parent_id as parent_id')
                        ->from($this->_sTable,'vi')
                        ->join(Phpfox::getT('ynultimatevideo_category'),'ct','vi.category_id = ct.category_id')
                        ->where('vi.video_id ='.(int)$iVideoId)
                        ->execute('getSlaveRow');

        $iParentCategoryId = $aVideo['category_id'];
        if($aVideo)
        {
            if(isset($aVideo['parent_id']) && (int)$aVideo['parent_id'] > 0){
                $iParentCategoryId = (int)$aVideo['parent_id'];
            }
            $iNewParentId = $iParentCategoryId;
            while($iNewParentId != 0){
                $iNewParentId = Phpfox::getService('ultimatevideo.category')->getParentCategoryId($iParentCategoryId);
                if($iNewParentId != 0){
                    $iParentCategoryId = $iNewParentId;
                }
            }
        }
        return $iParentCategoryId;
    }

}