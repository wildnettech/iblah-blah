<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/10/16
 * Time: 2:11 PM
 */

namespace Apps\YouNet_UltimateVideos\Service;
use Phpfox;
use Friend_Service_Friend;
use Phpfox_Search;

class Browse extends \Phpfox_Service
{

    public function sample($iLimit, $sOrder, $aWhere =  [], $bIsUserRelateVideo = false)
    {
        $db =  $this->database();

        $db->select('video.* ,' .Phpfox::getUserField())
            ->from(Phpfox::getT('ynultimatevideo_videos'),'video')
            ->join(Phpfox::getT('user'),'u','u.user_id=video.user_id')
            ->limit(0, intval($iLimit))
            ->order($sOrder);

        $aWhere[] =  'video.is_approved=1 AND video.status=1 AND video.module_id !="pages" AND video.module_id !="groups"';
        if (Phpfox::getParam('core.friends_only_community') && Phpfox::isModule('friend')){
            $db->join(Phpfox::getT('friend'), 'friends', 'friends.user_id = video.user_id AND friends.friend_user_id = ' . Phpfox::getUserId())
                ->where(implode(' AND ', $aWhere))->union();
            if(!$bIsUserRelateVideo){    
                $aWhere[] = 'video.user_id='.Phpfox::getUserId();
                $db->select('video.* ,' .Phpfox::getUserField())
                    ->from(Phpfox::getT('ynultimatevideo_videos'),'video')
                    ->join(Phpfox::getT('user'),'u','u.user_id=video.user_id')
                    ->where(implode(' AND ', $aWhere))
                    ->limit(0, intval($iLimit))
                    ->order($sOrder)
                    ->union();
            }
        }
        else{
            $db->where(implode(' AND ', $aWhere));
        }
        

        return $db->execute('getSlaveRows');
    }

    public function getMostCommentedVideos($iLimit)
    {
        return $this->sample($iLimit, 'video.total_comment DESC');
    }

    public function getMostLikedVideos($iLimit)
    {
        return $this->sample($iLimit, 'video.total_like DESC');
    }

    public function getMostFavouriteVideos($iLimit)
    {
        return $this->sample($iLimit, 'video.total_favorite DESC');
    }

    public function getMostViewedVideos($iLimit)
    {
        return $this->sample($iLimit, 'video.total_view DESC');
    }

    public function getMostRecentVideos($iLimit)
    {
        return $this->sample($iLimit, 'video.time_stamp DESC');
    }

    public function getMostFeaturedVideos($iLimit)
    {
        return $this->sample($iLimit, 'video.time_stamp DESC', ['video.is_featured=1']);
    }

    public function getRecommendedCategoryIdList()
    {
        return array_map(function ($tmp){
                return $tmp['cid'];
            },
            $this->database()
                ->select('distinct(v.category_id) as cid')
                ->from(Phpfox::getT('ynultimatevideo_videos'),'v')
                ->join(Phpfox::getT('ynultimatevideo_history'),'h','h.item_id=v.video_id AND h.item_type=0 AND h.user_id='. intval(Phpfox::getUserId()))
                ->where('v.category_id <>0')
                ->execute('getSlaveRows'));

    }

    public function getRandomVideos($iLimit, $where = [])
    {
        $where [] =  'video.status=1';
        $where [] = 'video.is_approved=1';
        $where = implode(' AND ', $where);

        $this->database()
            ->select('video.* ,' .Phpfox::getUserField())
            ->from(Phpfox::getT('ynultimatevideo_videos'),'video')
            ->join(Phpfox::getT('user'),'u','u.user_id=video.user_id')
            ->where($where)
            ->order('RAND()')
            ->limit(0, intval($iLimit));
        if (Phpfox::getParam('core.friends_only_community') && Phpfox::isModule('friend')){
            $this->database()->join(Phpfox::getT('friend'), 'friends', 'friends.user_id = video.user_id AND friends.friend_user_id = ' . Phpfox::getUserId())->union();
            $this->database()->select('video.* ,' .Phpfox::getUserField())
                ->from(Phpfox::getT('ynultimatevideo_videos'),'video')
                ->join(Phpfox::getT('user'),'u','u.user_id=video.user_id')
                ->where($where.' AND video.user_id ='.Phpfox::getUserId())
                ->order('RAND()')
                ->limit(0, intval($iLimit))->union();
        }            
        return $this->database()->execute('getSlaveRows');
    }

    public function getMostRecommendedVideos($iLimit)
    {
        if(!Phpfox::getUserId()){
            return $this->getRandomVideos($iLimit);
        }

        $aCategoryIds =  $this->getRecommendedCategoryIdList();

        if(empty($aCategoryIds)){
            return $this->getRandomVideos($iLimit);
        }

        $sub_sql = strtr('select item_id from :history as h where h.item_type=0 and h.user_id=:user',[
            ':history'=> Phpfox::getT('ynultimatevideo_history'),
            ':user'=> intval(Phpfox::getUserId()),
        ]);

        $eachLimit = ceil($iLimit/count($aCategoryIds));
        foreach($aCategoryIds as $id){
            if (Phpfox::getParam('core.friends_only_community') && Phpfox::isModule('friend')){
                $this->database()->select('v.*, '. Phpfox::getUserField());
            }
            else{
                $this->database()->select('v.*');
            }
                $this->database()->from(Phpfox::getT('ynultimatevideo_videos'),'v')
                ->join(Phpfox::getT('user'), 'u', 'u.user_id = v.user_id')
                ->where('v.is_approved=1 and v.status=1 AND v.category_id ='.intval($id).' AND v.video_id NOT IN ('.$sub_sql.')' )
                ->limit($eachLimit)
                ->order('RAND()')
                ->union();
        }

        if (Phpfox::getParam('core.friends_only_community') && Phpfox::isModule('friend')){
            $this->database()
                ->select('video.*, '. Phpfox::getUserField())
                ->from(Phpfox::getT('ynultimatevideo_videos'),'video')
                ->join(Phpfox::getT('user'), 'u', 'u.user_id = video.user_id');
            $this->database()->join(Phpfox::getT('friend'), 'friends', 'friends.user_id = video.user_id AND friends.friend_user_id = ' . Phpfox::getUserId())->union();
                
            $this->database()->select('v.* ,' .Phpfox::getUserField())
                ->from(Phpfox::getT('ynultimatevideo_videos'),'v')
                ->join(Phpfox::getT('user'),'u','u.user_id=v.user_id')
                ->where('v.is_approved=1 and v.status=1 AND v.category_id ='.intval($id).' AND v.video_id NOT IN ('.$sub_sql.') AND v.user_id='.Phpfox::getUserId() )
                ->limit($eachLimit)
                ->order('RAND()')
                ->union();
        }  
        else{
            $this->database()
                ->select('video.*, '. Phpfox::getUserField())
                ->unionFrom('video')
                ->join(Phpfox::getT('user'), 'u', 'u.user_id = video.user_id');
        }

        $result =  $this->database()->execute('getSlaveRows');

        $total = count($result);
        if($total < $iLimit){
            $ids = array_map(function($tmp){
                return $tmp['video_id'];
            }, $result);
            $ids[ ]= 0;
            $rows  =  $this->getRandomVideos($iLimit -  $total,['video.video_id NOT IN('.implode(',',$ids).')']);

            foreach($rows as $row){
                $result[] = $row;
            }
        }else if ($total>$iLimit){
            $result = array_splice($result, $total, 0, $iLimit);
        }

        return $result;
    }

    public function getSlideshowVideos($iLimit)
    {
        return $this->sample($iLimit,'video.video_id DESC',['video.is_featured = 1']);
    }

    public function getUserPostedVideos($iLimit, $iUserId)
    {
        return $this->sample($iLimit, 'video.total_view DESC', ['video.user_id='. intval($iUserId),'video.video_id !='.ULTIMATE_VIDEO_ID],true);
    }

    public function getWatchItAgainVideos($iLimit)
    {
        return $this->database()
            ->select('video.* ,' .Phpfox::getUserField())
            ->from(Phpfox::getT('ynultimatevideo_videos'),'video')
            ->join(Phpfox::getT('user'),'u','u.user_id=video.user_id')
            ->join(Phpfox::getT('ynultimatevideo_history'),'ha', 'ha.user_id='. intval(Phpfox::getUserId()) .' AND ha.item_type=0 AND ha.item_id=video.video_id')
            ->where('video.is_approved = 1')
            ->limit(0, intval($iLimit))
            ->order('rand()')
            ->execute('getSlaveRows');

    }

    public function getMostRelatedVideos($iLimit)
    {
        if(!defined('ULTIMATE_VIDEO_CATEGORY_ID')){
            return $this->getRandomVideos($iLimit);
        }

        return $this->getRandomVideos($iLimit, ['video.category_id='. ULTIMATE_VIDEO_CATEGORY_ID,'video.video_id !='.ULTIMATE_VIDEO_ID]);

    }

    public function getTagCloud($mMaxDisplay)
    {
        return get_from_cache(['ultimatevideo.video','tags'],function() use ($mMaxDisplay){
            return array_map(function ($row){
                return [
                    'text'=> $row['tag'],
                    'link'=> \Phpfox_Url::instance()->makeUrl('ultimatevideo',['tag'=> $row['tag']]),
                    'total' => $row['total']
                ];
            },$this->database()->select('category_id, tag_text AS tag, tag_url, COUNT(item_id) AS total')
                ->from(Phpfox::getT('tag'))
                ->where('category_id=\'ynultimatevideo\'')
                ->group('tag_text, tag_url')
                ->order('total DESC')
                ->limit($mMaxDisplay)
                ->execute('getSlaveRows'));

        }, 1);

    }

    public function query()
    {

        if (Phpfox::isUser() && Phpfox::isModule('like'))
        {
        }

    }

    public function getQueryJoins($bIsCount = false, $bNoQueryFriend = false)
    {
        if (Phpfox::isModule('friend') && Friend_Service_Friend::instance()->queryJoin($bNoQueryFriend))
        {
            $this->database()->join(Phpfox::getT('friend'), 'friends', 'friends.user_id = video.user_id AND friends.friend_user_id = ' . Phpfox::getUserId());
        }

        if(null != ($sTag = $this->request()->get('tag'))) {
            $sTag = $this->database()->escape($sTag);
            $this->database()->join(Phpfox::getT('tag'), 'tag', "(tag.category_id='ynultimatevideo' AND tag.item_id=video.video_id AND tag.tag_text='{$sTag}')");
        }

        $view =  $this->request()->get('view');
        switch($view){
            case 'history':
                $mParam = 'history.item_id=video.video_id AND history.item_type=0 AND history.user_id='. intval(Phpfox::getUserId());
                $this->database()->join(Phpfox::getT('ynultimatevideo_history'),'history',$mParam);
                break;
            case 'later':
                $mParam = 'wl.video_id=video.video_id AND wl.user_id='. intval(Phpfox::getUserId());
                $this->database()->join(Phpfox::getT('ynultimatevideo_watchlaters'),'wl',$mParam);
                break;
            case 'my':

                break;
            case 'favorite':
                $this->database()->join(Phpfox::getT('ynultimatevideo_favorites'),'fa', 'fa.video_id=video.video_id AND fa.user_id='. intval(Phpfox::getUserId()));
                break;
        }

        if (Phpfox::getParam('core.section_privacy_item_browsing'))
        {
            if ($this->search()->isSearch())
            {

            }
        }
        else
        {
            if ($bIsCount && $this->search()->isSearch())
            {

            }
        }
    }

    public function processRows(&$aRows)
    {
        if(empty($aRows))
            return ;

        $sVideoIds =  implode(',', array_map(function ($video){return $video['video_id'];}, $aRows));

        $aWatchlaters = array_map(function ($temp){
            return $temp['video_id'];
        },$this->database()->select('video_id')
            ->from(Phpfox::getT('ynultimatevideo_watchlaters'),'wl')
            ->where('video_id IN ('. $sVideoIds.')')
            ->execute('getSlaveRows'));

        foreach($aRows as $index=> $aRow){
            if($aRow['type'] == '1' && $aRow['duration'] == 0 )
            {
                $adapter = Phpfox::getService('ultimatevideo') -> getClass('Youtube');;
                $adapter -> setParams(array(
                        'code' => $aRow['code'],
                        'video_id' => $aRow['video_id']
                ));
                $vImagePath = "";
                if($adapter -> getVideoLargeImage())
                    $vOriginalImagePath = $adapter -> getVideoLargeImage();
                    $vImagePath = Phpfox::getService('ultimatevideo.process')->downloadImage($vOriginalImagePath);
                $vDuration = 0;
                if($adapter -> getVideoDuration())
                    $vDuration =  $adapter -> getVideoDuration();
                //update image
                if(!empty($aRow['image_path']))
                {
                        $sImagePath = $aRow['image_path'];
                        $aImages = array(
                            Phpfox::getParam('core.dir_pic') . sprintf($sImagePath, '_120'),
                            Phpfox::getParam('core.dir_pic') . sprintf($sImagePath, '_250'),
                            Phpfox::getParam('core.dir_pic') . sprintf($sImagePath, '_500'),
                            Phpfox::getParam('core.dir_pic') . sprintf($sImagePath, '_1024')
                            );
                        foreach ($aImages as $sImage)
                        {
                            if (file_exists($sImage))
                            {
                                @unlink($sImage);
                            }
                        }
                }
                $this->database()->update(Phpfox::getT('ynultimatevideo_videos'),['image_path' => $vImagePath,'duration' => $vDuration],'video_id='.$aRow['video_id']);
            }
            $aRows[$index]['watchlater'] = in_array($aRow['video_id'],$aWatchlaters);
        }
    }
}