<?php
/**
 * Created by PhpStorm.
 * User: namnv
 * Date: 8/11/16
 * Time: 5:01 PM
 */

namespace Apps\YouNet_UltimateVideos\Service\Playlist;

use Phpfox_Service;
use Friend_Service_Friend;
use Phpfox;

class Browse extends Phpfox_Service
{

    public function sample($iLimit, $sOrder, $aWhere = [], $bIsUserRelatePlaylist = false)
    {
        $db =  $this->database();

        $db->select('playlist.* ,' .Phpfox::getUserField())
            ->from(Phpfox::getT('ynultimatevideo_playlists'),'playlist')
            ->join(Phpfox::getT('user'),'u','u.user_id=playlist.user_id')
            ->limit(0, intval($iLimit))
            ->order($sOrder);

        $aWhere[] =  'playlist.is_approved=1';
        if (Phpfox::getParam('core.friends_only_community') && Phpfox::isModule('friend')){
            $db->join(Phpfox::getT('friend'), 'friends', 'friends.user_id = playlist.user_id AND friends.friend_user_id = ' . Phpfox::getUserId())
                ->where(implode(' AND ', $aWhere))
                ->union();
            if(!$bIsUserRelatePlaylist){
                $aWhere[] = 'playlist.user_id='.Phpfox::getUserId();
                $db->select('playlist.* ,' .Phpfox::getUserField())
                    ->from(Phpfox::getT('ynultimatevideo_playlists'),'playlist')
                    ->join(Phpfox::getT('user'),'u','u.user_id=playlist.user_id')
                    ->where(implode(' AND ', $aWhere))
                    ->limit(0, intval($iLimit))
                    ->order($sOrder)
                    ->union();
            }
        }   
        else{
            $db->where(implode(' AND ', $aWhere));
        }
        $aPlaylists = $db->execute('getSlaveRows');
        if(count($aPlaylists))
        {
            foreach ($aPlaylists as $key => $aPlaylist) {
                $aPlaylists[$key]['video_list'] = $this->getSomeVideoOfPlaylist($aPlaylist['playlist_id']);
            }
        }
        return $aPlaylists;
    }

    public function getMostRecentPlaylists($iLimit)
    {
        return $this->sample($iLimit, 'playlist.time_stamp DESC');
    }

    public function getMostFeaturedPlaylists($iLimit)
    {
        // missing featured
        return $this->sample($iLimit, 'playlist.total_video DESC',[]);
    }

    public function getMostRelatedPlaylists($iLimit)
    {
        if(!defined('ULTIMATE_PLAYLIST_CATEGORY_ID')){
            return $this->getRandomPlaylists($iLimit);
        }

        return $this->getRandomPlaylists($iLimit, ['playlist.category_id='. ULTIMATE_PLAYLIST_CATEGORY_ID,'playlist.playlist_id !='.ULTIMATE_PLAYLIST_ID]);

    }

    public function getMostViewedPlaylists($iLimit)
    {
        return $this->sample($iLimit, 'playlist.total_view DESC');
    }

    public function getMostCommentedPlaylists($iLimit)
    {
        return $this->sample($iLimit, 'playlist.total_comment DESC');
    }

    public function getMostLikedPlaylists($iLimit)
    {
        return $this->sample($iLimit, 'playlist.total_like DESC');
    }

    public function getMostRecommendedPlaylists($iLimit)
    {
        return $this->sample($iLimit, 'playlist.total_like DESC');
    }

    public function getSlideshowPlaylists($iLimit)
    {
        return $this->sample($iLimit, 'playlist.playlist_id DESC',['playlist.is_featured = 1']);
    }

    public function getUserPostedPlaylists($iLimit, $iUserId =1)
    {
        return $this->sample($iLimit, 'playlist.time_stamp DESC', ['playlist.user_id='. intval($iUserId),'playlist.playlist_id !='.ULTIMATE_PLAYLIST_ID],true);
    }

    public function getQueryJoins($bIsCount = false, $bNoQueryFriend = false)
    {
        $view =  $this->request()->get('view');
        if (Phpfox::isModule('friend') && $view == 'friendplaylist')
        {
            $this->database()->join(Phpfox::getT('friend'), 'friends', 'friends.user_id = playlist.user_id AND friends.friend_user_id = ' . Phpfox::getUserId());
        }
    }

    public function getTagCloud($mMaxDisplay)
    {

        return get_from_cache(['ultimatevideo.playlist','tags'],function() use ($mMaxDisplay){
            return array_map(function ($row){
                return [
                    'text'=> $row['tag'],
                    'link'=> \Phpfox_Url::instance()->makeUrl('ultimatevideo.playlist',['tag'=> $row['tag']])
                ];
            },$this->database()->select('category_id, tag_text AS tag, tag_url, COUNT(item_id) AS total')
                ->from(Phpfox::getT('tag'))
                ->where('category_id=\'ynultimatevideo_playlist\'')
                ->group('tag_text, tag_url')
                ->order('total DESC')
                ->limit($mMaxDisplay)
                ->execute('getSlaveRows'));

        }, 1);

    }

    public function query()
    {
        $view =  $this->request()->get('view');

        switch($this->request()->get('req3')){
            case 'my':
                $view = 'myplaylist';
                break;
            case 'category':
                $view = 'category';
                break;
        }

        switch($view){
            case 'myplaylist':
                $this->search()->setCondition('playlist.user_id='.intval(Phpfox::getUserId()));
                break;
            case 'category':
                //$iCategoryId =  $this->request()->getInt('req4');
                //$this->search()->setCondition('AND playlist.category_id = ' . intval($iCategoryId));
                break;
            case 'historyplaylist':
                $mParam = 'history.item_id=playlist.playlist_id AND history.item_type=\'1\' AND history.user_id='. intval(Phpfox::getUserId());
                $this->database()->join(Phpfox::getT('ynultimatevideo_history'),'history',$mParam);
                break;
        }

        if(null != ($userId =  $this->request()->get('user'))){
            $this->database()->where('playlist.user_id='.intval($userId));
        }
    }
    public function getRandomPlaylists($iLimit, $where = [])
    {
        $where [] = 'playlist.is_approved=1';
        $where = implode(' AND ', $where);

        $this->database()
            ->select('playlist.* ,' .Phpfox::getUserField())
            ->from(Phpfox::getT('ynultimatevideo_playlists'),'playlist')
            ->join(Phpfox::getT('user'),'u','u.user_id=playlist.user_id')
            ->where($where)
            ->order('RAND()')
            ->limit(0, intval($iLimit));
            
        if (Phpfox::getParam('core.friends_only_community') && Phpfox::isModule('friend')){
            $this->database()->join(Phpfox::getT('friend'), 'friends', 'friends.user_id = playlist.user_id AND friends.friend_user_id = ' . Phpfox::getUserId())
                    ->union();
            $this->database()->select('playlist.* ,' .Phpfox::getUserField())
                ->from(Phpfox::getT('ynultimatevideo_playlists'),'playlist')
                ->join(Phpfox::getT('user'),'u','u.user_id=playlist.user_id')
                ->where($where.' AND playlist.user_id ='.Phpfox::getUserId())
                ->order('RAND()')
                ->limit(0, intval($iLimit))
                ->union();
        }

        return $this->database()->execute('getSlaveRows');
    }
    public function getSomeVideoOfPlaylist($iPlaylistId)
    {
        return $this->database()->select('v.title,v.video_id')
                    ->from(Phpfox::getT('ynultimatevideo_playlist_data'),'pd')
                    ->join(Phpfox::getT('ynultimatevideo_playlists'),'p','p.playlist_id = pd.playlist_id')
                    ->join(Phpfox::getT('ynultimatevideo_videos'),'v','v.video_id = pd.video_id')
                    ->where('pd.playlist_id = '.(int)$iPlaylistId)
                    ->limit(2)
                    ->execute('getSlaveRows');
    }
}