<?php 
 
defined('PHPFOX') or exit('NO DICE!'); 

?>
<div id="ajax-response-custom">
</div>
<form method="post" action="" onsubmit="return onSubmitValid(this);">
{if $bIsEdit}
	<div><input type="hidden" name="id" value="{$aForms.category_id}" /></div>
{/if}
	<div class="table_header">
		{_p('Ultimate Videos Category Detail')}
	</div>
	<div class="table">
		<div class="table_left">
			{_p('Parent Category')}:
		</div>
		<div class="table_right">
            {$selectBox}
        </div>
		<div class="clear"></div>
	</div>
    {foreach from=$aLanguages item=aLanguage}
    <div class="table form-group">
        <div class="table_left">
            {required} {phrase var='title'}&nbsp;<strong>{$aLanguage.title}</strong>:
        </div>
        <div class="table_right">
            {assign var='value_name' value="name_"$aLanguage.language_id}
            <input type="text" name="val[name_{$aLanguage.language_id}]" value="{value id=$value_name type='input'}" size="30" />
        </div>
        <div class="clear"></div>
    </div>
    {/foreach}
	<div class="table_clear">
		<input type="submit" value="{_p('Submit')}" class="button" />
	</div>
</form>
{literal}
<script type="text/javascript">
	var MissingAddName = "{/literal}{_p('Provide a category name.')}{literal}";
	function onSubmitValid(obj){
        $.ajaxCall('ultimatevideo.AdminAddCategory',$(obj).serialize(),'post');
        return false;
    }
</script>
{/literal}