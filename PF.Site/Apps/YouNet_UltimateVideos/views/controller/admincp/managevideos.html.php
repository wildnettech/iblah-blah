<?php
/**
 * [PHPFOX_HEADER]
 * @copyright      YouNet Company
 * @author         TienNPL
 * @package        Module_Coupon
 * @version        3.01
 */
defined('PHPFOX') or exit('NO DICE!');
?>
{if !$bIsSearch}
<script type="text/javascript" src="{$corePath}/assets/jscript/manage.js"></script>
{/if}
<!-- Filter Search Form Layout -->
<form class="ynuv_video_search_form" method="post" onsubmit="return ultimatevideo.getSearchData(this);">
        <!-- Form Header -->
        <div class="table_header">
                {_p('Search Filter')}
        </div>
        <!-- Coupon Name-->
        <div class="table form-group">
                <div class="table_left">
                       {_p('Title')}:
                </div>
                <div class="table_right">
                    <input class="form-control" type="text" name="search[title]" value="{value type='input' id='title'}" id="title" size="50" />
                </div>
                <div class="clear"></div>
        </div>  

        <div class="table form-group">
                <div class="table_left">
                       {_p('Owner')}:
                </div>
                <div class="table_right">
                    <input class="form-control" type="text" name="search[owner]" value="{value type='input' id='owner'}" id="owner" size="50" />
                </div>
                <div class="clear"></div>
        </div>   

        <div class="table form-group">
                <div class="table_left">
                       {_p('Category')}:
                </div>
                <div class="table_right">
	        			{$aCategories}  
                </div>
                <div class="clear"></div>
        </div>  


        <div class="table form-group">
            <div class="table_left">
                {_p('Featured')}:
            </div>
            <div class="table_right">
                <select name="search[feature]" class="form-control">
                    <option value="0">{_p('Any')}</option>
                    <option value="featured"  {value type='select' id='feature' default = 'featured'}>{_p('Featured')}</option>
                    <option value="not_featured"  {value type='select' id='feature' default = 'not_featured'}>{_p('Un-Featured')}</option>
                </select>   
            </div>
            <div class="clear"></div>
        </div>

        <div class="table form-group">
            <div class="table_left">
                {_p('Video Source')}:
            </div>
            <div class="table_right">
                <select name="search[source]" class="form-control">
                    <option value="0">{_p('Any')}</option>
                    <option value="1"  {value type='select' id='source' default = '1'}>Youtube</option>
                    <option value="2"  {value type='select' id='source' default = '2'}>Vimeo</option>
                    <option value="3"  {value type='select' id='source' default = '3'}>{_p('Uploaded')}</option>
                    <option value="4"  {value type='select' id='source' default = '4'}>Dailymotion</option>
                    <option value="5"  {value type='select' id='source' default = '5'}>{_p('Video Url')}</option>
                    <option value="6"  {value type='select' id='source' default = '6'}>{_p('Embed')}</option>
                    <option value="7"  {value type='select' id='source' default = '7'}>{_p('Facebook')}</option>
                </select>   
            </div>
        </div>

        <div class="table form-group">
            <div class="table_left">
                {_p('Approve')}:
            </div>
            <div class="table_right">
                <select name="search[approve]" class="form-control">
                    <option value="0">{_p('Any')}</option>
                    <option value="approved"  {value type='select' id='approve' default = 'approved'}>{_p('Approve Only')}</option>
                    <option value="denied"  {value type='select' id='approve' default = 'denied'}>{_p('Deny Only')}</option>
                </select>   
            </div>
        </div>
        <div class="clear"></div>
        <!-- Submit Buttons -->
        <div class="">
            <input type="submit" id="ynuv_filter_video_submit" name="search[submit]" value="{_p('Search')}" class="button"/>
        </div>
</form>
<br/>
<div class="table_header">
     {_p('Video Listing')}
</div>
<span id="ynuv_loading" style="display: none;">{img theme='ajax/add.gif'}</span>
{if count($aList) >0}
	<form method="post" id="ynuv_video_list" action="" onsubmit="return ultimatevideo.actionMultiSelect(this);">
	<div class="table-responsive">
		<table align='center'>
	       <!-- Table rows header -->
	        <tr>
	        	<th class="table_row_header"><input type="checkbox" onclick="ultimatevideo.checkAllVideo();" id="ynuv_video_list_check_all" name="ynuv_video_list_check_all"/></th>
	            <th class="table_row_header"></th>
	            <th>{_p('Title')}</th>
	            <th class="table_row_header">{_p('Owner')}</th>
	            <th class="table_row_header">{_p('Featured')}</th>
	            <th class="table_row_header">{_p('Approve')}</th>
	            <th class="table_row_header">{_p('Category')}</th>
				<th class="table_row_header">{_p('Video Source')}</th>
				<th class="table_row_header">{_p('Status')}</th>
	            <th class="table_row_header">{_p('Date')}</th>
	            <th class="table_row_header">{_p('Views')}</th>
	            <th class="table_row_header">{_p('Likes')}</th>
	            <th class="table_row_header">{_p('Comments')}</th>
	        </tr>
		{ foreach from=$aList key=iKey item=aItem }
			<tr id="ynuv_video_row_{$aItem.video_id}" class="">
				<td style="width:20px">				
					<input type = "checkbox" class="video_row_checkbox" id="ynuv_video_{$aItem.video_id}" name="video_row[]" value="{$aItem.video_id}" onclick="ultimatevideo.checkDisableStatus();"/>
				</td>
				<td class="t_center">
					<a href="#" class="js_drop_down_link" title="Options">{img theme='misc/bullet_arrow_down.png' alt=''}</a>
					<div class="link_menu">
						<ul>
							<li>
								<a target="_blank" href="{permalink module='ultimatevideo.add' id='id_'.$aItem.video_id}">
									{_p('Edit')}
								</a>
							</li>
							<li>
								<a href="javascript:void(0)" onclick="if(confirm('{_p('Are you sure want to delete this video?')}')){l}ultimatevideo.deleteVideo({$aItem.video_id});{r}">
									{_p('Delete')}
								</a>
							</li>
						</ul>
					</div>	
				</td>
				<td style="min-width:200px">
					<a href="{permalink module='ultimatevideo' id=$aItem.video_id title=$aItem.title}" >
						{$aItem.title|shorten:50:'...'}
					</a>
				</td>
				<td align="center">
					{$aItem|user}
				</td>
				<td id="ynuv_video_update_featured_{$aItem.video_id}" align="center">
					<div class="{if $aItem.is_featured}js_item_is_active{else}js_item_is_not_active{/if}">
	                    <a class="js_item_active_link" href="javascript:void(0);"
	                       onclick="$Core.ajaxMessage();ultimatevideo.updateFeatured({$aItem.video_id}, {$aItem.is_featured});">
	                        <div style="width:50px;">
	                        {if $aItem.is_featured}
	                            {img theme='misc/bullet_green.png' alt=''}
	                        {else}
	                        	{img theme='misc/bullet_red.png' alt=''}
	                        {/if}	
	                        </div>
	                    </a>
                    </div>
                </td>	

				<td id="ynuv_video_update_approve_{$aItem.video_id}" align="center">
                    
                    <div class="{if $aItem.is_approved}js_item_is_active{else}js_item_is_not_active{/if}">
                        <a class="js_item_active_link" href="#?call=ultimatevideo.updateApprovedInAdmin&amp;iVideoId={$aItem.video_id}&amp;iIsApproved={$aItem.is_approved}"
                           >
                            <div style="width:50px;">
                            {if $aItem.is_approved}
                                {img theme='misc/bullet_green.png' alt=''}
                            {else}
                            	{img theme='misc/bullet_red.png' alt=''}
                            {/if}
                            </div>
                        </a>
                    </div>
                </td>                			
				<td align="center">
					{if $aItem.category_title}
                        {softPhrase var=$aItem.category_title}
					{else}
						{_p('None')}
					{/if}
				</td>
				<td align="center">
					{$aItem.source}
				</td>
				<td align="center">
					{if $aItem.status == 1}
						{_p('Ready')}
					{elseif $aItem.status == 0}
						{_p('Queued')}
					{else}
						{_p('Failed')}
					{/if}
				</td>
				<td align="center" style="min-width:100px">
					{$aItem.time_stamp|date:'core.global_update_time'}
				</td>
				<td align="center">
					{$aItem.total_view}
				</td>
				<td align="center">
					{$aItem.total_like}
				</td>	
				<td align="center">
					{$aItem.total_comment}
				</td>												
			</tr>
		{/foreach}
		</table>
	</div>
		{template file="ultimatevideo.block.pager"}
		<!-- Delete selected button -->
		<div class="table_bottom">
			<input type="hidden" name="val[selected]" id="ynuv_multi_select_action" value="0"/>
	        <input type="submit" name="val[delete_selected]" id="delete_selected" value="{_p('Delete Selected')}" class="delete_selected button disabled" disabled onclick="return ultimatevideo.switchAction(this,'delete');"/>
			<input type="submit" name="val[approve_selected]" id="approve_selected" value="{_p('Approve Selected')}" class="approve_selected button disabled" disabled onclick="return ultimatevideo.switchAction(this,'approve');"/>
			<input type="submit" name="val[unapprove_selected]" id="unapprove_selected" value="{_p('UnApprove Selected')}" class="unapprove_selected button disabled" disabled onclick="return ultimatevideo.switchAction(this,'unapprove');"/>
	    </div>
	</form>
{else}
{_p('No Videos Found.')}
{/if}