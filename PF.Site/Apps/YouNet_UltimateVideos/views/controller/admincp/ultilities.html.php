<?php 

defined('PHPFOX') or exit('NO DICE!'); 

?>
<div class="table_header">
	{_p('Video Utilities')}
</div>
{if !$isError}
<div class="table">
    <div class="clear" style="margin-bottom: 5px;"></div>
    <div class="table_left">
        {_p('Ffmpeg Version')}
    </div>
    <div class="table_right">
    	<label>{_p('This will display the current installed version of ffmpeg.')}</label>
        <textarea rows="10">{$sVersion}</textarea>
    </div>
    <div class="clear" style="margin-bottom: 5px;"></div>
    <div class="table_left">
    	{_p('Supported Video Formats')}
    </div>
    <div class="table_right">
    <label for="text">{_p('This will run and show the output of "ffmpeg -formats". Please see this page for more info.')}</label>
        <textarea rows="10">{$sFormat}</textarea>
    </div>
</div>
{else}
<div class="table">
	<div class="table_right">
		{_p('FFMPEG is not configured or FFMPEG path is not correct. Please try again!')}
	</div>
</div>	
{/if}