<?php
/**
 * [PHPFOX_HEADER]
 * @copyright      YouNet Company
 * @author         TienNPL
 * @package        Module_Coupon
 * @version        3.01
 */
defined('PHPFOX') or exit('NO DICE!');
?>
{if !$bIsSearch}
<script type="text/javascript" src="{$corePath}/assets/jscript/manageplaylist.js"></script>
{/if}
<!-- Filter Search Form Layout -->
<form class="ynuv_playlist_search_form" method="post" onsubmit="return ultimatevideo_playlist.getSearchData(this);">
        <!-- Form Header -->
        <div class="table_header">
                {_p('Search Filter')}
        </div>
        <!-- Coupon Name-->
        <div class="table form-group">
                <div class="table_left">
                       {_p('Title')}:
                </div>
                <div class="table_right">
                    <input class="form-control" type="text" name="search[title]" value="{value type='input' id='title'}" id="title" size="50" />
                </div>
                <div class="clear"></div>
        </div>  

        <div class="table form-group">
                <div class="table_left">
                       {_p('Owner')}:
                </div>
                <div class="table_right">
                    <input class="form-control" type="text" name="search[owner]" value="{value type='input' id='owner'}" id="owner" size="50" />
                </div>
                <div class="clear"></div>
        </div>   

        <div class="table form-group">
                <div class="table_left">
                       {_p('Category')}:
                </div>
                <div class="table_right">
	        			{$aCategories}  
                </div>
                <div class="clear"></div>
        </div>  

        <div class="table form-group">
            <div class="table_left">
                {_p('Featured')}:
            </div>
            <div class="table_right">
                <select name="search[feature]" class="form-control">
                    <option value="0">{_p('Any')}</option>
                    <option value="featured"  {value type='select' id='feature' default = 'featured'}>{_p('Featured')}</option>
                    <option value="not_featured"  {value type='select' id='feature' default = 'not_featured'}>{_p('Un-Featured')}</option>
                </select>   
            </div>
            <div class="clear"></div>
        </div>

        <div class="table form-group">
            <div class="table_left">
                {_p('Approve')}:
            </div>
            <div class="table_right">
                <select name="search[approve]" class="form-control">
                    <option value="0">{_p('Any')}</option>
                    <option value="approved"  {value type='select' id='approve' default = 'approved'}>{_p('Approve Only')}</option>
                    <option value="denied"  {value type='select' id='approve' default = 'denied'}>{_p('Deny Only')}</option>
                </select>   
            </div>
        </div>
        <div class="clear"></div>
        <!-- Submit Buttons -->
        <div class="">
            <input type="submit" id="ynuv_filter_playlist_submit" name="search[submit]" value="{_p('Search')}" class="button"/>
        </div>
</form>
<br/>
<div class="table_header">
     {_p('Playlists Listing')}
</div>
<span id="ynuv_loading" style="display: none;">{img theme='ajax/add.gif'}</span>
{if count($aList) >0}
	<form method="post" id="ynuv_playlist_list" action="" onsubmit="return ultimatevideo_playlist.actionMultiSelect(this);">
	<div class="table-responsive">
		<table align='center'>
	       <!-- Table rows header -->
	        <tr>
	        	<th class="table_row_header"><input type="checkbox" onclick="ultimatevideo_playlist.checkAllPlaylist();" id="ynuv_playlist_list_check_all" name="ynuv_playlist_list_check_all"/></th>
	            <th class="table_row_header"></th>
	            <th>{_p('Title')}</th>
	            <th class="table_row_header">{_p('Owner')}</th>
	            <th class="table_row_header">{_p('Featured')}</th>
	            <th class="table_row_header">{_p('Approve')}</th>
	            <th class="table_row_header">{_p('Category')}</th>
	            <th class="table_row_header">{_p('Date')}</th>
	            <th class="table_row_header">{_p('Views')}</th>
	            <th class="table_row_header">{_p('Likes')}</th>
	            <th class="table_row_header">{_p('Comments')}</th>
	        </tr>
		{ foreach from=$aList key=iKey item=aItem }
			<tr id="ynuv_playlist_row_{$aItem.playlist_id}" class="">
				<td style="width:20px">				
					<input type = "checkbox" class="playlist_row_checkbox" id="ynuv_playlist_{$aItem.playlist_id}" name="playlist_row[]" value="{$aItem.playlist_id}" onclick="ultimatevideo_playlist.checkDisableStatus();"/>
				</td>
				<td class="t_center">
					<a href="#" class="js_drop_down_link" title="Options">{img theme='misc/bullet_arrow_down.png' alt=''}</a>
					<div class="link_menu">
						<ul>
							<li>
								<a target="_blank" href="{permalink module='ultimatevideo.addplaylist' id='id_'.$aItem.playlist_id}">
									{_p('Edit')}
								</a>
							</li>
							<li>
								<a href="javascript:void(0)" onclick="if(confirm('{_p('Are you sure want to delete this playlist?')}')){l}ultimatevideo_playlist.deletePLaylist({$aItem.playlist_id});{r}">
									{_p('Delete')}
								</a>
							</li>
						</ul>
					</div>	
				</td>
				<td style="min-width:200px">
					<a href="{permalink module='ultimatevideo.playlist' id=$aItem.playlist_id title=$aItem.title}" >
						{$aItem.title|shorten:50:'...'}
					</a>
				</td>
				<td align="center">
					{$aItem|user}
				</td>
				<td id="ynuv_playlist_update_featured_{$aItem.playlist_id}" align="center">
					<div class="{if $aItem.is_featured}js_item_is_active{else}js_item_is_not_active{/if}">
	                    <a class="js_item_active_link" href="javascript:void(0);"
	                       onclick="ultimatevideo_playlist.updateFeatured({$aItem.playlist_id}, {$aItem.is_featured});;">
	                        <div style="width:50px;">
	                        {if $aItem.is_featured}
	                            {img theme='misc/bullet_green.png' alt=''}
	                        {else}
	                        	{img theme='misc/bullet_red.png' alt=''}
	                        {/if}	
	                        </div>
	                    </a>
                    </div>
                </td>	

				<td id="ynuv_playlist_update_approve_{$aItem.playlist_id}" align="center">
                    
                    <div class="{if $aItem.is_approved}js_item_is_active{else}js_item_is_not_active{/if}">
                        <a class="js_item_active_link" href="javascript:void(0);"
                           onclick="ultimatevideo_playlist.updateApproved({$aItem.playlist_id}, {$aItem.is_approved});">
                            <div style="width:50px;">
                            {if $aItem.is_approved}
                                {img theme='misc/bullet_green.png' alt=''}
                            {else}
                            	{img theme='misc/bullet_red.png' alt=''}
                            {/if}
                            </div>
                        </a>
                    </div>
                </td>                			
				<td align="center">
					{if $aItem.category_title}
                        {softPhrase var=$aItem.category_title}
					{else}
						{_p('None')}
					{/if}
				</td>
				<td align="center" style="min-width:100px">
					{$aItem.time_stamp|date:'core.global_update_time'}
				</td>
				<td align="center">
					{$aItem.total_view}
				</td>
				<td align="center">
					{$aItem.total_like}
				</td>	
				<td align="center">
					{$aItem.total_comment}
				</td>												
			</tr>
		{/foreach}
		</table>
	</div>
		{template file="ultimatevideo.block.pager"}
		<!-- Delete selected button -->
		<div class="table_bottom">
			<input type="hidden" name="val[selected]" id="ynuv_multi_select_action" value="0"/>
	        <input type="submit" name="val[delete_selected]" id="delete_selected" value="{_p('Delete Selected')}" class="delete_selected button disabled" disabled onclick="return ultimatevideo_playlist.switchAction(this,'delete');"/>
			<input type="submit" name="val[approve_selected]" id="approve_selected" value="{_p('Approve Selected')}" class="approve_selected button disabled" disabled onclick="return ultimatevideo_playlist.switchAction(this,'approve');"/>
			<input type="submit" name="val[unapprove_selected]" id="unapprove_selected" value="{_p('UnApprove Selected')}" class="unapprove_selected button disabled" disabled onclick="return ultimatevideo_playlist.switchAction(this,'unapprove');"/>
	    </div>
	</form>
{else}
{_p('No Playlists Found.')}
{/if}