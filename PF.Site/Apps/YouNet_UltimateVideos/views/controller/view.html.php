{if isset($sError) && !empty($sError)}
<div>{$sError}</div>
{else}
<div class="ultimatevideo_video_detail">
    <div class="ultimatevideo_video_detail-title-rating clearfix">
        <div class="ultimatevideo_video_detail-title">
            <span class="ultimatevideo-video-featured-icon ynuv_feature_video_icon_{$aItem.video_id}" {if $aItem.is_featured == 0}style="display:none"{/if} >
                <i class="ynicon yn-diamond"></i> {_p('Featured')}
            </span>
            
            {$aItem.title}
        </div>
        <div class="ultimatevideo_video_detail-ratings ultimatevideo-rating">
            {$aItem.rating|ultimatevideo_rating:$aItem.video_id}
        </div>
    </div>

    <div class="ultimatevideo_video_detail-info">
        <div class="ultimatevideo_video_detail-categories-owner clearfix">
            <div class="ultimatevideo_video_detail-owner">
                <div class="ultimatevideo_video_detail-owner-img clearfix">
                    {img user=$aItem suffix='_50_square'} 
                </div>

                <div class="ultimatevideo_video_detail-owner-info">
                    <div class="ultimatevideo_video_detail-owner-username">
                        {_p('Posted by')} {$aItem|user}
                    </div>
                    <div class="ultimatevideo_video_detail-date">
                        {$aItem.time_stamp|date}
                    </div>
                </div>
            </div>
        </div>

        <div class="ultimatevideo_video_detail-count">
            <div class="ultimatevideo_video_detail-count-items">
                <div class="ultimatevideo_video_detail-count-item">
                    <span>{$aItem.total_like}</span> {if $aItem.total_like == 1}{_p('like')}{else}{_p('likes')}{/if}
                </div>

                <div class="ultimatevideo_video_detail-count-item">
                    <span>{$aItem.total_comment}</span> {if $aItem.total_comment == 1}{_p('comment')}{else}{_p('comments')}{/if}
                </div>

                <div class="ultimatevideo_video_detail-count-item">
                    <span>{$aItem.total_view}</span> {if $aItem.total_view == 1}{_p('view')}{else}{_p('views')}{/if}
                </div>

                <div class="ultimatevideo_video_detail-count-item">
                    <span>{$aItem.total_favorite}</span> {if $aItem.total_favorite == 1}{_p('favorite')}{else}{_p('favorites')}{/if}
                </div>
                <div class="ultimatevideo_video_detail-count-item ultimatevideo_video_action">
                    {if $bShowEditMenu}
                        {template file='ultimatevideo.block.link_video_edit'}
                    {/if}
                </div>
            </div>
        </div>
    </div>

    <div>
        {$aItem.embed_code}
    </div>

    <div class="ultimatevideo_video_detail-actions-description">
        <div class="ultimatevideo_video_detail-actions clearfix">
            <div class="ultimatevideo_addthis">
                <div class="addthis_sharing_toolbox" data-url="{$aItem.bookmark_url}" data-title="{$aItem.title|clean}"></div>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid={'ynuv_addthis_pubid'|setting)}" async="async"></script>
            </div>

            <div class="ultimatevideo_video_detail-actions-right">
                {if Phpfox::getUserId() }
                {template file='ultimatevideo.block.link_video_viewer'}
                {/if}
                <div class="dropdown">
                    <a data-caption="HTML Code" title="HTML Code" class="btn btn-default btn-sm" data-toggle="ultimatevideo" data-cmd="show_embed_code" >
                        <i class="fa fa-code"></i> {_p('Embed Code')} <i class="fa fa-angle-down"></i>

                    </a>
                </div>
                {if Phpfox::isUser()}
                <div class="dropdown">
                    <a title="Invite Friends" class="btn btn-default btn-sm popup" href="{permalink module='ultimatevideo.invite' id=$aItem.video_id type=1"}">
                        <i class="fa fa-users"></i> {_p('Invite Friends')}
                    </a>
                </div>
                {/if}
            </div>
            <div class="ultimate_video_html_code_block hide">
                <textarea id="ultimatevideo_html_code_value" readony class="form-control disabled" rows="2"><iframe src="{$sUrl}" width="525" height="525" style="overflow:hidden;"></iframe></textarea>
                <div class="text-right" style="padding-top: 10px">
                    <button type="button" class="btn btn-sm btn-default" data-toggle="ultimatevideo" data-cmd="show_embed_code">
                        {_p('Close')}
                    </button>
                    <button type="button" class="btn btn-sm btn-primary" data-cmd="copy_embed_code" data-clipboard-target="#ultimatevideo_html_code_value">
                        {_p('Copy code')}
                    </button>
                </div>
            </div>
        </div>
        
        <div class="ultimatevideo_video_detail-description-block">
            {if $aItem.sTags}
            <p><i class="fa fa-tag"></i> {_p('Tags')}: {$aItem.sTags}
            </p>
            {/if}
            <div class="ultimatevideo_video_view_description ultimatevideo_video_show_less" id="ultimatevideo_video_detail_moreless">
                <div class="ultimatevideo_video_view_description-detail">
                    <div class="ultimatevideo_video_detail-descriptions item_content item_view_content">
                        {$aItem.description|parse}
                        <div class="ultimatevideo-custom-fields">
                            {module name='ultimatevideo.custom.view' video_id=$aItem.video_id}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="ultimatevideo_show_less_more" style="display: none;">
            <a href="javascript:void(0)" class="ultimatevideo_link_more">
                {_p('View more')}
            </a>
        </div>
    </div>
</div>

{if $aItem.is_approved}
    <div class="ultimatevideo_video_detail-comment">
        {module name='feed.comment'}
    </div>
{/if}
{literal}
    <script type="text/javascript">
        $Behavior.loadClipboardJs = function(){
            var eles =  $('[data-cmd="copy_embed_code"]');
            if(eles.length){
                if(typeof Clipboard == 'undefined'){
                    $Core.loadStaticFile('{/literal}{$corePath}{literal}/assets/jscript/clipboard.min.js');
                    window.setTimeout(function(){
                        new Clipboard(eles.get(0));
                    },2000);
                }else{
                    new Clipboard(eles.get(0));
                }
            }

        };
        $Behavior.checkDescription = function(){
            var desc_h = $('.ultimatevideo_video_detail-descriptions').height();

            if(desc_h > 54){
                $('.ultimatevideo_show_less_more').show();
            }

            $('.ultimatevideo_show_less_more a').bind('click', function() {
                if ($(this).hasClass('ultimatevideo_link_more')) {
                    $(this).html({/literal}'{_p('View less')}'{literal});
                    $(this).removeClass('ultimatevideo_link_more');
                    $(this).addClass('ultimatevideo_link_less');
                    $('#ultimatevideo_video_detail_moreless').removeClass('ultimatevideo_video_show_less');
                } else {
                    $(this).html('View more');
                    $(this).addClass('ultimatevideo_link_more');
                    $(this).removeClass('ultimatevideo_link_less');
                    $('#ultimatevideo_video_detail_moreless').addClass('ultimatevideo_video_show_less');
                }
            });
        }
    </script>
{/literal}
{/if}