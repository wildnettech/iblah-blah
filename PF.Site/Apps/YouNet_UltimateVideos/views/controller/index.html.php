<?php
defined('PHPFOX') or exit('NO DICE!');
?>
{assign var='bShowCommand' value='true' }
{assign var='bMultiViewMode' value='true' }
{if (isset($bIsUserProfile) && $bIsUserProfile) && $iPage <= 1}
<div class="page_section_menu ">
    <ul id="ultimatevideo_tab" class="nav nav-tabs nav-justified">
        <li class="active">
            <a href="{url link=$aUser.user_name}ultimatevideo"><span>{_p('Videos')}</span></a> 
        </li>
        <li>
            <a href="{url link=$aUser.user_name}ultimatevideo/playlist"><span>{_p('Playlists')}</span></a>  
        </li>
    </ul>
    <div class="clear"></div>
    <br/>
</div>

{/if}
{if $bIsSearch}
    {if isset($bSpecialMenu) && $bSpecialMenu == true}
    {template file='ultimatevideo.block.specialmenu'}
    {/if}
    {if !count($aItems) && $iPage <= 1}
    <div class="extra_info">
        {_p('No videos found.')}
    </div>
    {else}
    {if !PHPFOX_IS_AJAX}
    <div class="block">
    <div class="title clearfix" style="padding-bottom: 20px">
        {if isset($sView) && ($sView == 'history' || $sView == 'favorite' || $sView == 'later')}
        <button type="button" class="btn btn-danger" data-toggle="ultimatevideo" data-cmd="video_clear_all" data-view="{$sView}">
            <span class="ynicon yn-trash-alt"></span> 
            {if $sView == 'history'}
                {_p('Clear all videos history')}
            {elseif $sView == 'favorite'}
                {_p('Clear all favorite videos')}
            {elseif $sView == 'later'}
                {_p('Clear all watch later videos')}
            {/if}    
        </button>
        {/if}
        {''|ultimatevideo_video_view_mode:true}
    </div>
        <div class="content">
    <div class="ultimatevideo-grid show_grid_view clearfix">
     {/if}
        {foreach from=$aItems name=video item=aItem}
        {template file='ultimatevideo.block.entry'}
        {/foreach}
        {pager}
     {if !PHPFOX_IS_AJAX}
    </div>
        </div>
        </div>
    {/if}
    {/if}
{elseif !$bIsNoItem}
    {module name='ultimatevideo.slideshow_video'}

    {module name='ultimatevideo.recommended_video'}

    {module name='ultimatevideo.watch_it_again'}

    {module name='ultimatevideo.recent_playlist'}
{else}
    <div class="extra_info">
        {_p('No videos found.')}
    </div>
{/if}
{unset var=$bShowCommand}
{unset var=$bMultiViewMode}

{if !PHPFOX_IS_AJAX && $bShowModeration && $bIsSearch}
{moderation}
{/if}