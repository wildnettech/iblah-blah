<?php
defined('PHPFOX') or exit('NO DICE!');

?>
{if isset($sError) && !empty($sError)}
<div>{$sError}</div>
{else}
{$sCreateJs}
<div id="js_ultimatevideo_playlist_block_detail" data-addjs="{$corePath}/assets/jscript/add_playlist.js" data-validjs="{$corePath}/assets/jscript/jquery.validate.js" class="js_photo_block page_section_menu_holder">
    <form method="post" enctype="multipart/form-data" action="{url link='current'}" id="ynuv_add_playlist_form">
        <div id="js_custom_privacy_input_holder">
            {if $bIsEdit && (!isset($sModule) || empty($sModule))}
                {module name='privacy.build' privacy_item_id=$aForms.playlist_id privacy_module_id='ultimatevideo_playlist'}
            {/if}
        </div>
    {if $bIsEdit}<input type="hidden" id="ynuv_playlistid" name="val[playlist_id]" value="{$aForms.playlist_id}">{/if}
       <div class="table form-group-follow">
            <div class="table_left">
                <label>{required}{_p('Playlist Name')}:</label>
            </div>
            <div class="table_right">
            	<input type="text" name="val[title]" id="ynuv_add_playlist_title" value="{value type='input' id='title'}"/>
            </div>
            <div class="table_left">
                <label>{_p('Playlist Description')}:</label>
            </div>
            <div class="table_right">
            	{editor id='description'}
            </div>
            <div class="global_attachment">
            <div class="global_attachment_header">
                <ul class="global_attachment_list">
                    <li class="global_attachment_title">{phrase var='attachment.insert'}:</li>
                    <li>
                    	<a href="#" onclick="return $Core.shareInlineBox(this, '{$aAttachmentShare.id}', {if $aAttachmentShare.inline}true{else}false{/if}, 'attachment.add', 500, '&amp;category_id={$aAttachmentShare.type}&amp;attachment_custom=photo');" class="js_global_position_photo js_hover_title">
                    		<i class="fa fa-photo"></i>
                    		<span class="js_hover_info">{phrase var='attachment.insert_a_photo'}</span>
                    	</a>
                	</li>
                </ul>
                <div class="clear"></div>
            </div>
        	</div>
            <div class="table_left">
                <label for="category">{required}{_p('Category')}:</label>
            </div>
            <div class="table_right" id="ynuv_section_category">
                {$sCategories}
            </div> 
            <div class="table_left">
            	<label>{_p('Viewing Mode')}</label>
            </div>       	
            <div class="table_right">
            	<div class="" id="ynuv_add_playlist_mode_listing">
	            	<input type="radio" name="val[view_mode]" value="1" {if !$bIsEdit}checked{elseif $bIsEdit && $aForms.view_mode == 1}checked{/if}/>
	            	<img src="{$corePath}/assets/image/playlist_edit_preview_listing.png"/>
            	</div>
            	<div class="table_clear"></div>
            	<div class="" id="ynuv_add_playlist_mode_slideshow">
	            	<input type="radio" name="val[view_mode]" value="2" {if $bIsEdit && $aForms.view_mode == 2}checked{/if}/>
	            	<img src="{$corePath}/assets/image/playlist_edit_preview_slideshow.png"/>
            	</div>
            </div>
        </div>
        {if empty($sModule) && Phpfox::isModule('privacy')}
            <div class="table form-group-follow">
                <div class="table_left">
                    {_p('Playlist Privacy')}:
                </div>

                <div class="table_right">
                    {module name='privacy.form' privacy_name='privacy' default_privacy='anyone'}
                </div>
                <div class="extra-info help-block">
                    {_p('Control who can see this playlist')}
                </div>
            <br/>
            </div>
        {/if}
        {if !$bIsEdit}
            <input type="submit" value="{_p('Submit')}" name="val[submit]" id="ynuv_add_submit" class="button btn btn-sm btn-primary" >   
        {else}
            <input type="submit" value="{_p('Update')}" name="val[submit]" id="ynuv_add_update" class="button btn btn-sm btn-primary">  
        {/if}
        
    </form>
</div>
{if $bIsEdit}
<div id="js_ultimatevideo_playlist_block_photo" class="js_photo_block page_section_menu_holder" style="display:none">
    <form method="post" action="{url link='ultimatevideo.addplaylist' id=$aForms.playlist_id photo=true}" enctype="multipart/form-data">
        <div class="table form-group-follow">
                <label>
                    {_p('Select a image:')}
                </label>
                <div class="table_clear"></div>
                <div class="table_right">
                    <div id="js_event_upload_image">
                        <input type="file" name="imageUpload" id="imageUpload" accept="image/*" />
                        <span class="help-block">
                            {_p('You can upload a JPG, GIF or PNG file.')}
                        </span>

                        {if $iMaxFileSize !== null}
                        <span class="help-block">
                            {_p('The file size limit is ')}{$iMaxFileSize}.{_p('If your upload does not work, try uploading a smaller picture.')}
                        </span>
                        {/if}                           
                    </div>
                </div>
        </div>
            
        <div id="js_submit_upload_image" class="table_clear">
            <input type="submit" name="val[upload_photo]" value="{_p('Upload Photo')}" class="btn btn-sm btn-primary" />
        </div>
    {if !empty($aForms.image_path)}
    <h3>{_p('Current Photo')}</h3>
    <div class="js_photo_holder">
        {img server_id=$aForms.image_server_id title=$aForms.title path='core.url_pic' file=$aForms.image_path suffix='_500' max_width='500' max_height='500'}

    </div>
    {/if}
    </form>

</div>
<div id="js_ultimatevideo_playlist_block_video" class="js_photo_block page_section_menu_holder" style="display:none">
    {template file="ultimatevideo.block.edit_playlist_video"}
</div>
{/if}
{/if}
