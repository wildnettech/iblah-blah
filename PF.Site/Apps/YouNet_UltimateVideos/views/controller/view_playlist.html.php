{if isset($sError) && !empty($sError)}
<div>{$sError}</div>
{else}
<div class="ultimatevideo_video_detail ultimatevideo_playlist_detail">
    <div class="ultimatevideo_video_detail-title-rating clearfix">
        <div class="ultimatevideo_video_detail-title">
            <span class="ultimatevideo-video-featured-icon ynuv_feature_playlist_icon_{$aPitem.playlist_id}" {if $aPitem.is_featured == 0}style="display:none"{/if}>
                    <i class="ynicon yn-diamond"></i> {_p('Featured')}
            </span>
            {$aPitem.title}
        </div>
    </div>

    <div class="ultimatevideo_video_detail-info">
        <div class="ultimatevideo_video_detail-categories-owner clearfix">
            <div class="ultimatevideo_video_detail-owner">
                <div class="ultimatevideo_video_detail-owner-img clearfix">
                    {img user=$aPitem suffix='_50_square'} 
                </div>

                <div class="ultimatevideo_video_detail-owner-info">
                    <div class="ultimatevideo_video_detail-owner-username">
                        {_p('Posted by')} {$aPitem|user}
                    </div>
                    <div class="ultimatevideo_video_detail-date">
                        {$aPitem.time_stamp|date}
                    </div>
                </div>
            </div>
        </div>

        <div class="ultimatevideo_video_detail-count">
            <div class="ultimatevideo_video_detail-count-items">
                <div class="ultimatevideo_video_detail-count-item">
                    <span>{$aPitem.total_like}</span> {if $aPitem.total_like == 1}{_p('like')}{else}{_p('likes')}{/if}
                </div>

                <div class="ultimatevideo_video_detail-count-item">
                    <span>{$aPitem.total_comment}</span> {if $aPitem.total_comment == 1}{_p('comment')}{else}{_p('comments')}{/if}
                </div>

                <div class="ultimatevideo_video_detail-count-item">
                    <span>{$aPitem.total_view}</span> {if $aPitem.total_view == 1}{_p('view')}{else}{_p('views')}{/if}
                </div>
                <div class="ultimatevideo_video_detail-count-item ultimatevideo_video_action">
                    {if $bShowEditMenu}
                        {template file='ultimatevideo.block.link_playlist'}
                    {/if}

                </div>
            </div>
        </div>
    </div>



    {if $aPitem.view_mode == 1}
        <div class="ultimatevide_playlist_mode_listing clearfix" id="ultimatevide_playlist_mode_listing">
            {module name="ultimatevideo.playlist_detail_mode_listing"}
        </div>
    {else}
        <div class="ultimatevide_playlist_mode_slide" id="ultimatevide_playlist_mode_slide">
            {module name="ultimatevideo.playlist_detail_mode_slide"}
        </div>
    {/if}

    <div class="ultimatevideo_video_detail-actions-description">
        <div class="ultimatevideo_video_detail-actions clearfix">
            <div class="ultimatevideo_addthis">
                <div class="addthis_sharing_toolbox" data-url="{$aPitem.bookmark_url}" data-title="{$aPitem.title|clean}"></div>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid={'ynuv_addthis_pubid'|setting)}" async="async"></script>
            </div>

            <div class="ultimatevideo_video_detail-actions-right">
                {if Phpfox::isUser()}
                <div class="dropdown">
                    <a title="Invite Friends" class="btn btn-default btn-sm popup" href="{permalink module='ultimatevideo.invite' id=$aPitem.playlist_id type=2"}">
                        <i class="fa fa-users"></i> {_p('Invite Friends')}
                    </a>
                </div>
                {/if}
            </div>
        </div>
        
        {if {$aPitem.description}
        <div class="ultimatevideo_video_detail-description-block">
            <div class="ultimatevideo_video_view_description ultimatevideo_video_show_less" id="ultimatevideo_video_detail_moreless">
                <div class="ultimatevideo_video_view_description-detail">
                    <div class="ultimatevideo_video_detail-descriptions item_content item_view_content">
                        {$aPitem.description|parse}
                    </div>
                </div>
            </div>
        </div>
        {/if}
        <div class="ultimatevideo_show_less_more" style="display: none;">
            <a href="javascript:void(0)" class="ultimatevideo_link_more">
                {_p('View more')}
            </a>
        </div>
    </div>
</div>
{if $aPitem.is_approved}
    <div class="ultimatevideo_playlist_detail-comment">
        {module name='feed.comment'}
    </div>
{/if}
{literal}
    <script type="text/javascript">
        $Behavior.checkDescription = function(){
            var desc_h = $('.ultimatevideo_video_detail-descriptions').height();

            if(desc_h > 54){
                $('.ultimatevideo_show_less_more').show();
            }

            $('.ultimatevideo_show_less_more a').bind('click', function() {
                if ($(this).hasClass('ultimatevideo_link_more')) {
                    $(this).html({/literal}'{_p('View less')}'{literal});
                    $(this).removeClass('ultimatevideo_link_more');
                    $(this).addClass('ultimatevideo_link_less');
                    $('#ultimatevideo_video_detail_moreless').removeClass('ultimatevideo_video_show_less');
                } else {
                    $(this).html('View more');
                    $(this).addClass('ultimatevideo_link_more');
                    $(this).removeClass('ultimatevideo_link_less');
                    $('#ultimatevideo_video_detail_moreless').addClass('ultimatevideo_video_show_less');
                }
            });
        }
    </script>
{/literal}
{/if}