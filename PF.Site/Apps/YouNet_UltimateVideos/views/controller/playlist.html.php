{assign var='bShowCommand' value='true' }
{assign var='bMultiViewMode' value='true' }
{if (isset($bIsUserProfile) && $bIsUserProfile) && $iPage <= 1}
<div class="page_section_menu ">
    <ul id="ultimatevideo_tab" class="nav nav-tabs nav-justified">
        <li >
            <a href="{url link=$aUser.user_name}ultimatevideo"><span>{_p('Videos')}</span></a> 
        </li>
        <li class="active">
            <a href="{url link=$aUser.user_name}ultimatevideo/playlist"><span>{_p('Playlists')}</span></a>  
        </li>
    </ul>
    <div class="clear"></div>
    <br/>
</div>

{/if}
{if $bIsSearch}
    {if isset($bSpecialMenu) && $bSpecialMenu == true}
    {template file='ultimatevideo.block.specialmenu'}
    {/if}
    {if !count($aItems) && $iPage <= 1}
    <div class="extra_info">
        {_p('No playlists found.')}
    </div>
    {else}
    {if !PHPFOX_IS_AJAX}
    <div class="block">
    <div class="title clearfix" style="padding-bottom: 20px">
        {if isset($sView) && $sView == 'historyplaylist'}
        <button type="button" class="btn btn-danger btn-sm" data-toggle="ultimatevideo" data-cmd="playlist_clear_all" data-view="{$sView}">
            <span class="ynicon yn-trash-alt"></span> {_p('Clear all playlists history')}
        </button>
        {/if}
        {''|ultimatevideo_playlist_view_mode:true}
    </div>
    <div class="content">
    <div class="ultimatevideo_playlists_grid show_grid_view ultimatevideo-grid clearfix">
        {/if}
        {foreach from=$aItems name=video item=aPitem}
        {template file='ultimatevideo.block.entry_playlist'}
        {/foreach}
        {pager}
        {if !PHPFOX_IS_AJAX}
    </div>
</div>
</div>
    {/if}
    {/if}
{elseif !$bIsNoItem}
    {module name='ultimatevideo.slideshow_playlist'}

    {module name='ultimatevideo.recommended_playlist'}
{else}
        <div class="extra_info">
        {_p('No playlists found.')}
    </div>
{/if}
{unset var=$bShowCommand}
{unset var=$bMultiViewMode}

{if !PHPFOX_IS_AJAX && ($bShowModeration) && $bIsSearch}
{moderation}
{/if}