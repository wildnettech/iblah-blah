<?php
?>
<script src="http://code.jquery.com/jquery-3.1.1.js"></script>
{$aItem.embed_code}
{literal}
<script type="text/javascript">
    $(document).ready(function() {
        var videoFrameClass = [
            '.youtube_iframe_big',
            '.youtube_iframe_small',
            '.vimeo_iframe_small',
            '.vimeo_iframe_big',
            '.facebook_iframe',
            '.dailymotions_iframe_small',
            '.dailymotions_iframe_big'
        ], videoAspec = 16 / 9;
        (function (eles) {
            eles.each(function (index, ele) {
                var $ele = $(ele),
                    parent = $ele.parent();

                $ele.data('built', true);
                $ele.css("width", parent.width());
                $ele.css("height", parent.width() / videoAspec);
            });
        })($(videoFrameClass.join(', ')).not('.built'));
    });
</script>
{/literal}
