<?php
defined('PHPFOX') or exit('NO DICE!');

?>

{if isset($sError) && !empty($sError)}
<div>{$sError}</div>
{elseif !isset($bIsSpam) || !$bIsSpam}
{$sCreateJs}
<div id="js_ultimatevideo_block_detail" data-addjs="{$corePath}/assets/jscript/add.js" data-validjs="{$corePath}/assets/jscript/jquery.validate.js" class="js_photo_block page_section_menu_holder">
    <form method="post" enctype="multipart/form-data" action="{url link='current'}" id="ynuv_add_video_form">
        <div id="js_custom_privacy_input_holder">
            {if $bIsEdit && (!isset($sModule) || empty($sModule))}
                {module name='privacy.build' privacy_item_id=$aForms.video_id privacy_module_id='ultimatevideo'}
            {/if}
        </div>
        {if $bIsEdit}<input type="hidden" id="ynuv_videoid" name="val[video_id]" value="{$aForms.video_id}">{/if}
        <div class="table form-group-follow">
            <div class="table_left">
                <label for="category">{required}{_p('Category')}:</label>
            </div>
            <div class="table_right" id="ynuv_section_category">
                {$sCategories}
            </div>
            {if $bIsEdit && $aForms.type == 3 && setting('ynuv_allow_user_upload_video_to_yt')}
            <input type="checkbox" name="val[allow_upload_channel]" id='allow_upload_channel'} {if $aForms.allow_upload_channel} checked {/if} /> {_p('Also upload this video to youtube')}
            {/if}
            {if !$bIsEdit}
            <div class="table_left" >
                <label for="category">{required}{_p('Video Source')}:</label>
            </div>
            <div class="table_right">
                <select name="val[video_source]" id="ynav_js_source_video" class="form-control">
                    <option value="">{_p('Select')}:</option>
                    <option value="Youtube">YouTube Video</option>
                    <option value="Vimeo">Vimeo Video</option>
                    <option value="Uploaded">Uploaded Video</option>
                    <option value="Dailymotion">Dailymotion Video</option>
                    <option value="VideoURL">URL Video</option>
                    <option value="Embed">Embed Video</option>
                    <option value="Facebook">Facebook Video</option>
                </select>
            </div>
            <div id="ynav_js_input_add_video">
                <input type="hidden" name="val[video_code]" id="ynuv_add_video_code" value="{value type='hidden' id='video_code'}"/>
                <div id="ynav_add_video_embed" style="display:none">
                    <div class="table_left">
                        <label>{required}{_p('Embed Code')}</label>
                    </div>
                    <div class="table_right">
                        <input type="text" class="form-control" name="val[video_embed]" value="{value type='input' id='video_embed'}" id="ynuv_add_video_input_embed"/>
                    </div>
                    <span class="help-block">{_p('Paste embed codes (iframe) of the video here. Embeded video will not get the original duration and image. Please make sure to select another thumbnail image for your video, otherwise it will display the default one.')}</span>
                </div>
                <div id="ynav_add_video_link" style="display:none">
                    <div class="table_left">
                        <label>{required}{_p('Video link \(URL\)')}</label>
                    </div>
                    <div class="table_right">
                        <input type="text" name="val[video_link]" value="{value type='input' id='video_link'}" id="ynuv_add_video_input_link"/>
                        <span class="help-block" id="ynuv_help_block_link" style="display:none">{_p('Paste the web address of the video here.')}</span>
                        <span class="help-block" id="ynuv_help_block_url" style="display:none">{_p('Paste the web address of the video here. Only support MP4 video when uploading video via URL.')}</span>
                    </div>
                </div>
                <div id="ynav_add_video_upload" style="display:none">
                    <div class="table_clear"></div>
                    <div class="table_right form-group">
                        <input type="file" id="videoUpload" name="videoUpload" onchange="ultimatevideo.fileSelected();"/>
                        <br/>
                        <div class="ynuv_file_info" id="ynuv_file_info">
                            <div id="ynuv_fileName"></div>
                            <div id="ynuv_fileSize"></div>
                        </div>
                    </div>
                    <div class="extra_info">
                        {if setting('ynuv_allow_user_upload_video_to_yt')}
                        <input type="checkbox" name="val[allow_upload_channel]"/> {_p('Also upload this video to YouTube')}
                        {/if}
                    </div>
                    <span class="help-block">{_p('Please wait while your video is being uploaded. When your upload finish, your video will be processed - you will be notified when it is ready to be viewed.')}</span>
                    <!--progress bar-->
                    <div id="ynuv_progress" style="display:none;">
                        <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
                    </div>
                    <span style="display:none;" id="ynuv_progressNumber" class="progress-text">0%</span>
                </div>
            </div>
            <div class="ynuv_processing message" id="ynuv_add_processing" style="display:none">{_p('Checking URL...')}</div>
            <div class="ynuv_error error_message" id="ynuv_add_error_link" style="display:none"></div>
            <div class="ynuv_processing message" id="ynuv_add_processing_embed" style="display:none">{_p('Checking Embed Code...')}</div>
            <div class="ynuv_error error_message" id="ynuv_add_error_embed" style="display:none">{_p('We could not find a video there - please check the Embed Code and try again.')}</div>
            {/if}
            <div class="table_left">
                <label>{required}{_p('Video Title')}:</label>
            </div>
            <div class="table_right">
                <input type="text" name="val[title]" id="ynuv_add_video_title" value="{value type='input' id='title'}"/>
            </div>
            <div class="table_left">
                <label>{_p('Tags')}:</label>
            </div>
            <div class="table_right">
                <input type="text" name="val[tag_list]" value="{value type='input' id='tag_list'}"/>
                <span class="help-block">{_p('Separate tags with commas.')}</span>
            </div>
            <div class="table_left">
                <label>{_p('Video Description')}:</label>
            </div>
            <div class="table_right">
                {editor id='description'}
            </div>
        </div>
        <div class="form-group">
            <div id="ynuv_customfield_category">
            </div>
        </div>
        {if empty($sModule) && Phpfox::isModule('privacy')}
        <div class="table form-group-follow">
            <div class="table_left">
                {_p('Video Privacy')}:
            </div>
            <div class="table_right">
                {module name='privacy.form' privacy_name='privacy' default_privacy='anyone'}
            </div>
            <div class="extra-info help-block">
                {_p('Control who can see this video')}
            </div>
            <br/>
        </div>
        {/if}
        {if !$bIsEdit}
        <input type="submit" value="{_p('Submit')}" name="val[submit]" id="ynuv_add_submit" class="button btn btn-sm btn-primary" disabled="disabled">
        {else}
        <input type="submit" value="{_p('Update')}" name="val[submit]" id="ynuv_add_update" class="button btn btn-sm btn-primary">
        {/if}

    </form>
</div>
{if $bIsEdit}
<div id="js_ultimatevideo_block_photo" class="js_photo_block page_section_menu_holder" style="display:none">
    <form method="post" action="{url link='ultimatevideo.add' id=$aForms.video_id photo=true}" enctype="multipart/form-data">
        <div class="table form-group-follow">
            <label>
                {_p('Select a image:')}
            </label>
            <div class="table_clear"></div>
            <div class="table_right">
                <div id="js_event_upload_image">
                    <input type="file" name="imageUpload" id="imageUpload" accept="image/*" />
                    <span class="help-block">
                            {_p('You can upload a JPG, GIF or PNG file.')}
                        </span>

                    {if $iMaxFileSize !== null}
                    <span class="help-block">
                            {_p('The file size limit is ')}{$iMaxFileSize}.{_p('If your upload does not work, try uploading a smaller picture.')}
                        </span>
                    {/if}
                </div>
            </div>
        </div>

        <div id="js_submit_upload_image" class="table_clear">
            <input type="submit" name="val[upload_photo]" value="{_p('Upload Photo')}" class="btn btn-sm btn-primary" />
        </div>

    </form>
    {if !empty($aForms.image_path)}
    <h3>{_p('Current Photo')}</h3>
    <div class="js_photo_holder">
        {img server_id=$aForms.image_server_id title=$aForms.title path='core.url_pic' file=$aForms.image_path suffix='_500' max_width='500' max_height='500'}
    </div>
    {/if}
</div>
{/if}
{/if}
