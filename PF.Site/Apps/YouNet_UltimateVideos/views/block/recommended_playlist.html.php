{if !empty($aItems)}
<div class="ultimatevideo_playlists_grid show_grid_view ultimatevideo-grid clearfix">
    {foreach from=$aItems name=video item=aPitem}
    {template file='ultimatevideo.block.entry_playlist'}
    {/foreach}
</div>
{else}
<div class="extra_info">
    {_p('No playlists found.')}
</div>
{/if}