{if count($aItems)}
{foreach from=$aItems name=playlist item=aItem}
<div class="checkbox">
    <label data-toggle="ultimatevideo" data-cmd="add_to_playlist" data-playlist="{$aItem.playlist_id}" data-id="{$iVideoId}">
        <input type="checkbox" {if $aItem.added_video}checked{/if} />
        {$aItem.title} {if isset($aItem.privacy) && $aItem.privacy == 3}&nbsp;<i class="fa fa-lock" aria-hidden="true"></i>
{/if}
    </label>
</div>
{/foreach}
{else}
	<span style="padding: 10px 15px; display:block">{_p('No playlists.')}</span>
{/if}