{if user('ynuv_can_feature_playlist') || ($aPitem.is_approved == 0 && user('ynuv_can_approve_playlist')) || (Phpfox::getUserId () == $aPitem.user_id && ( user('ynuv_can_edit_own_playlists') || user('ynuv_can_delete_own_playlists'))) || (Phpfox::getUserId () != $aPitem.user_id && ( user('ynuv_can_edit_playlist_of_other_user') || user('ynuv_can_delete_playlist_of_other_user'))) || (isset($sView) && ($sView == 'historyplaylist'))
}
<div class="dropdown clearfix {if isset($bIsDetailViewPlaylist) && $bIsDetailViewPlaylist}item_bar_action_holder{/if}">
    <a role="button" data-toggle="dropdown" class="{if isset($bIsDetailViewPlaylist) && $bIsDetailViewPlaylist}item_bar_action{else}btn btn-default btn-sm{/if}">
        {if !isset($bIsDetailViewPlaylist)}<i class="fa fa-edit"></i>{/if}
    </a>
    <ul class="dropdown-menu dropdown-menu-right">
        {if $aPitem.is_approved == 0 && user('ynuv_can_approve_playlist')}
        <li role="presentation">
            <a href="" data-toggle="ultimatevideo" data-cmd="approve_playlist" data-id="{$aPitem.playlist_id}">
                <i class="fa fa-eye"></i>
                {_p('Approve')}
            </a>
        </li>
        {/if}
        {if user('ynuv_can_feature_playlist')}
            {if $aPitem.is_featured == 0}
            <li role="presentation">
                <a href="" data-toggle="ultimatevideo" data-cmd="featured_playlist" data-id="{$aPitem.playlist_id}" class="ynuv_feature_playlist_{$aPitem.playlist_id}">
                    <i class="fa fa-diamond"></i>
                    {_p('Featured')}</a>
            </li>
            {else}
            <li role="presentation">
                <a href="" data-toggle="ultimatevideo" data-cmd="unfeatured_playlist" data-id="{$aPitem.playlist_id}" class="ynuv_feature_playlist_{$aPitem.playlist_id}">
                    <i class="fa fa-diamond"></i>
                    {_p('Un-Featured')}</a>
            </li>
            {/if}
        {/if}
        {if (Phpfox::getUserId() != $aPitem.user_id && user('ynuv_can_edit_playlist_of_other_user')) || (Phpfox::getUserId() == $aPitem.user_id && user('ynuv_can_edit_own_playlists')) }
        <li role="presentation">
            <a href="{url link='ultimatevideo.addplaylist' id=$aPitem.playlist_id}">
                <i class="fa fa-pencil"></i>
                {_p('Edit Playlist')}</a></a>
        </li>
        {/if}
        {if (Phpfox::getUserId() != $aPitem.user_id && user('ynuv_can_delete_playlist_of_other_user')) || (Phpfox::getUserId() == $aPitem.user_id && user('ynuv_can_delete_own_playlists'))}
        <li role="presentation" class="item_delete">
            <a href="" data-toggle="ultimatevideo" class="btn-danger" data-cmd="delete_playlist" data-id="{$aPitem.playlist_id}" data-confirm="{_p('Are you sure want to delete this playlist?')}" {if isset($bIsDetailViewPlaylist) && $bIsDetailViewPlaylist}data-detail="true"{else}data-detail="false"{/if}>
                <i class="fa fa-trash"></i>
                {_p('Delete Playlist')}</a>
        </li>
        {/if}
        {if isset($sView) && ($sView == 'historyplaylist')}
        <li role="presentation" class="">
            <a href="" class="no_ajax_link" data-toggle="ultimatevideo" data-cmd="delete_playlist_history" data-id="{$aPitem.playlist_id}">
                <i class="fa fa-times"></i>
                {_p('Remove from History')}</a>
        </li>
        {/if}
    </ul>
</div>
{/if}