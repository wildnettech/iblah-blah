{if isset($aParentFeed) && $aParentFeed.type_id == 'ultimatevideo_playlist'}
<div class="ultimatevideo-feed-video">
	<a class="ultimatevideo_playlists_grid-bg" href="{$aParentFeed.feed_link}"> 
        {if !empty($aParentFeed.image_path)}
        	{img server_id=$aParentFeed.image_server_id path='core.url_pic' file=$aParentFeed.image_path suffix='_500'}
        {else}
        	<img src="{param var='core.path_actual'}PF.Site/Apps/YouNet_UltimateVideos/assets/image/noimg_playlist.jpg"/>
        {/if}         	
    </a>
    <a class="ultimatevideo-feed-title" href="{$aParentFeed.feed_link}">{$aParentFeed.feed_title}</a>
    <div class="ultimatevideo-feed-description item_view_content">
    {$aParentFeed.feed_content|feed_strip|shorten:200:'...'|split:55}
    </div>
</div>
{else}
<div class="ultimatevideo-feed-playlists">
	<a class="ultimatevideo_playlists_grid-bg" href="{$aFeed.feed_link}">
        {if !empty($aFeed.image_path)}
        	{img server_id=$aFeed.image_server_id path='core.url_pic' file=$aFeed.image_path suffix='_500'}
        {else}
        	<img src="{param var='core.path_actual'}PF.Site/Apps/YouNet_UltimateVideos/assets/image/noimg_playlist.jpg"/>
        {/if}         	
    </a>
    <a class="ultimatevideo-feed-title" href="{$aFeed.feed_link}">{$aFeed.feed_title}</a>
    <div class="ultimatevideo-feed-description item_view_content">
    {$aFeed.feed_content|feed_strip|shorten:200:'...'|split:55}
    </div>
</div>
{/if}
{unset var=$aParentFeed}