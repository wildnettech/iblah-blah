
{if user('ynuv_can_upload_video', '1') == '0'}
<div class="error_message">
	{_p('Your membership group does not have access to share a video.')}
</div>
{else}
<div class="uv_process_form hide">
	<span></span>
	<div class="pf_process_bar"></div>
	<div class="extra_info">{_p('pf_video_uploading_message')}</div>
</div>
<div class="uv_video_message" style="display:none;">
	<div class="valid_message">{_p('Your video has successfully been uploaded. We are processing it and you will be notified when its ready.')}</div>
</div>


<div class="ynuv_upload_form">
	<form method="post" action="" class="ajax_post form-group" data-add-spin="true">
        <div class="pf_v_title feed-attach-form-label">
            {_p('Select a video to upload')}
        </div>
        <div class="table_right form-group feed-attach-form-file">
            <input type="file" class="select-video" id="ultvideoUpload" name="ultvideoUpload"/>
        </div>
        <span class="extra_info hide_it">
            <a href="#" class="pf_v_upload_cancel button btn-sm">{_p('Cancel')}</a>
        </span>
		{if !empty($sModule)}
			<div>
				<input type="hidden" name="val[callback_module]" value="">
				<input type="hidden" name="val[callback_item_id]" value="">
			</div>
		{/if}
        <div class="pf_v_title feed-attach-form-label">
            {_p('Or insert a video link')}
        </div>
        <br/>
        <div class="table_right form-group">
            <input type="text" name="val[video_link]" class="form-control" value="{value type='input' id='video_link'}" id="ynuv_add_video_input_link"/>
            <input type="hidden" name="val[video_code]" id="ynuv_add_video_code" value=""/>
            <input type="hidden" name="val[video_source]" id="ynuv_add_video_source" value=""/>
        </div>
		<div class="pf_v_video_url">
			<span class="extra_info hide_it">
				<a href="#" class="pf_v_url_cancel">{_p('Cancel')}</a>
			</span>
		</div>
</div>
{/if}