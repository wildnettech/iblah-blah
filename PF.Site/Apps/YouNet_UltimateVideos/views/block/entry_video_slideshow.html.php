<div class="ultimatevideo-video-entry item">
    <a class="ultimatevideo-btnplay" href="{permalink module='ultimatevideo' id=$aItem.video_id title=$aItem.title}"><i class="ynicon yn-play"></i></a>
    <div class="ultimatevideo-video-image-wrap">
        <a class="ultimatevideo-video-image"
           alt="{$aItem.title}"
           href="{permalink module='ultimatevideo' id=$aItem.video_id title=$aItem.title}"
            {if $aItem.image_path}
                style="background-image: url({img server_id=$aItem.image_server_id path='core.url_pic' file=$aItem.image_path suffix='_500' return_url=true})">
           {else}
                style="background-image: url({param var='core.path_actual'}PF.Site/Apps/YouNet_UltimateVideos/assets/image/noimg_video.jpg)">
            {/if}
        </a>
    </div>
    <div class="ultimatevideo-video-info">
        <div class="uv-box-left">
            <a class="ultimatevideo-video-title" href="{permalink module='ultimatevideo' id=$aItem.video_id title=$aItem.title}">{$aItem.title|clean}</a>
            <div class="ultimatevideo-video-owner">
                <span>by {$aItem|user}</span>
            </div>
        </div>
        
        <div class="uv-box-right">
            <div class="ultimatevideo-rating">
                {$aItem.rating|ultimatevideo_rating}
            </div>
            <div class="ultimatevideo-video-stats ultimatevideo-separators">
                {if isset($bShowTotalView) && $bShowTotalView}
                <span>
                    {$aItem.total_view} {if $aItem.total_view == 1}{_p('view')}{else}{_p('views')}{/if}
                </span>
                {/if}
                {if isset($bShowTotalLike) && $bShowTotalLike}
                <span>
                    {$aItem.total_like} {if $aItem.total_like == 1}{_p('like')}{else}{_p('likes')}{/if}
                </span>
                {/if}
                {if isset($bShowTotalComment) && $bShowTotalComment}
                <span>
                {$aItem.total_comment} {if $aItem.total_comment == 1}{_p('comment')}{else}{_p('comments')}{/if}
                </span>
                {/if}
            </div>
        </div>
    </div>
</div>