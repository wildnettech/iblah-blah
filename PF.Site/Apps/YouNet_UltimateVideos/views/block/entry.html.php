<div data-vid="{$aItem.video_id}" class="ultimatevideo-video-entry {if !Phpfox::getUserId()}ultimatevideo-no-bgopacity{/if} {if isset($bMultiViewMode) && $bMultiViewMode}{$phpfox.iteration.video|ultimatevideo_mode_view_video_format}{/if}" id="js_ynuv_video_item_holder_{$aItem.video_id}">
    <div class="ultimatevideo-video-image-wrap">
        <a href="{permalink module='ultimatevideo' id=$aItem.video_id title=$aItem.title}" class="ultimatevideo-video-bgopacity"></a>
        <a class="ultimatevideo-video-image"
           alt="{$aItem.title}"
           href="{permalink module='ultimatevideo' id=$aItem.video_id title=$aItem.title}"
           {if $aItem.image_path}
           style="background-image: url({img server_id=$aItem.image_server_id path='core.url_pic' file=$aItem.image_path suffix='_500' return_url=true})">
           {else}
            style="background-image: url({param var='core.path_actual'}PF.Site/Apps/YouNet_UltimateVideos/assets/image/noimg_video.jpg)">
            {/if}
            </a>

        <a href="{permalink module='ultimatevideo' id=$aItem.video_id title=$aItem.title}" class="ultimatevideo-video-gradient"></a>
        <div class="ultimatevideo-bar-01">
            <span class="ultimatevideo-duration">
                {$aItem.duration|ultimatevideo_duration}
            </span>

            <span class="ultimatevideo-video-featured-icon ynuv_feature_video_icon_{$aItem.video_id}" {if $aItem.is_featured == 0}style="display:none"{/if} >
                <i class="ynicon yn-diamond" title="{_p('Featured')}"></i>
            </span>
            
            {if $aItem.is_approved != 1}
            <span class="ultimatevideo-video-pending">{_p('Pending')}</span>
            {else}
            <span class="ultimatevideo-rating">
                {$aItem.rating|ultimatevideo_rating}
            </span>

            <div class="ultimatevideo-video-stats" style="display: none;">
                {if isset($bShowTotalView) && $bShowTotalView}
                <span class="ultimatevideo-stats-views">
                   <b>{$aItem.total_view}</b> {if $aItem.total_view == 1}{_p('view')}{else}{_p('views')}{/if}
                </span>
                {/if}
                {if isset($bShowTotalLike) && $bShowTotalLike}
                <span class="ultimatevideo-stats-likes">
                    <b>{$aItem.total_like}</b> {if $aItem.total_like == 1}{_p('like')}{else}{_p('likes')}{/if}
                </span>
                {/if}
                {if isset($bShowTotalComment) && $bShowTotalComment}
                <span class="ultimatevideo-stats-comments">
                    <b>{$aItem.total_comment}</b> {if $aItem.total_comment == 1}{_p('comment')}{else}{_p('comments')}{/if}
                </span>
                {/if}
            </div>
            {/if}
            
        </div>
    </div>
    {if $aItem.is_featured == 1}
    <span class="ultimatevideo-video-featured-icon" style="display: none;">
        <i class="ynicon yn-diamond" title="{_p('Featured')}"></i>
    </span>
    {/if}

    <div class="ultimatevideo-info">
<!--         <a href="{permalink module='ultimatevideo' id=$aItem.video_id title=$aItem.title}" class="ultimatevideo-btn-play" style="display:none"><i class="fa fa-play"></i></a> -->
        <a class="ultimatevideo-video-title" href="{permalink module='ultimatevideo' id=$aItem.video_id title=$aItem.title}" title="{$aItem.title|clean}">{$aItem.title|clean}</a>
        <div class="ultimatevideo-video-owner ultimatevideo-separators">
            {if $aItem.duration}
            <span class="ultimatevideo-duration">
                {$aItem.duration|ultimatevideo_duration}
            </span>
            {/if}
            <span class="ultimatevideo-author">{_p('by')} {$aItem|user}</span>
            <span class="ultimatevideo-time-stamp">{$aItem.time_stamp|date}</span>
        </div>
        <div class="ultimatevideo-video-stats ultimatevideo-separators">
            {if isset($bShowTotalView) && $bShowTotalView}
            <span>
                {$aItem.total_view} {if $aItem.total_view == 1}{_p('view')}{else}{_p('views')}{/if}
            </span>
            {/if}
            {if isset($bShowTotalLike) && $bShowTotalLike}
            <span>
                {$aItem.total_like} {if $aItem.total_like == 1}{_p('like')}{else}{_p('likes')}{/if}
            </span>
            {/if}
            {if isset($bShowTotalComment) && $bShowTotalComment}
            <span>
            {$aItem.total_comment} {if $aItem.total_comment == 1}{_p('comment')}{else}{_p('comments')}{/if}
            </span>
            {/if}
        </div>
    </div>

    {if isset($bShowCommand) && $bShowCommand}
    <div class="ultimatevideo-cmds">

        {template file='ultimatevideo.block.link'}
    </div>
    {/if}

    {if (isset($bShowCommand) && $bShowCommand && $bIsSearch && $bShowModeration) || (isset($bIsPagesView)  && $bIsPagesView)}
    <div class="_moderator">
        <a href="#{$aItem.video_id}" class="moderate_link built" data-id="mod" rel="ultimatevideo_video"><i class="fa"></i></a>
    </div>
    {/if}

</div>