<div id="ajax-response-custom">
</div>
<div class="table_header">
{if isset($bIsEditGroup) && $bIsEditGroup }
	{_p('Edit Custom Field Groups')}
{else}
	{_p('Add New Custom Field Groups')}
{/if}
</div>

	<form onsubmit="return onSubmitValid(this);" action="" method="post" id="js_add_group_name_form" name="js_add_group_name_form" >

<div class="table">
	{if isset($bIsEditGroup) && $bIsEditGroup }
	<input type="hidden" name="val[group_id]" value="{$aGroup.group_id}"/>
	{/if}
	<div class="table_left">
		{required}{_p('Group Name')}
	</div>
	<div class="table_right">
		{foreach from=$aLanguages item=aLanguage}
			<input type="text" id="ynuv_group_name_edit" name="val[group_name]{if isset($aLanguage.phrase_var_name)}[{$aLanguage.phrase_var_name}]{/if}[{$aLanguage.language_id}]{if isset($sMode)}[{$sMode}]{/if}" value="{$aLanguage.post_value|htmlspecialchars}" />
			<div class="extra_info">{$aLanguage.title}</div>
		{/foreach}
	</div>
	<div class="clear"></div>
</div>

{if isset($bIsEditGroup) && $bIsEditGroup }
	<div class="table">
		<div class="table_left">
			{_p('Mapping Categories')}
		</div>
		<div class="table_right">
			<div id="mapping_categories">
				{foreach from=$aCategories key=iKeyCate item=aCategory}
				<div>
					{if (($iKeyCate	) % 3 == 0 ) }
					<div>
					{/if}
						<input type="checkbox" name="val[categories][]" value="{$aCategory.category_id}"
					   		{if isset($bIsEditGroup) && $bIsEditGroup}
							   {if in_array($aCategory.category_id,$aGroup.categories)}
							   checked="checked"
							   {/if}
							{/if}
						> {softPhrase var=$aCategory.title}
					{if (($iKeyCate + 1) % 3 == 0 || ($iKeyCate + 1) == $totalCategory) }
					</div>
					{/if}
				</div>
				{/foreach}
			</div>
		</div>
	</div>
	{if $aGroup.customfield}
		<div class="table_header">
			{_p('Custom Fields')}
		</div>
		<table>
			<tr>
				<th>{_p('Custom Field Name')}</th>
				<th align="center">{_p('Option')}</th>
			</tr>
			{foreach from=$aGroup.customfield key=iKey item=iField}
				<tr>
					<input type="hidden" name="val[customfield][]" value="{$iField.field_id}">
					<td>{phrase var=$iField.phrase_var_name}</td>
					<td align="center">
						<a href="#" onclick="editCustomField({$iField.field_id});">{_p('Edit')}</a>
						/
						<a href="#" onclick="deleteCustomField({$iField.field_id},{$aGroup.group_id});">{_p('Delete')}</a>
					</td>
				</tr>
			{/foreach}
		</table>
	{/if}
	<div class="js_mp_parent_holder" id="js_mp_holder">
		{if isset($bIsEditGroup) && $bIsEditGroup}
			<a href="#" onclick="tb_show('{_p('Add Custom Field')}', $.ajaxBox('ultimatevideo.AdminAddCustomFieldBackEnd', 'height=300&width=300&action=add&iGroupId={$aGroup.group_id}')); return false;">{_p('Add Custom Field')}</a>
		{/if}
	</div>
{/if}
	<div class="table_clear">
		<input type="submit" value="{_p('Submit')}" class="button" />
	</div>
</form>
{literal}
<script type="text/javascript">
function deleteCustomField(iCustomFieldId,iGroupId){
	if (confirm('Are you sure?')){
		$.ajaxCall('ultimatevideo.AdminDeleteCustomField','iFieldId='+ iCustomFieldId + '&iGroupId='+ iGroupId,'post');
	}
}

function editCustomField(iCustomFieldId){
	tb_show('Edit Custom Field', $.ajaxBox('ultimatevideo.AdminAddCustomFieldBackEnd', 'height=300&amp;width=300&action=edit&id='+iCustomFieldId+'&iGroupId='+{/literal}{$aGroup.group_id}{literal}));
}

var MissingAddName = "{/literal}{_p('Group name cannot be empty')}{literal}";
function onSubmitValid(obj){
	var error = false;
	$(".ynuv_add_group_name").each(function(){
		if($(this).val() == ""){
			error = true;
		}
	})
    if(error == true)
    {
        $('#ajax-response-custom').html('<div class="error_message">' + MissingAddName +' </div>');
        return false;
    }
    else {
    	$.ajaxCall('ultimatevideo.AdminAddCustomFieldGroup',$(obj).serialize(),'post');
    	return false;
    }
    return true;
}

</script>
{/literal}