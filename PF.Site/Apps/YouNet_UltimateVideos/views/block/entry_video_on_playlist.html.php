<div class="ultimatevideo_playlist_detail_item ms-slide">
    <img src="{$corePath}/assets/jscript/masterslider/blank.gif" data-src="{if $aItem.image_path}{img server_id=$aItem.image_server_id path='core.url_pic' file=$aItem.image_path suffix='_500' return_url=true}{else}{$corePath}/assets/image/noimg_video.jpg{/if}" alt="lorem ipsum dolor sit"/>
    {$aItem.embed_code}
    <div class="ultimatevideo_playlist_detail_infomation ms-thumb" onclick="setTimeout(function(){l}ynultimatevideoPlay();{r},1000)">
        <span class="ultimatevideo-video-featured-icon ynuv_feature_video_icon_{$aItem.video_id}" {if $aItem.is_featured == 0}style="display:none"{/if} >
            <i class="ynicon yn-diamond"></i>
        </span>
        <div class="ultimatevideo_video_thumb clearfix">
            <img src="{if $aItem.image_path}{img server_id=$aItem.image_server_id path='core.url_pic' file=$aItem.image_path suffix='_500' return_url=true}{else}{$corePath}/assets/image/noimg_video.jpg{/if}" alt="">
        </div>

        <div class="ultimatevideo_playlist_detail_infomation_detail ultimatevideo-video-entry">
            <div class="ultimatevideo-video-title">
               	{$aItem.title|clean}
            </div>
            
            <div class="ultimatevideo-video-owner-duration">
                <div class="ultimatevideo-video-owner">
                     {$aItem|user}
                </div>
                
                <div class="ultimatevideo-duration">
                	{$aItem.duration|ultimatevideo_duration}
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" class="video_id" value="{$aItem.video_id}"/>
    <input type="hidden" class="title" value="{$aItem.title}"/>
    <input type="hidden" class="href" value="{permalink module='ultimatevideo' id=$aItem.video_id title=$aItem.title}"/>

</div>