<div class="dropdown clearfix {if isset($bIsDetailView) && $bIsDetailView}item_bar_action_holder{/if}">
    <a role="button" data-toggle="dropdown" class="{if isset($bIsDetailView) && $bIsDetailView}item_bar_action{else}btn btn-default btn-sm{/if}">
        {if !isset($bIsDetailView)}<i class="fa fa-edit"></i>{/if}
    </a>
    <ul class="dropdown-menu dropdown-menu-right">
        {if $aItem.is_approved == 0 && user('ynuv_can_approve_video')}
        <li role="presentation">
            <a href="" data-toggle="ultimatevideo" data-cmd="approve_video" data-id="{$aItem.video_id}">
                <i class="fa fa-eye"></i>
                {_p('Approve')}
            </a>
        </li>
        {/if}
        {if user('ynuv_can_feature_video')}
        {if $aItem.is_featured == 0 && isset($bIsPagesView) && !$bIsPagesView }
        <li role="presentation">
            <a href="" data-toggle="ultimatevideo" data-cmd="featured_video" data-id="{$aItem.video_id}" class="ynuv_feature_video_{$aItem.video_id}">
                <i class="fa fa-diamond"></i>
                {_p('Featured')}</a>
        </li>
        {elseif $aItem.is_featured == 1 && isset($bIsPagesView) && !$bIsPagesView}
        <li role="presentation">
            <a href="" data-toggle="ultimatevideo" data-cmd="unfeatured_video" data-id="{$aItem.video_id}" class="ynuv_feature_video_{$aItem.video_id}">
                <i class="fa fa-diamond"></i>
                {_p('Un-Featured')}</a>
        </li>
        {/if}
        {/if}
        {if user('ynuv_can_edit_video_of_other_user') || (Phpfox::getUserId() == $aItem.user_id && user('ynuv_can_edit_own_video'))}
        <li role="presentation">
            <a href="{url link='ultimatevideo.add' id=$aItem.video_id}">
                <i class="fa fa-pencil"></i>
                {_p('Edit video')}</a>
        </li>
        {/if}
        {if user('ynuv_can_delete_video_of_other_user') || (Phpfox::getUserId() == $aItem.user_id && user('ynuv_can_delete_own_video') )}
        <li role="presentation" class="item_delete">
            <a href="" href="" class="no_ajax_link" data-toggle="ultimatevideo" data-cmd="delete_video" data-id="{$aItem.video_id}" data-confirm="{_p('Are you sure want to delete this video?')}" {if isset($bIsDetailView) && $bIsDetailView}data-detail="true"{else}data-detail="false"{/if}>
                <i class="fa fa-trash"></i>
                {_p('Delete video')}</a>
        </li>
        {/if}
        {if isset($sView) && ($sView == 'history')}
        <li role="presentation" class="item_delete">
            <a href="" class="no_ajax_link" data-toggle="ultimatevideo" data-cmd="delete_video_history" data-id="{$aItem.video_id}" >
                <i class="fa fa-times"></i>
                {_p('Remove from History')}</a>
        </li>
        {/if}
    </ul>
</div>