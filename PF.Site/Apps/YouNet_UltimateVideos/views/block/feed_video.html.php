{if isset($aParentFeed) && $aParentFeed.type_id == 'ultimatevideo_video'}
<div class="ultimatevideo-feed-video">
    {if isset($aParentFeed.embed_code)}{$aParentFeed.embed_code}{/if}

    <a href="{$aParentFeed.feed_link}" class="ultimatevideo-feed-title">{$aParentFeed.title}</a>
    <div class="ultimatevideo-feed-description item_view_content">
    	{$aParentFeed.feed_content|feed_strip|shorten:200:'...'|split:55}
    </div>
</div>
{else}
<div class="ultimatevideo-feed-video">
    {if isset($aFeed.embed_code)}{$aFeed.embed_code}{/if}
    <a href="{$aFeed.feed_link}" class="ultimatevideo-feed-title">{$aFeed.feed_title}</a>
    <div class="ultimatevideo-feed-description item_view_content">
    	{$aFeed.feed_content|feed_strip|shorten:200:'...'|split:55}
    </div>
</div>
{/if}
{unset var=$aParentFeed}