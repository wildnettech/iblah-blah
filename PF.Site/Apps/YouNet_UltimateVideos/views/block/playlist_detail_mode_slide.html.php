<!-- Base MasterSlider style sheet -->
<link href="{$corePath}/assets/jscript/masterslider/style/masterslider.css" rel='stylesheet' type='text/css'>
<!-- Master Slider Skin -->
<link href="{$corePath}/assets/jscript/masterslider/skins/default/style.css" rel='stylesheet' type='text/css'>
<!-- MasterSlider Template Style -->
<link href='{$corePath}/assets/jscript/masterslider/style/ms-videogallery.css' rel='stylesheet' type='text/css'>

<link href='{$corePath}/assets/jscript/mediaelementplayer/mediaelementplayer.css' rel='stylesheet' type='text/css'>

{if !empty($aItems)}
<div id="ultimatevideo-playlist-detail-slide" class="ms-videogallery-template  ms-videogallery-vertical-template">
    <div class="master-slider ms-skin-default" id="ultimatevideo_playlist_slideshow_masterslider"  data-js="{$corePath}/assets/jscript/masterslider.min.js,{$corePath}/assets/jscript/mediaelementplayer/mediaelement-and-player.min.js" data-jquery="{$corePath}/assets/jscript/masterslider/jquery.easing.min.js">
        <div class="ultimatevideo_playbutton-block">
            <a class="ultimatevideo_playbutton ultimatevideo_btn_playlist_pre" href="javascript:void(0)" onclick="ynultimatevideoPrev();"><i class="ynicon yn-arr-left"></i></a>
            <a class="ultimatevideo_playbutton ultimatevideo_btn_playlist_play" href="javascript:void(0)" onclick="ynultimatevideoPlay();"><i class="ynicon yn-play"></i></a>
            <a class="ultimatevideo_playbutton ultimatevideo_btn_playlist_next" href="javascript:void(0)" onclick="ynultimatevideoNext();"><i class="ynicon yn-arr-right"></i></a>
        </div>

        <div class="ultimatevideo_playlist_detail_actions-block">
            <div class="ultimatevideo_playlist_detail_actions">
                <a id="ultimatevideo_repeat_button" data-status="repeat" href="javascript:void(0);" title="{_p('Repeat')}" onclick="ynultimatevideoSwitch(this);" class="ultimatevideo_status_button">
                    <i class="fa fa-repeat"></i>
                </a>
                <a id="ultimatevideo_shuffle_button" data-status="shuffle" href="javascript:void(0);" title="{_p('Shuffle')}" onclick="ynultimatevideoSwitch(this);" class="ultimatevideo_status_button">
                    <i class="fa fa-random"></i>
                </a>
                <span>
                    <label for="ultimatevideo_continue_button">
                        <input type="checkbox" data-status="auto_play" name="auto_play" id="ultimatevideo_continue_button" value="{_p('Autoplay Next')}" onclick="ynultimatevideoSwitch(this);" class="ultimatevideo_status_button"/> 
                        <span>{_p('Autoplay Next')}</span>
                    </label>
                </span>
            </div>
            <div class="ultimatevideo_current_title_playing" id="ynuv_current_video_info-title" style="display:none">
                <span>
                    <span>{_p('Playing')} </span>
                    <span id="ynuv_current_video_title"></span>
                </span>

                <div class="ultimatevideo_videos_count">
                    <i class="fa fa-film" aria-hidden="true"></i>
                    {$aPitem.total_video} {if $aPitem.total_video == 1} {_p('Video')} {else} {_p('Videos')} {/if}
                </div>
            </div>

            <div class="ultimatevideo_playlist_detail_actions-toggle">
                <i class="ynicon yn-arr-up"></i>
            </div>
        </div>

        {foreach from=$aItems name=video item=aItem}
        	{template file="ultimatevideo.block.entry_video_on_playlist"}
        {/foreach}
    </div>
</div>	
{else}
{_p('No videos found.')}
{/if}
<script type="text/javascript" src="{$corePath}/assets/jscript/froogaloop2.min.js"></script>
<script src="https://api.dmcdn.net/all.js"></script>
<script type="text/javascript" src="{$corePath}/assets/jscript/videoPlayer.js"></script>
{literal}
<script type="text/javascript">
	$Behavior.onLoadSlideShowPlaylistDetail = function(){
	    window.addEventListener('domready', function() {
	        var playerCookie = getCookie('ultimatevideo_player_status');
	        if (playerCookie) {
	            var playerStatus = JSON.parse(playerCookie);
	            $(".ultimatevideo_status_button").each(function(){
	                if (playerStatus[$(this).data('status')]) {
	                    $(this).addClass('active');
	                }
	            });
	        }
	    });
        if(typeof window.MSLayerEffects == 'undefined') {
            var script1 = document.createElement('script');
            script1.src = '{/literal}{$corePath}/assets/jscript/masterslider.min.js{literal}';
            document.getElementsByTagName("head")[0].appendChild(script1);

            var script2 = document.createElement('script');
            script2.src = '{/literal}{$corePath}/assets/jscript/mediaelementplayer/mediaelement-and-player.min.js{literal}';
            document.getElementsByTagName("head")[0].appendChild(script2);
        }
	}
    function ynultimatevideoSwitch(ele) {
        if ($(ele).hasClass('active')) {
            $(ele).removeClass('active');
        } else {
            $(ele).addClass('active');
        }
        var status = getPlayingStatus();
        setCookie('ultimatevideo_player_status', JSON.stringify(status));
    }

    function getPlayingStatus() {
        var status = {
            auto_play: 0,
            repeat: 0,
            shuffle: 0
        };
        $(".ultimatevideo_status_button").each(function(){
            status[$(this).data('status')] = $(this).hasClass('active') ? 1 : 0;
        });
        return status;
    }

    function generateRandom(current, min, max) {
        // playlist contain only 1 video
        if (max == 1) {
          return 0;
        }
        var gen = Math.floor(Math.random() * (max - min + 1)) + min;
        while (gen == current) {
            gen = Math.floor(Math.random() * (max - min + 1)) + min;
        }
        return gen;
    }
</script>
{/literal}