<?php 
/**
 * [PHPFOX_HEADER]
 */
 
defined('PHPFOX') or exit('NO DICE!'); 

?>
{if count($aMenus)}
<div class="page_section_menu page_section_menu_header">
    <ul class="action">
        {foreach from=$aMenus key=sPageSectionKey item=sPageSectionMenu}
            <li {if $sPageSectionKey == 'detail'}class="active"{/if}>
                <a href="#" rel="js_ultimatevideo_block_{$sPageSectionKey}">{$sPageSectionMenu}</a>
            </li>
        {/foreach}
        <li>
            <a href="{$sLink}">{$sView}</a>
        </li>
    </ul>
</div>
{/if}
<script type="text/javascript">
    var bIsFirstRunVideo = false;
     $Behavior.pageSectionMenuEditVideo = function() {l}
     
        if(!bIsFirstRunVideo)
        {l}
            {if $isEditPhoto}
                $Core.pageSectionMenuShow('#js_ultimatevideo_block_photo');
            {/if}
            bIsFirstRunVideo = true;
        {r}
    {r}
</script>