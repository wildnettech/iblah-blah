
<div class="ultimatevideo-actions dropdown clearfix">
    <a role="button" data-toggle="dropdown" class="btn {if isset($bIsDetailView) && $bIsDetailView}btn-primary{else}btn-default{/if} btn-sm ynuv_action_view_video" data-id="{$aItem.video_id}" data-imgpath="{$corePath}/assets/image/loading.gif" onclick="getPlaylistToQuickAddVideo(this);">
        <i class="fa fa-plus"></i> {if isset($bIsDetailView) && $bIsDetailView}{_p('Add to')} <i class="fa fa-angle-down"></i>{/if}
    </a>
    <ul class="dropdown-menu dropdown-menu-right">
        {if isset($aItem.watchlater)}
        <li role="presentation">
            {if !$aItem.watchlater }
            <a href="" data-toggle="ultimatevideo" data-cmd="watchlater_video" data-id="{$aItem.video_id}" class="ynuv_watchlater_video_{$aItem.video_id}">
                <i class="fa fa-clock-o"></i>
                {_p('Watch Later')}
            </a>
            {else}
            <a href="" data-toggle="ultimatevideo" data-cmd="unwatchlater_video" data-id="{$aItem.video_id}" class="ynuv_watchlater_video_{$aItem.video_id}">
                <i class="fa fa-clock-o"></i>
                {_p('Un-Watch Later')}
            </a>
            {/if}
        </li>
        {/if}
        <li role="presentation">
            {if ultimatevideo_favourite($aItem.video_id) }
            <a href="" data-toggle="ultimatevideo" data-cmd="unfavorite_video" data-id="{$aItem.video_id}" class="ynuv_favorite_video_{$aItem.video_id}">
                <i class="fa fa-star"></i>
                {_p('Un-Favorite')}</a>
            {else}
            <a href="" data-toggle="ultimatevideo" data-cmd="favorite_video" data-id="{$aItem.video_id}" class="ynuv_favorite_video_{$aItem.video_id}">
                <i class="fa fa-star-o"></i>
                {_p('Favorite')}</a>
            {/if}
        </li>
        <li class="dropdown-header">
            {_p('Add to Playlist')}
        </li>
        <li class="ynuv_error_add_to_playlist_{$aItem.video_id}" hidden></li>
        <li class="ynuv_noti_add_to_playlist_{$aItem.video_id}" hidden></li>
        <li class="ynuv_quick_list_playlist">
            <div class="text-center"><img src="{$corePath}/assets/image/loading.gif"/></div>
        </li>
        <li class="ynuv_quick_list_playlist-title">
            <a data-id="{$aItem.video_id}" data-toggle="ultimatevideo" data-cmd="showFormAddPlaylist">
                {_p('Add to new playlist')}
                <i class="pull-right fa-lg fa fa-angle-down" aria-hidden="true"></i>
                <i class="pull-right fa-lg fa fa-angle-up" aria-hidden="true" hidden></i>
            </a>
        </li>
        <li class="ynuv_quick_list_playlist-title-form">
            <div class="ultimatevideo-quick-add-form" style="display:none">
                <span class="ultimatevideo-error" style="display:none">{_p('Please input the playlist title!')}</span>
                <input type="text" name="title" class="ynuv_quick_add_playlist_input"/>
                <div class="ultimatevideo-quick-add-form-btn">
                    <button class="btn btn-primary btn-sm" data-toggle="ultimatevideo" data-cmd="add_new_playlist" data-id="{$aItem.video_id}">{_p('Create')}</button>
                    <button class="btn btn-default btn-sm" data-toggle="ultimatevideo" data-cmd="close_add_form">{_p('Cancel')}</button>
                </div>
            </div>
        </li>
    </ul>
</div>
