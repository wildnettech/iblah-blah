{if user('ynuv_can_feature_playlist') || ($aPitem.is_approved == 0 && user('ynuv_can_approve_playlist')) || (Phpfox::getUserId () == $aPitem.user_id && ( user('ynuv_can_edit_own_playlists') || user('ynuv_can_delete_own_playlists'))) || (Phpfox::getUserId () != $aPitem.user_id && ( user('ynuv_can_edit_playlist_of_other_user') || user('ynuv_can_delete_playlist_of_other_user'))) || (isset($sView) && ($sView == 'historyplaylist'))
}
<div class="ultimatevideo_playlists_grid_playlist-item">
{else}
<div class="ultimatevideo_playlists_grid_playlist-item ultimatevideo-no-bgopacity">
{/if}
    <div class="ultimatevideo_playlists_grid-item">
        {if isset($bShowCommand) && $bShowCommand && !isset($bIsPlaylistDetail)}
        <div class="ultimatevideo-cmds">
            {template file='ultimatevideo.block.link_playlist'}
        </div>
        {/if}
        <div class="ultimatevideo_playlists_grid-content">
            <div class="ultimatevideo_playlists_grid-bg" 
                {if $aPitem.image_path}
                style="background-image: url({img server_id=$aPitem.image_server_id path='core.url_pic' file=$aPitem.image_path suffix='_500' return_url=true})
                {else}
                style="background-image: url({param var='core.path_actual'}PF.Site/Apps/YouNet_UltimateVideos/assets/image/noimg_playlist.jpg)
                {/if}">

                <a href="{permalink module='ultimatevideo.playlist' id=$aPitem.playlist_id title=$aPitem.title}">
                    <div class="ultimatevideo_playlists_grid-count">
                       <i class="fa fa-film" aria-hidden="true"></i>
                        <span>{$aPitem.total_video}</span>
                        <span class="ultimatevideo-video-featured-icon ynuv_feature_playlist_icon_{$aPitem.playlist_id}" {if $aPitem.is_featured == 0}style="display:none"{/if}>
                            <i class="ynicon yn-diamond" title="{_p('Featured')}"></i>
                        </span>
                    </div>
                </a>

                {if $aPitem.is_approved != 1}
                <span class="ultimatevideo-video-pending">{_p('Pending')}</span>
                {/if}
            </div>

            <div class="ultimatevideo_playlists_grid-info">
                <div class="ultimatevideo_playlists_grid-title">
                    <a href="{permalink module='ultimatevideo.playlist' id=$aPitem.playlist_id title=$aPitem.title}" title="{$aPitem.title|clean}">
                        {$aPitem.title|clean}
                    </a>
                </div>
                <div class="ultimatevideo_playlists_grid-owner">
                        <span>{_p('by')}</span>
                        {$aPitem|user}
                    <div>
                        {if isset($bShowTotalView) && $bShowTotalView}
                        <span>
                            {$aPitem.total_view} {if $aPitem.total_view == 1}{_p('view')}{else}{_p('views')}{/if}
                            <b>&nbsp;.&nbsp;</b>
                        </span>
                        {/if}
                        {if isset($bShowTotalLike) && $bShowTotalLike}
                        <span>
                            {$aPitem.total_like} {if $aPitem.total_like == 1}{_p('like')}{else}{_p('likes')}{/if}
                            <b>&nbsp;.&nbsp;</b>
                        </span>
                        {/if}
                        {if isset($bShowTotalComment) && $bShowTotalComment}
                        <span>
                        {$aPitem.total_comment} {if $aPitem.total_comment == 1}{_p('comment')}{else}{_p('comments')}{/if}
                        <b>&nbsp;.&nbsp;</b>
                        </span>
                        {/if}
                    </div>
                </div>
            </div>
            {if isset($aPitem.video_list) && count($aPitem.video_list)}
            <ul class="ultimatevideo_playlists_grid-playlist-videos">
                {foreach from=$aPitem.video_list name=video item=aVideo}
                <li>
                    <i class="fa fa-angle-right"></i>&nbsp;
                    <a class="ultimatevideo_playlist_list-videos-title" href="{permalink module='ultimatevideo' id=$aVideo.video_id title=$aVideo.title}">{$aVideo.title}</a>
                </li>
                {/foreach}
            </ul>
            {/if}
        </div>
    </div>

    {if isset($bShowCommand) && $bShowCommand && $bIsSearch && $bShowModeration && !isset($bIsPlaylistDetail)}
        <div class="_moderator">
            <a href="#{$aPitem.playlist_id}" class="moderate_link built" data-id="mod" rel="ultimatevideo_playlist"><i class="fa"></i></a>
        </div>
    {/if}

    <!-- Listview -->
    <div class="ultimatevideo_playlist_list-item">
        <div class="ultimatevideo_playlist_list-content ultimatevideo_playlists_grid-content">
            <div class="ultimatevideo_playlist_list-bg" 
                {if $aPitem.image_path}
                style="background-image: url({img server_id=$aPitem.image_server_id path='core.url_pic' file=$aPitem.image_path suffix='_500' return_url=true})
                {else}
                style="background-image: url({param var='core.path_actual'}PF.Site/Apps/YouNet_UltimateVideos/assets/image/noimg_playlist.jpg)
                {/if}">
                <a href="{permalink module='ultimatevideo.playlist' id=$aPitem.playlist_id title=$aPitem.title}"></a>
                <div class="ultimatevideo_playlist_list-count">
                    <div class="ultimatevideo_playlists_list-count">
                       <i class="fa fa-film" aria-hidden="true"></i>
                        <span>{$aPitem.total_video}</span>
                        
                        <span class="ultimatevideo-video-featured-icon ynuv_feature_playlist_icon_{$aPitem.playlist_id}" {if $aPitem.is_featured == 0}style="display:none"{/if}>
                            <i class="ynicon yn-diamond" title="{_p('Featured')}"></i>
                        </span>
                        
                    </div>
                    {if $aPitem.is_approved != 1}
                    <span class="ultimatevideo-video-pending">{phrase var='ultimatevideo.pending'}</span>
                    {/if}
                </div>
                {if isset($bShowCommand) && $bShowCommand && !isset($bIsPlaylistDetail)}
                <div class="ultimatevideo-cmds">
                    {template file='ultimatevideo.block.link_playlist'}
                </div>
                {/if}
            </div>



            <div class="ultimatevideo_playlist_list-info">
                
                <div class="ultimatevideo_playlist_list-title">
                    <a href="{permalink module='ultimatevideo.playlist' id=$aPitem.playlist_id title=$aPitem.title}" title="{$aPitem.title|clean}">
                        {$aPitem.title|clean}
                    </a>
                </div>


                <div class="ultimatevideo_playlist_list-owner">
                   <span>{_p('by')}</span>
                    {$aPitem|user}
                </div>

                <div class="ultimatevideo_playlist_list-statis">
                    {if isset($bShowTotalView) && $bShowTotalView}
                    <span>
                        {$aPitem.total_view} {if $aPitem.total_view == 1}{_p('view')}{else}{_p('views')}{/if}
                    </span>
                    {/if}
                    {if isset($bShowTotalLike) && $bShowTotalLike}
                    <span>
                        {$aPitem.total_like} {if $aPitem.total_like == 1}{_p('like')}{else}{_p('likes')}{/if}
                    </span>
                    {/if}
                    {if isset($bShowTotalComment) && $bShowTotalComment}
                    <span>
                    {$aPitem.total_comment} {if $aPitem.total_comment == 1}{_p('comment')}{else}{_p('comments')}{/if}
                    </span>
                    {/if}
                </div>

                <div class="ultimatevideo_playlist_list-videos">
                    {if isset($aPitem.video_list) && count($aPitem.video_list)}
                    <ul class="ultimatevideo_playlist_list-videos-item">
                        {foreach from=$aPitem.video_list name=video item=aVideo}
                        <li>
                            <i class="fa fa-angle-right"></i>&nbsp;
                            <a class="ultimatevideo_playlist_list-videos-title" href="{permalink module='ultimatevideo' id=$aVideo.video_id title=$aVideo.title}">{$aVideo.title}</a>
                        </li>
                        {/foreach}
                    </ul>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>