<?php
/**
 * Created by PhpStorm.
 * User: minhhai
 * Date: 8/24/16
 * Time: 09:11
 */
defined('PHPFOX') or exit('NO DICE!');

?>
{if count($aVideos)}
<form method="post" action="{url link='ultimatevideo.addplaylist' id=$aForms.playlist_id video=true}" onsubmit="">
    <div class="table">
        <div class="sortable">
            <ul class="ultimatevideo-dragdrop ui-sortable">
                {foreach from=$aVideos name=video item=aItem}
                    <li class="ui-sortable-handle">
                        <input type="hidden" name="order[{$aItem.video_id}]" value="{$aItem.ordering}" class="js_mp_order"/>
                        <a href="#" class="js_drop_down"><i class="fa fa-arrows" aria-hidden="true"></i> &nbsp; {$aItem.title}</a>
                        <a class="ultimatevideo-dragdrop-remove" data-toggle="ultimatevideo" data-cmd="remove_video_from_playlist" data-video="{$aItem.video_id}"><i class="fa fa-times"></i></a>
                    </li>
                {/foreach}
            </ul>
        </div>
    </div>
    <input type="hidden" name="removed" id="ynuv_removed_video" value="">
    <div class="table_clear">
        <input type="submit" value="{_p('Update')}" class="btn btn-primary" />
    </div>
</form>
{else}
   {_p('No videos found.')}
{/if}
