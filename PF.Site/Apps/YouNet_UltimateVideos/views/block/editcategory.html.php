<?php 
 
defined('PHPFOX') or exit('NO DICE!'); 

?>
<div id="ajax-response-custom">
</div>
<form method="post" action="" onsubmit="return onSubmitValid(this);">
{if $bIsEdit}
	<div><input type="hidden" id="ynuv_category_id" name="val[category_id]" value="{$aForms.category_id}" /></div>
	<div><input type="hidden" id="ynuv_parent_category_id" name="parent_id" value="{$aForms.parent_id}" /></div>
    <div><input type="hidden" name="val[name]" value="{$aForms.title}" /></div>
{/if}
	<div class="table_header">
		{_p('Ultimate Videos Category Detail')}
	</div>
	<div class="table">
		<div class="table_left">
			{_p('Parent Category')}:
		</div>
		{if empty($selectBox)}
		<div class="table_right">
			<select name="val[parent_id]" style="width:300px;">
				<option value="">{_p('Select')}:</option>
				{$sOptions}
			</select>
		</div>
		{else}
			<div class="table_right">
           		{$selectBox}
        	</div>
		{/if}
		<div class="clear"></div>
	</div>
    {foreach from=$aLanguages item=aLanguage}
    <div class="table form-group">
        <div class="table_left">
            {required} {phrase var='title'}&nbsp;<strong>{$aLanguage.title}</strong>:
        </div>
        <div class="table_right">
            {assign var='value_name' value="name_"$aLanguage.language_id}
            <input type="text" name="val[name_{$aLanguage.language_id}]" value="{value id=$value_name type='input'}" size="30" />
        </div>
        <div class="clear"></div>
    </div>
    {/foreach}
	<div class="table_clear">
		<input type="submit" value="{_p('Submit')}" class="button" />
	</div>
</form>
{literal}
<script type="text/javascript">
	if($('#ynuv_parent_category_id').length != 0){
		var parentId = $('#ynuv_parent_category_id').val();
		var cateId = $('#ynuv_category_id').val();
		$('#js_mp_category_item_' + parentId).attr('selected',true);
		$("#js_mp_category_item_" + cateId).remove();
	}
	var MissingAddName = "{/literal}{_p('Provide a category name.')}{literal}";
	function onSubmitValid(obj){
        $.ajaxCall('ultimatevideo.AdminAddCategory',$(obj).serialize(),'post');
        return false;
    }
</script>
{/literal}