<?php

use Phpfox;
use Phpfox_Plugin;
use Phpfox_Service;
use Phpfox_Database;
use Phpfox_Error;
use Phpfox_Url;

include "cli.php";
@ini_set('display_startup_errors',1);
@ini_set('display_errors',1);
@ini_set('error_reporting',-1);


$array = Phpfox_Database::instance()->select('`to`,`from`')
            ->from('cometchat')
            ->where('`read` = 0 AND `notification` = 0')
            ->execute('getSlaveRows');



for ($e = 0; $e < count($array); $e++)
{
  $duplicate = null;
  for ($ee = $e+1; $ee < count($array); $ee++)
  {
    if (strcmp($array[$ee]['to'],$array[$e]['to']) === 0 && strcmp($array[$ee]['from'],$array[$e]['from']) === 0)
    {
      $duplicate = $ee;
      array_splice($array,$duplicate,1);
      //break;
    }
  }
    
}

foreach($array as $key => $value){
   
  $totalRows = Phpfox_Database::instance()->select('*')
            ->from('cometchat')
            ->where('`read` = 0 AND `notification` = 0 AND `to` = "'.$value['to'].'" AND `from` = "'.$value['from'].'" ')
            ->execute('getSlaveRows');
            
  $count = count($totalRows);
  $upsql = "UPDATE `cometchat` SET `notification`=1 WHERE `to`=".$value['to']." and `from` =".$value['from']."";
  Phpfox::getLib('database')->query($upsql);
   
    $fromUserId = intval($value['from']);
    $toUserId = intval($value['to']);
    
    if($count == 1){
        $total_message = "a";
    }else{
        $total_message = $count;
    }
    
    $msg = "send ". $total_message ." message to you";
    
    $fromUser = Phpfox::getService('user')->get($fromUserId);
    if (!$fromUser) 
    {
        return;
    }
    
    $content = strip_tags(sprintf('%s %s', $fromUser['full_name'], $msg));
    
    $params = array(
        'ios' => array(
            'aps' => array(
                'alert' => $content,
                'badge' => 0,
                'sound' => 'default'
            ) ,
            'iId' => $fromUserId,
            'sType' => 'chat'
        ) ,
        'android' => array(
            'message' => $content,
            'iId' => $fromUserId,
            'sType' => 'chat'
        )
    );
    
    $response = Phpfox::getService('mfox.cloudmessage')->send($params, $toUserId);
    
}




echo "\nRun Cron successfully";