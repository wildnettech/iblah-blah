<?php
/**
 * Created by PhpStorm.
 * User: davidnguyen
 * Date: 7/26/16
 * Time: 10:36 AM
 */
namespace Apps\YouNet_UltimateVideos\Ajax;


use Phpfox;
use Phpfox_Ajax;
use Phpfox_Url;
/**
 * Class Ajax
 *
 * @package Apps\YouNet_UltimateVideos\Ajax
 */
class Ajax extends Phpfox_Ajax
{
	public function editcategory()
    {
		$iCategoryId = (int) $this->get('id');
		Phpfox::getBlock('ultimatevideo.editcategory', array('iCategoryId' => $iCategoryId));

		$this->html('#app-custom-holder', $this->getContent(false));
	}
	public function editcustomfieldgroup()
    {
		$iCustomFieldGroupId = (int) $this->get('id');
		
		Phpfox::getBlock('ultimatevideo.editcustomfield', array('iCustomFieldGroupId' => $iCustomFieldGroupId));

		$this->html('#app-custom-holder', $this->getContent(false));
	}
	public function AdminAddCustomFieldBackEnd()
    {
        $iGroupId = $this->get('iGroupId');
        
        Phpfox::getComponent('ultimatevideo.admincp.customfield.addfield', array('iGroupId' => $iGroupId), 'controller');
    }
    public function addField()
    {
        $aVals = $this->get('val');

        list($iFieldId, $aOptions) = Phpfox::getService('ultimatevideo.custom.process')->add($aVals);
        if(!empty($iFieldId))
        {
            $this->call("$('#js_add_field_loading').hide();");
            $this->call("$('#js_add_field_button').attr('disabled', false);");
            $this->editcustomfieldgroup();
            $this->call("js_box_remove($('.js_box_close'));");
        }
        else{
            $this->call("$('#js_add_field_loading').hide();");
            $this->call("$('#js_add_field_button').attr('disabled', false);");
            $this->alert(_p('Please input name of field'));
        }
        

    }

    public function updateField()
    {
        $aVals = $this->get('val');
        if(Phpfox::getService('ultimatevideo.custom.process')->update($aVals['id'], $aVals))
        {
            $this->call("$('#js_add_field_loading').hide();");
            $this->call("$('#js_add_field_button').attr('disabled', false);");
            $this->editcustomfieldgroup();
            $this->call("js_box_remove($('.js_box_close'));");
        }
        else{
            $this->call("$('#js_add_field_loading').hide();");
            $this->call("$('#js_add_field_button').attr('disabled', false);");
            $this->alert(_p('Please input name of field'));
        }

    }

    public function deleteField()
    {
        $id = $this->get('id');
        if (Phpfox::getService('ultimatevideo.custom.process')->delete($id))
        {
            $this->remove('#js_custom_field_'.$id);
        }
    }
    
    public function deleteOption()
    {
        $id = $this->get('id');

        if (Phpfox::getService('ultimatevideo.custom.process')->deleteOption($id))
        {
            $aFields = Phpfox::getService('ultimatevideo.custom')->getCustomField();
            $this->remove('#js_current_value_'.$id);
        }
        else
        {
            $this->alert(_p('Could not delete'));
        }
    }

    public function toggleActiveGroup()
    {
        if (Phpfox::getService('ultimatevideo.custom.group')->toggleActivity($this->get('id')))
        {
            $this->call('$Core.ultimatevideo.toggleGroupActivity(' . $this->get('id') . ')');
        }       
    }
    public function AdminAddCustomFieldGroup()
    {
        if ($aVals = $this->get('val'))
        {
                if ($this->__isValid($aVals)){
                   
                    if (isset($aVals['group_id']))
                    {
                            if (Phpfox::getService('ultimatevideo.custom.group')->updateGroup($aVals['group_id'], $aVals))
                            {
                                Phpfox::getBlock('ultimatevideo.editcustomfield', array('iCustomFieldGroupId' => $aVals['group_id']));

                                $this->html('#app-custom-holder', $this->getContent(false));
                                $this->html('#ajax-response-custom',"<div class='message'>"._p('Custom Field Groups Successfully Updated')."</div>");
                                $this->call('setTimeout(function() {$(\'#ajax-response-custom\').html(\'\');},2000);');
                            }
                    } else
                    {
                            if ($iGroupId = Phpfox::getService('ultimatevideo.custom.group')->addGroup($aVals))
                            {
                                Phpfox::getBlock('ultimatevideo.editcustomfield', array('iCustomFieldGroupId' => $iGroupId));

                                $this->html('#app-custom-holder', $this->getContent(false));
                                $this->html('#ajax-response-custom',"<div class='message'>"._p('Custom Field Groups Successfully Added')."</div>");
                                $this->call('setTimeout(function() {$(\'#ajax-response-custom\').html(\'\');},2000);');
                            }
                    }
                }
        }

    }
    public function AdminAddCategory()
    {
    if ($aVals = $this->get('val'))
        {
            if (isset($aVals['category_id']))
            {
                if (Phpfox::getService('ultimatevideo.category.process')->update($aVals['category_id'], $aVals))
                {
                    $this->html('#ajax-response-custom',"<div class='message'>"._p('Category Successfully Updated')."</div>");
                    $this->call('setTimeout(function() {$(\'#ajax-response-custom\').html(\'\');$(\'.apps_menu ul li:eq(2) a\').trigger(\'click\')},2000);');
                }
            } else
            {
                if (Phpfox::getService('ultimatevideo.category.process')->add($aVals))
                {
                    $this->html('#ajax-response-custom',"<div class='message'>"._p('Category Successfully Added')."</div>");
                    $this->call('setTimeout(function() {$(\'#ajax-response-custom\').html(\'\');$(\'.apps_menu ul li:eq(2) a\').trigger(\'click\')},2000);');
                }
            }
        }
    }
    public function AdminDeleteCategory()
    {
        $iCategoryId = (int) $this->get('id');
        if (Phpfox::getService('ultimatevideo.category.process')->delete($iCategoryId))
            {
                $this->html('#ajax-response-custom',"<div class='message'>"._p('Category successfully deleted')."</div>");
                $this->call('setTimeout(function() {$(\'#ajax-response-custom\').html(\'\');$(\'.apps_menu ul li:eq(2) a\').trigger(\'click\')},2000);');
            }
    }
    public function AdminUpdateOrderCategory()
    {
        if ($aOrder = $this->get('order'))
        {
            if (Phpfox::getService('ultimatevideo.category.process')->updateOrder($aOrder))
            {
                $this->html('#ajax-response-custom',"<div class='message'>"._p('Category order successfully updated.')."</div>");
            }
        }
    }
    public function AdminUpdateOrderCustomField()
    {
        if (($aFieldOrders = $this->get('field')) && Phpfox::getService('ultimatevideo.custom.process')->updateOrder($aFieldOrders))
        {           
            $this->html('#ajax-response-custom',"<div class='message'>"._p('Custom fields successfully updated.')."</div>");

        }
        
        if (($aGroupOrders = $this->get('group')) && Phpfox::getService('ultimatevideo.custom.group')->updateOrder($aGroupOrders))
        {           
            $this->html('#ajax-response-custom',"<div class='message'>"._p('Custom fields successfully updated.')."</div>");

        }   
    }
    public function AdminDeleteCustomFieldGroup()
    {
        $iCustomFieldGroupId = (int) $this->get('id');
        if (Phpfox::getService('ultimatevideo.custom.group')->deleteGroup($iCustomFieldGroupId))
        {
            $this->html('#ajax-response-custom',"<div class='message'>"._p('Custom fields successfully deleted.')."</div>");
            $this->call('setTimeout(function() {$(\'#ajax-response-custom\').html(\'\');$(\'.apps_menu ul li:eq(5) a\').trigger(\'click\')},2000);');
        }
    }
    public function AdminDeleteCustomField()
    {
        $iCustomFieldDelete = $this->get('iFieldId');
        $iCustomFieldGroupId = $this->get('iGroupId');
        if(!isset($iCustomFieldGroupId) && empty($iCustomFieldGroupId)) return;
        if (Phpfox::getService('ultimatevideo.custom.process')->delete($iCustomFieldDelete))
        {
                Phpfox::getBlock('ultimatevideo.editcustomfield', array('iCustomFieldGroupId' => $iCustomFieldGroupId));

                $this->html('#app-custom-holder', $this->getContent(false));
        }
    }
    public function __isValid($aVals)
    {
        $emptyGroupName = false;
        $group_name = $aVals['group_name'];
        foreach ($group_name as $keygroup_name => $valuegroup_name) {
            if(is_array($valuegroup_name)){
                foreach ($valuegroup_name as $keyvaluegroup_name => $valuevaluegroup_name) {
                    if(strlen(trim($valuevaluegroup_name)) == 0){
                        $emptyGroupName = true;
                        break;
                    }
                }
            } else {
                if(strlen(trim($valuegroup_name)) == 0){
                    $emptyGroupName = true;
                    break;
                }
            }
        }
        if($emptyGroupName){
            Phpfox_Error::set(_p('Group name cannot be empty'));
            return false;
        }

        return true;
    }
    public function changeCustomFieldByCategory()
    {
        $oRequest = Phpfox::getLib('request');
        $iCategoryId = $oRequest->get('iCategoryId');
        $aCustomFields = Phpfox::getService('ultimatevideo')->getCustomFieldByCategoryId($iCategoryId);

        $keyCustomField = array();


        $aCustomData = array();
        if($this->get('iVideoId')){
            $aCustomDataTemp = Phpfox::getService('ultimatevideo.custom')->getCustomFieldByVideoId($this->get('iVideoId'));
            
                if(count($aCustomFields)){
                    foreach ($aCustomFields as $aField) {
                            foreach ($aCustomDataTemp as $aFieldValue) {
                                if($aField['field_id'] == $aFieldValue['field_id']){
                                    $aCustomData[] = $aFieldValue;
                                }
                            }
                    }
                }

        }

        if(count($aCustomData)){
            $aCustomFields  = $aCustomData; 
        }

        Phpfox::getBlock('ultimatevideo.custom.form', array(
            'aCustomFields' => $aCustomFields, 
        ));
        // FAILURE/SUCCESS
        echo json_encode(array(
            'status' => 'SUCCESS', 
            'content' => $this->getContent(false)
        ));
    }
    public function validationUrl()
    {
        $url = $this->get('url');
        $type = $this->get('type');
        if($type == "VideoURL"){
            if(empty($url)){
                echo json_encode(array(
                    'status' => "FAIL",
                    'error_message' => _p('We could not find a video there - please check the URL and try again.')
                ));
            }
            else{
                echo json_encode(array(
                    'status' => "SUCCESS",
                    'title' => "",
                    'description' => ""
                ));
            }
            die;
        }
        $adapter = Phpfox::getService('ultimatevideo') -> getClass($type);
        if($type != "Dailymotion"){
            $adapter -> setParams(array('code' => $url));
        }
        else{
            $adapter -> setParams(array('link' => $url));
            $adapter -> setParams(array('code' => $url));
        }
        $valid = ($adapter -> isValid())?true:false;
        if($adapter -> fetchLink())
        {
            $title = strip_tags($adapter -> getVideoTitle());
            $description = $adapter -> getVideoDescription();
            $description = str_replace("<br />", "\r\n", $description);
            echo json_encode(array(
                'status' => "SUCCESS",
                'title' => $title,
                'description' => $description
                ));
        }
        else{
            echo json_encode(array(
                'status' => "FAIL",
                'error_message' => _p('We could not find a video there - please check the URL and try again.')
                ));
        }
    }
    public function updateFeaturedInAdmin()
    {
        // Get Params
        $iVideoId = (int)$this->get('iVideoId');
        $iIsFeatured = (int)$this->get('iIsFeatured');
        $iIsFeatured = (int)!$iIsFeatured;

        $oProcess = Phpfox::getService('ultimatevideo.process');
        if ($iVideoId)
        {
            $sResult = $oProcess->featureVideo($iVideoId, $iIsFeatured);
        }
        if(!$sResult)
        {
            $this->alert(_p('You do not have permission to feature this video.'));
            return false;
        }
        if ($iIsFeatured)
        {
            $sLabel = '<img src="'.Phpfox::getParam('core.path').'theme/adminpanel/default/style/default/image/misc/bullet_green.png" alt="">';
            $this->html('#ynuv_video_update_featured_'.$iVideoId, '<div class="js_item_is_active"><a href="javascript:void(0);" class="js_item_active_link"  onclick="$Core.ajaxMessage();ultimatevideo.updateFeatured('.$iVideoId.','.$iIsFeatured.');"><div style="width:50px;">'.$sLabel.'</div></a></div>');
        }
        else
        {
            $sLabel = '<img src="'.Phpfox::getParam('core.path').'theme/adminpanel/default/style/default/image/misc/bullet_red.png" alt="">';
            $this->html('#ynuv_video_update_featured_'.$iVideoId, '<div class="js_item_is_not_active"><a href="javascript:void(0);" class="js_item_active_link"  onclick="$Core.ajaxMessage();ultimatevideo.updateFeatured('.$iVideoId.','.$iIsFeatured.');"><div style="width:50px;">'.$sLabel.'</div></a></div>');
        }

        return true;
    }
    public function updateApprovedInAdmin()
    {
        // Get Params
        $iVideoId = (int)$this->get('iVideoId');
        $iIsApproved = (int)$this->get('iIsApproved');
        $iIsApproved = (int)!$iIsApproved;

        $oProcess = Phpfox::getService('ultimatevideo.process');
        if ($iVideoId)
        {
            $sResult = $oProcess->approvedVideo($iVideoId, $iIsApproved);
        }
        if(!$sResult)
        {
            $this->alert(_p('You do not have permission to approve this video.'));
            return false;
        }
        if ($iIsApproved)
        {
            $sLabel = '<img src="'.Phpfox::getParam('core.path').'theme/adminpanel/default/style/default/image/misc/bullet_green.png" alt="">';
            $this->html('#ynuv_video_update_approve_'.$iVideoId, '<div class="js_item_is_active"><a href="#?call=ultimatevideo.updateApprovedInAdmin&amp;iVideoId='.$iVideoId.'&amp;iIsApproved='.$iIsApproved.'" class="js_item_active_link" ><div style="width:50px;">'.$sLabel.'</div></a></div>');
        }
        else
        {
            $sLabel = '<img src="'.Phpfox::getParam('core.path').'theme/adminpanel/default/style/default/image/misc/bullet_red.png" alt="">';
            $this->html('#ynuv_video_update_approve_'.$iVideoId, '<div class="js_item_is_not_active"><a href="#?call=ultimatevideo.updateApprovedInAdmin&amp;iVideoId='.$iVideoId.'&amp;iIsApproved='.$iIsApproved.'" class="js_item_active_link" ><div style="width:50px;">'.$sLabel.'</div></a></div>');
        }
        $this->call('$Core.loadInit();');
        return true;
    }
    public function deleteVideoInAdmin()
    {
        $iVideoId = (int)$this->get('iVideoId');
        if($iVideoId)
        {
            if(Phpfox::getService('ultimatevideo.process')->deleteVideo($iVideoId))
            {
                $this->alert(_p('Video successfully deleted.'));
                $this->call('setTimeout(function() {js_box_remove($(\'.js_box_close\'));$(\'.apps_menu ul li:eq(7) a\').trigger(\'click\')},2000);');
            }
            else{
                $this->alert(_p('You do not have permission to delete this video.'));
            }
        }
    }
    public function actionMultiSelectVideo()
    {
        $aVals = $this->get('video_row');
        $aType = $this->get('val');
        if(!count($aVals)){
            $this->alert(_p('No Videos Selected'));
            return false;
        }
        $oProcess = Phpfox::getService('ultimatevideo.process');
        if($aType['selected']){
            switch ($aType['selected']) {
                case '1':
                    $success = false;
                    foreach ($aVals as $key => $videoID) {
                        $sResult = $oProcess->deleteVideo($videoID);
                        if(!$sResult)
                        {
                            $success = false;
                            $this->alert(_p('You do not have permission to detele videos.'));
                            continue;
                        }
                        else
                        {
                            $success = true;
                        }
                    }
                    if($success) {

                        $this->alert(_p('Videos successfully deleted.'));
                        $this->call('setTimeout(function() {js_box_remove($(\'.js_box_close\'));$(\'.apps_menu ul li:eq(7) a\').trigger(\'click\')},2000);');
                    }
                    else {
                        $this->alert(_p('Delete failed.'));
                    }
                    break;
                case '2':
                    foreach ($aVals as $key => $videoID) {
                        $sResult = $oProcess->approvedVideo($videoID, 1);
                        if(!$sResult)
                        {
                            $this->alert(_p('You do not have permission to approve videos.'));
                            continue;
                        }
                        else
                        {
                            $sLabel = '<img src="'.Phpfox::getParam('core.path').'theme/adminpanel/default/style/default/image/misc/bullet_green.png" alt="">';
                            $this->html('#ynuv_video_update_approve_'.$videoID, '<div class="js_item_is_active"><a href="javascript:void(0);" class="js_item_active_link"  onclick="ultimatevideo.updateApproved('.$videoID.',1);"><div style="width:50px;">'.$sLabel.'</div></a></div>');
                        }
                    }
                    break;
                case '3':
                    foreach ($aVals as $key => $videoID) {
                        $sResult = $oProcess->approvedVideo($videoID, 0);
                        if(!$sResult)
                        {
                            $this->alert(_p('You do not have permission to unapprove videos.'));
                            continue;
                        }
                        else
                        {
                            $sLabel = '<img src="'.Phpfox::getParam('core.path').'theme/adminpanel/default/style/default/image/misc/bullet_green.png" alt="">';
                            $this->html('#ynuv_video_update_approve_'.$videoID, '<div class="js_item_is_not_active"><a href="javascript:void(0);" class="js_item_active_link"  onclick="ultimatevideo.updateApproved('.$videoID.',0);"><div style="width:50px;">'.$sLabel.'</div></a></div>');
                        }
                    }
                    break;
                default:
                    # code...
                    break;
            }
        }
    }
    public function filterAdminFilterVideo(){
        $aSearch = $this->get('search');
        Phpfox::getComponent('ultimatevideo.admincp.managevideos', array('search' => $aSearch), 'controller');
        $this->html('#app-custom-holder', $this->getContent(false));
        $this->call('$Core.loadInit();');
    }
    public function changePageManageVideo(){
        $aPage = $this->get('page');
        $aParam = $this->getAll();
        $aSearch = array(
            'title'     => $this->get('title'),
            'owner'     => $this->get('owner'),
            'category'  => $this->get('category'),
            'source'    => $this->get('source'),
            'feature'   => $this->get('feature'),
            'approve'   => $this->get('approve')
            );
        Phpfox::getComponent('ultimatevideo.admincp.managevideos', array('page' => $aPage,'search' => $aSearch), 'controller');
        $this->html('#app-custom-holder', $this->getContent(false));
        $this->call('$Core.loadInit();');
    }

    public function delete_video()
    {
        $iVideoId = (int)$this->get('iVideoId');
        $isDetail = $this->get('isDetail');

        if($iVideoId)
        {
            if(Phpfox::getService('ultimatevideo.process')->deleteVideo($iVideoId))
            {
                $this->alert(_p('Videos successfully deleted.'));
                if($isDetail == 'false'){
                    $this->call('window.location.href=window.location.href');
                }
                else{
                    $sUrl = \Phpfox_Url::instance()->makeUrl('ultimatevideo');
                    $this->call('window.location.href="'.$sUrl.'"');
                }
            }
            else{
                $this->alert(_p('You do not have permission to delete this video.'));
                
            }
        }
    }

    public function approve_video()
    {
        // Get Params
        $iVideoId = (int)$this->get('iVideoId');

        $oProcess = Phpfox::getService('ultimatevideo.process');
        $sResult = null;

        if ($iVideoId)
        {
            $sResult = $oProcess->approvedVideo($iVideoId, 1);
        }

        if(!$sResult)
        {
            $this->alert(_p('You do not have permission to approve this video.'));
        }
        else
        {
            $this->alert(_p('Video successfully approved'));
            $this->call('window.location.href=window.location.href');
        }

    }

    public function featured_video()
    {
        // Get Params
        $iVideoId = (int)$this->get('iVideoId');

        $oProcess = Phpfox::getService('ultimatevideo.process');
        $sResult = null;

        if ($iVideoId)
        {
            $sResult = $oProcess->featureVideo($iVideoId, 1);
        }

        if(!$sResult)
        {
            $this->alert(_p('You do not have permission to feature this video.'));
        }else{
            $this->call('$(".ynuv_feature_video_'.$iVideoId.'").data("cmd","unfeatured_video").html("<i class=\"fa fa-diamond\"></i>'.
                _p(' Un-Featured').'");');
            $this->call('$(".ynuv_feature_video_icon_'.$iVideoId.'").css("display","inline-flex");');
        }

    }

    public function unfeatured_video()
    {
        // Get Params
        $iVideoId = (int)$this->get('iVideoId');

        $oProcess = Phpfox::getService('ultimatevideo.process');
        $sResult = null;

        if ($iVideoId)
        {
            $sResult = $oProcess->featureVideo($iVideoId, 0);
        }

        if(!$sResult)
        {
            $this->alert(_p('You do not have permission to featured this video.'));
        }else{
            $this->call('$(".ynuv_feature_video_'.$iVideoId.'").data("cmd","featured_video").html("<i class=\"fa fa-diamond\"></i>'.
                _p(' Featured').'");');
            $this->call('$(".ynuv_feature_video_icon_'.$iVideoId.'").css("display","none");');
        }


    }

    public function favorite_video()
    {
        $iVideoId = (int)$this->get('iVideoId');

        Phpfox::getService('ultimatevideo.favorite')->add(Phpfox::getUserId(), $iVideoId);


        $this->call('$(".ynuv_favorite_video_'.$iVideoId.'").data("cmd","unfavorite_video").html("<i class=\"fa fa-star\"></i>'.
                _p(' Un-Favorite').'")');

    }

    public function unfavorite_video()
    {
        $iVideoId = (int)$this->get('iVideoId');

        Phpfox::getService('ultimatevideo.favorite')->delete(Phpfox::getUserId(), $iVideoId);

        $this->call('if(window.location.href.match("view=favorite") != null || window.location.href.match("view_favorite") != null){setTimeout(function(){window.location.href=window.location.href},2000);}');
        $this->call('$(".ynuv_favorite_video_'.$iVideoId.'").data("cmd","favorite_video").html("<i class=\"fa fa-star-o\"></i>'.
                _p(' Favorite').'")');
        
    }

    public function watchlater_video()
    {
        $iVideoId = (int)$this->get('iVideoId');

        Phpfox::getService('ultimatevideo.watchlater')->add(Phpfox::getUserId(), $iVideoId);

        $this->call('$(".ynuv_watchlater_video_'.$iVideoId.'").data("cmd","unwatchlater_video").html("<i class=\"fa fa-clock-o\"></i>'.
                _p(' Un-Watch Later').'")');
    }

    public function unwatchlater_video()
    {
        $iVideoId = (int)$this->get('iVideoId');

        Phpfox::getService('ultimatevideo.watchlater')->delete(Phpfox::getUserId(), $iVideoId);

        $this->call('if(window.location.href.match("view=later") != null || window.location.href.match("view_later") != null){setTimeout(function(){window.location.href=window.location.href},2000);}');
        $this->call('$(".ynuv_watchlater_video_'.$iVideoId.'").data("cmd","watchlater_video").html("<i class=\"fa fa-clock-o\"></i>'.
                _p(' Watch Later').'")');
    }
    public function delete_video_history()
    {
        $iVideoId = (int)$this->get('iVideoId');
        if($iVideoId)
        {
            if(Phpfox::getService('ultimatevideo.history')->deleteVideo(Phpfox::getUserId(),$iVideoId))
            {
                $this->alert(_p('Video successfully deleted from your history.'));
                $this->call('{setTimeout(function(){window.location.href=window.location.href},2000);}');
            }
            else{
                $this->alert(_p('Delete history fail. Please try again.'));
            }
        }
    }
    public function delete_playlist_history()
    {
        $iPlaylistId = (int)$this->get('iVideoId');
        if($iPlaylistId)
        {
            if(Phpfox::getService('ultimatevideo.history')->deletePlaylist(Phpfox::getUserId(),$iPlaylistId))
            {
                $this->alert(_p('Playlist successfully deleted from your history.'));
                $this->call('{setTimeout(function(){window.location.href=window.location.href},2000);}');
            }
            else{
                $this->alert(_p('Delete history fail. Please try again.'));
            }
        }
    }    
    public function video_clear_all()
    {
        $sView = $this->get('sView');
        if($sView == 'history'){
            Phpfox::getService('ultimatevideo.history')->deleteAllHistory(0);
            $message = _p('Videos in history are removed successfully');
        }
        elseif($sView == 'favorite'){
            Phpfox::getService('ultimatevideo.favorite')->deleteAllFavorite();
            $message = _p('Videos in favorite are removed successfully');
        }
        elseif($sView == 'later'){
            Phpfox::getService('ultimatevideo.watchlater')->deleteAllWatchlater();
            $message = _p('Videos in watch later are removed successfully');
        }
        $this->alert($message);
        $this->call('{setTimeout(function(){window.location.href=window.location.href},2000);}');
    }
    public function playlist_clear_all()
    {
        Phpfox::getService('ultimatevideo.history')->deleteAllHistory(1);
        $this->alert(_p('Playlists in history are deleted successfully'));
        $this->call('{setTimeout(function(){window.location.href=window.location.href},2000);}');
    }
    public function showPopupCustomGroup(){
        $iCategoryId = $this->get('category_id');
        Phpfox::getBlock('ultimatevideo.popup_customfield_category', array('category_id' => $iCategoryId));

    }
    public function updateFeaturedPlaylistInAdmin()
    {
        // Get Params
        $iPlaylistId = (int)$this->get('iPlaylistId');
        $iIsFeatured = (int)$this->get('iIsFeatured');
        $iIsFeatured = (int)!$iIsFeatured;

        $oProcess = Phpfox::getService('ultimatevideo.playlist.process');
        if ($iPlaylistId)
        {
            $sResult = $oProcess->feature($iPlaylistId, $iIsFeatured);
        }
        if(!$sResult)
        {
            $this->alert(_p('You do not have permission to feature this playlist.'));
            return false;
        }
        if ($iIsFeatured)
        {
            $sLabel = '<img src="'.Phpfox::getParam('core.path').'theme/adminpanel/default/style/default/image/misc/bullet_green.png" alt="">';
            $this->html('#ynuv_playlist_update_featured_'.$iPlaylistId, '<div class="js_item_is_active"><a href="javascript:void(0);" class="js_item_active_link"  onclick="ultimatevideo_playlist.updateFeatured\('.$iPlaylistId.','.$iIsFeatured.');"><div style="width:50px;">'.$sLabel.'</div></a></div>');
        }
        else
        {
            $sLabel = '<img src="'.Phpfox::getParam('core.path').'theme/adminpanel/default/style/default/image/misc/bullet_red.png" alt="">';
            $this->html('#ynuv_playlist_update_featured_'.$iPlaylistId, '<div class="js_item_is_not_active"><a href="javascript:void(0);" class="js_item_active_link"  onclick="ultimatevideo_playlist.updateFeatured('.$iPlaylistId.','.$iIsFeatured.');"><div style="width:50px;">'.$sLabel.'</div></a></div>');
        }

        return true;
    }
    public function updateApprovedPlaylistInAdmin()
    {
        // Get Params
        $iPlaylistId = (int)$this->get('iPlaylistId');
        $iIsApproved = (int)$this->get('iIsApproved');
        $iIsApproved = (int)!$iIsApproved;

        $oProcess = Phpfox::getService('ultimatevideo.playlist.process');
        if ($iPlaylistId)
        {
            $sResult = $oProcess->approved($iPlaylistId, $iIsApproved);
        }
        if(!$sResult)
        {
            $this->alert(_p('You do not have permission to approve this playlist.'));
            return false;
        }
        if ($iIsApproved)
        {
            $sLabel = '<img src="'.Phpfox::getParam('core.path').'theme/adminpanel/default/style/default/image/misc/bullet_green.png" alt="">';
            $this->html('#ynuv_playlist_update_approve_'.$iPlaylistId, '<div class="js_item_is_active"><a href="javascript:void(0);" class="js_item_active_link"  onclick="ultimatevideo_playlist.updateApproved('.$iPlaylistId.','.$iIsApproved.');"><div style="width:50px;">'.$sLabel.'</div></a></div>');
        }
        else
        {
            $sLabel = '<img src="'.Phpfox::getParam('core.path').'theme/adminpanel/default/style/default/image/misc/bullet_red.png" alt="">';
            $this->html('#ynuv_playlist_update_approve_'.$iPlaylistId, '<div class="js_item_is_not_active"><a href="javascript:void(0);" class="js_item_active_link"  onclick="ultimatevideo_playlist.updateApproved('.$iPlaylistId.','.$iIsApproved.');"><div style="width:50px;">'.$sLabel.'</div></a></div>');
        }

        return true;
    }
    public function deletePlaylistInAdmin()
    {
        $iPlaylistId = (int)$this->get('iPlaylistId');
        if($iPlaylistId)
        {
            if(Phpfox::getService('ultimatevideo.playlist.process')->delete($iPlaylistId))
            {
                $this->alert(_p('PLaylist successfully deleted.'));
                $this->call('setTimeout(function() {js_box_remove($(\'.js_box_close\'));$(\'.apps_menu ul li:eq(8) a\').trigger(\'click\')},2000);');
            }
            else{
                $this->alert(_p('You do not have permission to delete this playlist.'));
            }
        }
    }
    public function filterAdminFilterPlaylist()
    {
        $aSearch = $this->get('search');
        Phpfox::getComponent('ultimatevideo.admincp.manageplaylists', array('search' => $aSearch), 'controller');
        $this->html('#app-custom-holder', $this->getContent(false));
        $this->call('$Core.loadInit();');
    } 
    public function actionMultiSelectPlaylist()
    {
        $aVals = $this->get('playlist_row');
        $aType = $this->get('val');
        if(!count($aVals)){
            $this->alert(_p('No Playlists Selected'));
            return false;
        }
        $oProcess = Phpfox::getService('ultimatevideo.playlist.process');
        if($aType['selected']){
            switch ($aType['selected']) {
                case '1':
                    $success = false;
                    foreach ($aVals as $key => $playlistID) {
                        $sResult = $oProcess->delete($playlistID);
                        if(!$sResult)
                        {
                            $success = false;
                            $this->alert(_p('You do not have permission to detele playlists.'));
                            continue;
                        }
                        else
                        {
                            $success = true;
                        }
                    }
                    if($success) {

                        $this->alert(_p('Playlists successfully deleted.'));
                        $this->call('setTimeout(function() {js_box_remove($(\'.js_box_close\'));$(\'.apps_menu ul li:eq(8) a\').trigger(\'click\')},2000);');
                    }
                    else {
                        $this->alert(_p('Delete failed.'));
                    }
                    break;
                case '2':
                    foreach ($aVals as $key => $playlistID) {
                        $sResult = $oProcess->approved($playlistID, 1);
                        if(!$sResult)
                        {
                            $this->alert(_p('You do not have permission to approve playlists.'));
                            continue;
                        }
                        else
                        {
                            $sLabel = '<img src="'.Phpfox::getParam('core.path').'theme/adminpanel/default/style/default/image/misc/bullet_green.png" alt="">';
                            $this->html('#ynuv_playlist_update_approve_'.$playlistID, '<div class="js_item_is_active"><a href="javascript:void(0);" class="js_item_active_link"  onclick="ultimatevideo_playlist.updateApproved('.$playlistID.',1);"><div style="width:50px;">'.$sLabel.'</div></a></div>');
                        }
                    }
                    break;
                case '3':
                    foreach ($aVals as $key => $playlistID) {
                        $sResult = $oProcess->approved($playlistID, 0);
                        if(!$sResult)
                        {
                            $this->alert(_p('You do not have permission to unapprove playlists.'));
                            continue;
                        }
                        else
                        {
                            $sLabel = '<img src="'.Phpfox::getParam('core.path').'theme/adminpanel/default/style/default/image/misc/bullet_green.png" alt="">';
                            $this->html('#ynuv_playlist_update_approve_'.$playlistID, '<div class="js_item_is_not_active"><a href="javascript:void(0);" class="js_item_active_link"  onclick="ultimatevideo_playlist.updateApproved('.$playlistID.',0);"><div style="width:50px;">'.$sLabel.'</div></a></div>');
                        }
                    }
                    break;
                default:
                    # code...
                    break;
            }
        }
    }

    public function rate_video()
    {
        Phpfox::isUser(true);
        $iVideoId = (int)$this->get('iVideoId');
        $iRating =  (int)$this->get('iValue');

        if($iVideoId)
        {
            if(Phpfox::getService('ultimatevideo.rating')->add(Phpfox::getUserId(),$iVideoId, $iRating))
            {
                $this->alert(_p('Rated successfully.'));
                $this->call('{setTimeout(function(){window.location.href=window.location.href},2000);}');
            }
            else{
                $this->alert(_p('Rating fail. Please try again.'));
            }
        }
    }

    public function moderation()
    {
        Phpfox::isUser(true);   
        
        switch ($this->get('action'))
        {
            case 'approve':
                user('ynuv_can_approve_video', true);
                foreach ((array) $this->get('item_moderate') as $iId)
                {
                    Phpfox::getService('ultimatevideo.process')->approvedVideo($iId,1);            
                }               
                $sMessage = _p('Videos successfully approved');
                break;          
            case 'delete':
                user('ynuv_can_delete_video_of_other_user', true);
                foreach ((array) $this->get('item_moderate') as $iId)
                {
                    Phpfox::getService('ultimatevideo.process')->deleteVideo($iId);
                }
                $sMessage = _p('Videos successfully deleted');
                break;
            case 'feature':
                user('ynuv_can_feature_video', true);
                foreach ((array) $this->get('item_moderate') as $iId)
                {
                    Phpfox::getService('ultimatevideo.process')->featureVideo($iId,1);
                }               
                $sMessage = _p('Videos successfully featured');
                break;
            case 'unfavorite':
                foreach ((array) $this->get('item_moderate') as $iId)
                {
                    Phpfox::getService('ultimatevideo.favorite')->delete(Phpfox::getUserId(), $iId);
                }
                $sMessage = _p('Videos successfully un-favorite');
                break;
            case 'unwatched':
                foreach ((array) $this->get('item_moderate') as $iId)
                {
                    Phpfox::getService('ultimatevideo.watchlater')->delete(Phpfox::getUserId(), $iId);
                }
                $sMessage = _p('Videos successfully remove from watch later list.');
                break;
            case 'history':
                foreach ((array) $this->get('item_moderate') as $iId)
                {
                    Phpfox::getService('ultimatevideo.history')->deleteVideo(Phpfox::getUserId(),$iId);
                }
                $sMessage = _p('Videos successfully remove from history');
                break;    
        }
        
        $this->alert($sMessage, 'Moderation', 300, 150, true);
        $this->hide('.moderation_process');     
        $this->call('setTimeout(function(){ window.location.href = window.location.href; },3000);');
    }
    public function changePageManagePlaylist()
    {
        $aPage = $this->get('page');
        $aParam = $this->getAll();
        $aSearch = array(
            'title'     => $this->get('title'),
            'owner'     => $this->get('owner'),
            'category'  => $this->get('category'),
            'feature'   => $this->get('feature'),
            'approve'   => $this->get('approve')
            );
        Phpfox::getComponent('ultimatevideo.admincp.manageplaylists', array('page' => $aPage,'search' => $aSearch), 'controller');
        $this->html('#app-custom-holder', $this->getContent(false));
        $this->call('$Core.loadInit();');
    }
    public function getAllPlaylistOfUser()
    {
        Phpfox::getBlock('ultimatevideo.user_playlist_checklist');
        $eleId =  $this->get('eleId');
        $this->html('#' . $eleId, $this->getContent(false));
    }
    public function updateQuickAddVideoToPlaylist()
    {

        $isChecked = $this->get('isChecked');
        $iVideoId = $this->get('iVideoId');
        $iPlaylistId = $this->get('iPlaylistId');
        $iContainerId = $this->get('iContainerId');
        if(!$iVideoId || !$iPlaylistId)
        {
            $this->alert(_p("Can't add video to playlist"));
            return;
        }
        $aPlaylist = Phpfox::getService('ultimatevideo.playlist')->getPlaylistById($iPlaylistId);
        
        if($isChecked)
        {
            $result = Phpfox::getService('ultimatevideo.playlist.process')->addVideo($iVideoId,$iPlaylistId);

            if(!$result){
                $message = _p('Can not add videos to this playlist.');
                $this->call('$(".ynuv_error_add_to_playlist_'.$iVideoId.'").css("display","flex");');
                $this->html('.ynuv_error_add_to_playlist_'.$iVideoId,$message);
                $this->call('$("#'.$iContainerId.'").find(".checkbox > label[data-playlist=\''.$iPlaylistId. '\'] input").removeAttr("checked");');
                $this->call('setTimeout(function(){$(".ynuv_error_add_to_playlist_'.$iVideoId.'").hide()},3000);');
            }
        }
        else
        {
            $result = Phpfox::getService('ultimatevideo.playlist.process')->removeVideo($iVideoId,$iPlaylistId);
            if(!$result){
                $message = _p('Remove failed. This video is not belong to this playlist');
                $this->call('$(".ynuv_error_add_to_playlist_'.$iVideoId.'").css("display","flex");');
                $this->html('.ynuv_error_add_to_playlist_'.$iVideoId,$message);
                $this->call('setTimeout(function(){$(".ynuv_error_add_to_playlist_'.$iVideoId.'").hide()},3000);');
            }
        }
    }
    public function addPlaylistOnAction()
    {
        $iVideoId = $this->get('iVideoId');
        $iContainerId = $this->get('iContainerId');
        $aVals = array(
            'title' => $this->get('sTitle')
        );

        if(!user('ynuv_can_add_playlist',0))
        {
            $message = _p('You don\'t have permission to add new playlist');
            $this->call('$(".ynuv_error_add_to_playlist_'.$iVideoId.'").show();');
            $this->html('.ynuv_error_add_to_playlist_'.$iVideoId,$message);
            $this->call('setTimeout(function(){$(".ynuv_error_add_to_playlist_'.$iVideoId.'").hide()},3000);');
            return;
        }
        if($iPlaylistId = Phpfox::getService('ultimatevideo.playlist.process')->add($aVals, true, $iVideoId))
        {
            $aPlaylist = Phpfox::getService('ultimatevideo.playlist')->getPlaylistById($iPlaylistId);
            $result = Phpfox::getService('ultimatevideo.playlist.process')->addVideo($iVideoId,$iPlaylistId);
            if($result){
                $message = _p('New playlist has just been created successfully');
                $this->call('$(".ynuv_noti_add_to_playlist_'.$iVideoId.'").css("display","flex");');
                $this->html('.ynuv_noti_add_to_playlist_'.$iVideoId,$message);
                $this->call('setTimeout(function(){$(".ynuv_noti_add_to_playlist_'.$iVideoId.'").hide()},3000);');
            }
            else{
                $message = _p('Can not add videos to new playlist.');
                $this->call('$(".ynuv_error_add_to_playlist_'.$iVideoId.'").css("display","flex");');
                $this->html('.ynuv_error_add_to_playlist_'.$iVideoId,$message);
                $this->call('setTimeout(function(){$(".ynuv_error_add_to_playlist_'.$iVideoId.'").hide()},3000);');
            }

            Phpfox::getBlock('ultimatevideo.user_playlist_checklist');
            $this->html('#' . $iContainerId, $this->getContent(false));
        }

    }

    public function featured_playlist()
    {
        // Get Params
        $iPlaylistId = (int)$this->get('iVideoId');

        $oProcess = Phpfox::getService('ultimatevideo.playlist.process');
        $sResult = null;

        if ($iPlaylistId)
        {
            $sResult = $oProcess->feature($iPlaylistId, 1);
        }

        if(!$sResult)
        {
            $this->alert(_p('You do not have permission to feature this playlist.'));
        }else{
            $this->call('$(".ynuv_feature_playlist_'.$iPlaylistId.'").data("cmd","unfeatured_playlist").html("<i class=\"fa fa-diamond\"></i>'.
                _p(' Un-Featured').'");');
            $this->call('$(".ynuv_feature_playlist_icon_'.$iPlaylistId.'").css("display","inline-flex");');
        }

    }

    public function unfeatured_playlist()
    {
        // Get Params
        $iPlaylistId = (int)$this->get('iVideoId');

        $oProcess = Phpfox::getService('ultimatevideo.playlist.process');
        $sResult = null;

        if ($iPlaylistId)
        {
            $sResult = $oProcess->feature($iPlaylistId, 0);
        }

        if(!$sResult)
        {
            $this->alert(_p('You do not have permission to featured this playlist.'));
        }else{
            $this->call('$(".ynuv_feature_playlist_'.$iPlaylistId.'").data("cmd","featured_playlist").html("<i class=\"fa fa-diamond\"></i>'.
                _p(' Featured').'");');
            $this->call('$(".ynuv_feature_playlist_icon_'.$iPlaylistId.'").css("display","none");');
        }


    }  

    public function approve_playlist()
    {
        // Get Params
        $iPlaylistId = (int)$this->get('iVideoId');

        $oProcess = Phpfox::getService('ultimatevideo.playlist.process');
        $sResult = null;

        if ($iPlaylistId)
        {
            $sResult = $oProcess->approved($iPlaylistId, 1);
        }

        if(!$sResult)
        {
            $this->alert(_p('You do not have permission to approve this playlist.'));
        }
        else
        {
            $this->alert(_p('Playlist successfully approved'));
            $this->call('window.location.href=window.location.href');
        }

    }
    public function delete_playlist()
    {
        $iPlaylistId = (int)$this->get('iPlaylistId');
        $isDetail = $this->get('isDetail');
        if($iPlaylistId)
        {
            if(Phpfox::getService('ultimatevideo.playlist.process')->delete($iPlaylistId))
            {
                $this->alert(_p('Playlist successfully deleted.'));
                if($isDetail == 'false'){
                    $this->call('window.location.href=window.location.href');
                }
                else{
                    $sUrl = \Phpfox_Url::instance()->makeUrl('ultimatevideo.playlist');
                    $this->call('window.location.href="'.$sUrl.'"');
                }
            }
            else{
                $this->alert(_p('You do not have permission to delete this playlist.'));
            }
        }
    }
    public function playlist_moderation(){
        Phpfox::isUser(true);   
        
        switch ($this->get('action'))
        {
            case 'approve':
                user('ynuv_can_approve_playlist', true);
                foreach ((array) $this->get('item_moderate') as $iId)
                {
                    Phpfox::getService('ultimatevideo.playlist.process')->approved($iId,1);            
                }               
                $sMessage = _p('Playlists successfully approved');
                break;          
            case 'delete':
                user('ynuv_can_delete_playlist_of_other_user', true);
                foreach ((array) $this->get('item_moderate') as $iId)
                {
                    Phpfox::getService('ultimatevideo.playlist.process')->delete($iId);
                }
                $sMessage = _p('Playlists successfully deleted');
                break;
            case 'feature':
                user('ynuv_can_feature_playlist', true);
                foreach ((array) $this->get('item_moderate') as $iId)
                {
                    Phpfox::getService('ultimatevideo.playlist.process')->feature($iId,1);
                }               
                $sMessage = _p('Playlists successfully featured');
                break;   
            case 'history':
                foreach ((array) $this->get('item_moderate') as $iId)
                {
                    Phpfox::getService('ultimatevideo.history')->deletePlaylist(Phpfox::getUserId(),$iId);
                }
                $sMessage = _p('Playlists successfully remove from history');
                break;       
        }
        
        $this->alert($sMessage, 'Moderation', 300, 150, true);
        $this->hide('.moderation_process');     
        $this->call('setTimeout(function(){ window.location.href = window.location.href; },3000);'); 
    } 
    public function changePageVideosInPlaylist(){
        $iPage = $this->get('page');
        $iPlaylistId = $this->get('playlist');
        $iModeView = $this->get('mode');
        Phpfox::getBlock('ultimatevideo.playlist_detail_mode_listing', array('page' => $iPage,'playlist_id' => $iPlaylistId,'current_mode'=>$iModeView));
        $this->html('#ultimatevide_playlist_mode_listing', $this->getContent(false));
        $this->call('$Core.loadInit();');
    }
    public function categoryOrdering()
    {
        Phpfox::isAdmin(true);
        $aVals = $this->get('val');
        Core_Service_Process::instance()->updateOrdering(array(
                'table' => 'ynultimatevideo_category',
                'key' => 'category_id',
                'values' => $aVals['ordering']
            )
        );

        Phpfox::getLib('cache')->remove('ynultimatevideo', 'substr');
    }

    public function updateActivity()
    {
        if (Phpfox::getService('ultimatevideo.category.process')->updateActivity($this->get('id'), $this->get('active'), $this->get('sub')))
        {

        }
    }
    public function getsubcategory(){
        $iSub = $this->get('id');
        Phpfox::getComponent('ultimatevideo.admincp.category.index', array('sub' => $iSub), 'controller');
        $this->html('#app-custom-holder', $this->getContent(false));
        $this->call('$Core.loadInit();');
    }
    public function loadFormAddOnFeed()
    {
        return Phpfox::getLib('template')->getBuiltFile('ultimatevideo.block.share_on_feed');
    }
    public function uploadVideoOnFeed(){
        $aVals = $this->get('val');
    }
}